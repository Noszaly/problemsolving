#prob78
# prob76-bol modositva
# brute force, ca. 1 minute very slow (i5@6500)


let
  TAR=10^6
  ini=0 #big(0) # 
  dp=[[1], [1,2]]

  n=3
  while true # && n<100
    dpn=fill(ini,n)
    cs=dpn[1]=1
    for k in 2:n-1
      kk=if k>n-k n-k else k end # !
      cs=(cs+dp[n-k][kk])%TAR
      dpn[k]=cs
    end
    dpn[n]=(1+cs)%TAR
    push!(dp,dpn)
    if 0==dpn[n]
      break
    end
#println(n," ",dpn[n]%TAR)    
    n+=1
  end

  println(n,"-->",dp[n][n])
end
