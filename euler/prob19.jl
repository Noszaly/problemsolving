#prob19
# painless library solution

let
  s=0
  for y in 1901:2000, m in 1:12
    s+=(dayname(Date(y,m,1))=="Sunday")
  end
  s |> println
end

