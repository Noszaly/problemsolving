function pSieve(N) # modified for Euler's phi
  sieve=fill(1,N)
  sieve[4:2:N] .= 2
  
  n=3
  while n*n<=N
    if 1==sieve[n]
      sieve[n*n:2n:N] .= n
    end
    n+=2
  end

  sieve[1]=1 # by def it is zero, but it is needed for multiplicative prop.
  #sieve[2]=1
  sieve[3]=2
  for n in 4:N
    p=sieve[n]
    (1==p)&&(sieve[n]=n-1;continue)
    
    pk=1
    nn=n÷p
    while 0==nn%p
      nn=nn÷p
      pk=pk*p
    end
    sieve[n]=sieve[nn]*pk*(p-1)
  end
  sieve
end


let 
  N=1000000
  fi=pSieve(N)
  1+argmax((2:N) ./ fi[2:N]) |> println
end
  

