N=1000000
path=fill(-1,N)
path[1]=1

function mfill(s)
  ss=if s%2==0 s÷2 else 3*s+1 end
  if s>N
    1+mfill(ss)    
  else
    if path[s]<0
      path[s]=1+mfill(ss)
    end
    path[s]
  end
end

for s in 2:N
  path[s]=mfill(s)
end

path[1:10] |> println

argmax(path) |> println
