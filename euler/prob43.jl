# prob43

using OffsetArrays

let
  used=OffsetArray(fill(false,10),0:9)

  dlev=[2,3,5,7,11,13,17]
  dlev=vcat([0,0,0],dlev)

  s=0
  akt=fill(0,10)
  function gen(lev)
    if lev>10
      x=parse(Int,akt |> join)
      x |> println
      s+=x
    end
    for k in 0:9
      used[k] && continue
      used[k]=true
      akt[lev]=k
      if (lev<4) || (0==parse(Int,akt[lev-2:lev]|>join) % dlev[lev])
        gen(lev+1)
      end
      used[k]=false
    end
  end

  for k in 1:9
    akt[1]=k
    used[k]=true
    gen(2)
    used[k]=false
  end

  println("s=",s)
end
