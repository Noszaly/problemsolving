# prob24
# rank -> perm


# k = 0...n!-1
# arr represents the 0-th permutation
function r2p(arr,k)
  n=length(arr)
  ret=copy(arr)
  for l in 1:(n-1)
    q=k÷factorial(n-l)
    ll=l+q
    while ll>l
      ret[ll],ret[ll-1]=ret[ll-1],ret[ll]
      ll-=1
    end
    k = k-q*factorial(n-l)
  end
  ret
end

r2p(collect(0:9),10^6-1) .|> string |> join