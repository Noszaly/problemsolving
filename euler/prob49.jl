# prob49

let
  function primeGen(N)
    pr=fill(0,N)
    pr[1:5].=[2,3,5,7,11]
    npr=5

    delta=4
    x=13
    while true
      isp=true
      y=2
      while pr[y]^2<=x
        if x%pr[y]==0
          isp=false
          break
        end
        y+=1
      end
      
      if true==isp
        npr+=1
        pr[npr]=x
        if npr==N
          break
        end
      end
    
      x+=delta
      delta=6-delta
    end  
    pr
  end

  N=10000
  plist=primeGen(N)
  # println(plist[end])
  plist=plist[999 .< plist .< 9999]
  println(plist[end], " " ,plist|>length)
  # following Bentley:ProgPearls anagram-classes example
  dct=Dict()
  for k in 1:length(plist)
    p=plist[k]
    fp=digits(p)|>sort|>join
    dct[fp]=push!(get(dct,fp,[]),p)
  end
  
  for (p,pl) in dct
    #(p,pl)|>println
    l=length(pl)
    if l>2
      for i in 1:l-2
        for j in i+1:l-1
          for k in j+1:l
            if pl[i]+pl[k]==2*pl[j]
              println(pl[i]," ",pl[j]," ",pl[k])
            end
          end
        end
      end
    end
  end
  



    

end
