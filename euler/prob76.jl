#prob76
#utolso tag alapjan
#azt hogy 2 poz csak a vegen veszi figyelembe
#reszosszeget szamol menet kozben (cs-cumsum)

let
  N=100
  ini=0 #big(0) # ok nincs overflow, 400-500 kozott van a Int64 hatara
  dp=fill(ini,N,N)
  dp[1,1]=1
  dp[2,1]=dp[2,2]=1; 
  dp[2,2]=2 # cumulative


  for n in 3:N
    cs=dp[n,1]=1 # cs cumsum
    for k in 2:n-1
      kk=if k>n-k n-k else k end # !
      cs+=dp[n-k,kk]
      dp[n,k]=cs
    end
    cs+=1
    dp[n,n]=cs
  end

  dp[N,N]-1 |> println
  dp[5,5]-1 |> println

end