let 
  inp=read("p022_names.txt",String)
  inp=split(inp,"\"",keepempty=false)
  inp=inp[inp .!= ","]
  sort!(inp)
  # inp |> println
  L=length(inp)
  s = 0
  for l in 1:L
    s = s + l*sum(Int.(collect(inp[l])) .- 64)
  end
  s |> println
end  