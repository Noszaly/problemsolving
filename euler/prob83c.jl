#prob83

using DelimitedFiles

# A=[
#   0 1 1 1 1 1;
#   0 0 0 0 1 1;
#   1 1 1 0 1 1;
#   1 0 0 0 1 1;
#   1 0 1 1 1 1;
#   1 0 0 0 0 0
# ]

# A=[
#   131 673 234 103 18;
#   201 96 342 965 150;
#   630 803 746 422 111;
#   537 699 497 121 956;
#   805 732 524 37 331
# ]

let
  A=readdlm("p083_matrix.txt",',',Float64,'\n') # 20210531 the same as of 081
  r,c=size(A)

  #INF=typemax(Int)
  INF=Inf64
  outq=fill(true,r,c)
  M=fill(INF,r,c)
  M[1,1]=A[1,1]
  q=Queue{typeof((1,1))}()
  enqueue!(q,(1,1))
  outq[1,1]=false

  update(i,j,ii,jj)=
    if 1≤ii≤r && 1≤jj≤c
      if M[i,j]+A[ii,jj]< M[ii,jj]
        M[ii,jj]=M[i,j]+A[ii,jj]
        true
      else
        false
      end
    else
      false
    end

  # sok felesleges vizsgalat...
  
  while length(q)>0
    i,j=dequeue!(q)
    outq[i,j]=true

    #(i==r && j==c)&&break
    if true==update(i,j,i-1,j)
      if true==outq[i-1,j]
        outq[i-1,j]=false
        enqueue!(q,(i-1,j))
      end
    end
    if true==update(i,j,i+1,j)
      if true==outq[i+1,j]
        outq[i+1,j]=false
        enqueue!(q,(i+1,j))
      end
    end
    if true==update(i,j,i,j-1)
      if true==outq[i,j-1]
        outq[i,j-1]=false
        enqueue!(q,(i,j-1))
      end
    end
    if true==update(i,j,i,j+1)
      if true==outq[i,j+1]
        outq[i,j+1]=false
        enqueue!(q,(i,j+1))
      end
    end
  end

  #M|>println

  M[r,c] |> println
end