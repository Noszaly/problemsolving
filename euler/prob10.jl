# Euler prob/10

let
  pr=[3,5,7,11]
  
  S=2+sum(pr)
  X=2000000
  x=13
  while x<X
    isp=true
    for p in pr
      if p^2>x break end
      if x%p==0
        isp=false
        break
      end
    end
    
    if true==isp
      push!(pr,x)
      S+=x
    end
    
    x+=2
  end  
  
  println(S)
end