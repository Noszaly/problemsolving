# prob21
let 
  function pSieve(N) # modified for sum of divisors
    sieve=fill(1,N)
    sieve[4:2:N] .= 2
    sieve[1]=1
    p=3
    while p*p<=N
      if 1==sieve[p]
        sieve[p*p:2p:N] .= p
      end
      p+=2
    end
    sieve[2]=3
    sieve[3]=4
    for p in 4:N
      d=sieve[p]
      (1==d)&&(sieve[p]=1+p;continue)
      a=0
      pp=p
      while 0==pp%d
        a+=1
        pp=pp÷d
      end
      sieve[p]=sieve[pp]*(d^(a+1)-1)÷(d-1)
    end

    for p in 2:N
      sieve[p]-=p
    end


    sieve
  end

  N=10^4 # ad hoc limit
  isp=pSieve(N)
  isp[220] |> println
  isp[284] |> println

  s=0
  for a in 2:N
    sa=isp[a]
    if sa!=a && sa<=N && isp[sa]==a
      s+=a
    end
  end
  
  s |> println


end
