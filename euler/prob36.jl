let
  N=10^6
  s=0
  for k in 1:N
    k10=string(k,base=10)
    (k10 != reverse(k10)) && continue
    k2=string(k,base=2)
    (k2 != reverse(k2)) && continue
    s+=k
  end
  s |> println

end