using OffsetArrays

N=200
dp=OffsetVector(fill(1,N+1), 0:N)
# init=1 mert 1p mar feldolgozottnak tekintendo
C=[2,5,10,20,50,100,200]
for c in C
  for i in 0:N-c
    dp[i+c]+=dp[i]
  end
end

dp[200] |> println