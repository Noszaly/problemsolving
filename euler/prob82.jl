#prob82

using DelimitedFiles

# A=[
#   1 1 1 1 1 1;
#   0 0 0 0 1 1;
#   1 1 1 0 1 1;
#   1 0 0 0 1 1;
#   1 0 1 1 1 1;
#   1 0 0 0 0 0
# ]

# A=[
#   131 673 234 103 18;
#   201 96 342 965 150;
#   630 803 746 422 111;
#   537 699 497 121 956;
#   805 732 524 37 331
# ]

let
  A=readdlm("p082_matrix.txt",',',Int,'\n') # 20210531 the same as of 081
  r,c=size(A)
  M=copy(A)
  

  INF=typemax(Int)
  for j=2:c-1
    M[:,j]+=M[:,j-1]
    for i=2:r
      M[i,j]=min(M[i,j], M[i-1,j]+A[i,j])
    end
    for i=r-1:-1:1
      M[i,j]=min(M[i,j], M[i+1,j]+A[i,j])
    end

  end
  M[:,c]+=M[:,c-1]
  minimum(M[:,c]) |> println
end