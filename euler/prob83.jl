#prob83

using DelimitedFiles

# A=[
#   0 1 1 1 1 1;
#   0 0 0 0 1 1;
#   1 1 1 0 1 1;
#   1 0 0 0 1 1;
#   1 0 1 1 1 1;
#   1 0 0 0 0 0
# ]

A=[
  131 673 234 103 18;
  201 96 342 965 150;
  630 803 746 422 111;
  537 699 497 121 956;
  805 732 524 37 331
]

let
  A=readdlm("p083_matrix.txt",',',Int,'\n') # 20210531 the same as of 081
  r,c=size(A)

  #INF=typemax(Int)
  INF=2*sum(A)
  M=fill(INF,r,c)
  M[1,1]=A[1,1]

  update(i,j,δi,δj)=
    if 1≤i+δi≤r && 1≤j+δj≤c
      if M[i+δi,j+δj]<INF && M[i+δi,j+δj]+A[i,j]<M[i,j]
        M[i,j]=M[i+δi,j+δj]+A[i,j]
        true
      else
        false
      end
    else
      false
    end

  # sok felesleges vizsgalat...
  
  while true
    volt=false
    for j=vcat(1:c,c-1:-1:1)
      for i=vcat(1:r,r-1:-1:1)
        volt=volt||update(i,j,-1,0)
        volt=volt||update(i,j,0,-1)

        volt=volt||update(i,j,1,0)
        volt=volt||update(i,j,0,1)
      end
    end
    if false==volt
      println("*")
      for i=vcat(1:r,r-1:-1:1)
        for j=vcat(1:c,c-1:-1:1)
          volt=volt||update(i,j,-1,0)
          volt=volt||update(i,j,0,-1)

          volt=volt||update(i,j,1,0)
          volt=volt||update(i,j,0,1)
        end
      end
    end
    (false==volt)&&break
    sum(M)|>println
  end

  #M|>println

  M[r,c] |> println
end