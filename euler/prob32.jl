# prob 32
# A * B = C
# A is an A digit B is a B digit number, then C is 
# either an A+B-1 or an A+B digit number, here the latter is not possible
# bcos it would lead us to (A+B)+(A+B) = 9.
# so (A+B)+(A+B-1)=9 -> A+B-1=4 

using Combinatorics

let
  s=0
  for c in permutations(1:9,4)
    C=parse(Int, c .|> string |> join)
    dC=Set(C |> string)
    # non-empty intersection
    nei(x,y)=length(intersect(x,y))>0


    for A in 2:C
      (A*A>C)&& break
      if C%A==0
        B=C÷A
        dA=A |> string |> Set
        nei(dA,dC)&&continue
        dB=B |> string |> Set
        (nei(dB,dC) || nei(dA,dB))&&continue
        if union(dA,dB,dC)==Set('1':'9')
          # println(A," ",B," ",C)      
          s+=C
          break
        end
      end
    end
  end
  s |> println
end
