#prob99
let
  lines=readlines("p099_base_exp.txt")
  val=-1.0
  loc=-1

  for k in 1:length(lines)
    a,b=parse.(Int,split(lines[k],","))
    c=b*log(a)
    if c>val
      val=c
      loc=k
    end
  end
  loc |> println
end