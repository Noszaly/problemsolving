# prob39

let

  N=1000
  P=fill(0,N)

  for a in 1:N÷3
    a2=a*a
    for b in a+1:N÷2
      c2=a2+b*b
      c=Int(floor(sqrt(c2)))
      if a+b+c>N
        break
      end
      if c*c==c2
#        (a,b,c,a+b+c) |> println
        P[a+b+c]+=1
      end
    end
  end

  findall(x->x==maximum(P),P) |> println
end
