# prob42
let
  # sima sorszamok kellenek, 
  # ezek highly triangle words-ok lennének
  #d=Dict(x=>(n*(n+1))÷2 for (x,n) in zip('A':'Z',1:26))
  #d|> println
  d=Dict(x=>n for (x,n) in zip('A':'Z',1:26))

  function istri(k)
    q=Int(floor(sqrt((8k+1))))
    (q*q==8k+1)
  end
  #istri.(1:20) |> println


  inp=read("p042_words.txt",String)
  inp=split(inp,"\"",keepempty=false)
  inp=inp[inp .!= ","]

  w="SKY"
  sw=sum(d[c] for c in w)
  istri(sw) |> println

  #inp[1:10] |> println

  tw=0
  for w in inp
    sw=sum(d[c] for c in w)
    if istri(sw)
      tw+=1
      #w|>println
    end
  end
  tw|>println
end
