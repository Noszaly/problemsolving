#brute force

function nDiv(n)
  (1==n)&&(return 1)
  nd=2
  d=2
  while d*d<n
    (n%d==0)&&(nd+=2)
    d+=1
  end
  (d*d==n)&&(nd+=1)
  nd
end

let
  TAR=500
  opt=(k=-1,tri=-1,nd=-1)
  tri=0
  k=0
  while true
    k+=1
    tri+=k
    nd=nDiv(tri)
    if nd>opt.nd
      opt=(k=k,tri=tri,nd=nd)
      (nd>TAR)&&break
    end
    
  end
  opt |> println
end
