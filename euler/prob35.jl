let 
  function pSieve(N)
    sieve=fill(Int8(1),N)
    sieve[4:2:N] .= 0
    sieve[1]=0
    p=3
    while p*p<=N
      if 1==sieve[p]
        sieve[p*p:2p:N] .= 0
      end
      p+=2
    end
    sieve
  end

  N=10^6
  isp=pSieve(N) 
  isp[[2, 3, 5, 7]] .= 2
  # isp[1:20] |> println

  forbidden=Set("024685")
  for p in 11:2:N
    (isp[p]!=1)&&continue
    pdig=collect(string(p)) # digits of p
    if length(intersect(forbidden,pdig))>0
      isp[p]=-1
      continue
    end
    circ=Set() # set for circular perms
    for i in 1:length(pdig)
      push!(circ,parse(Int,join(pdig)))
      pdig=circshift(pdig,1)
    end
    state=2 
    for x in circ
      if isp[x]!=1
        state=-1
        break
      end
    end
    for x in circ
      isp[x]=state
    end
  end
  sum(isp .== 2) |> println
  (1:N)[isp .==2] |> println
end