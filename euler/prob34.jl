# prob34 
# digit factorials

# approach from the least diverse side
# perliminary observation:
# num of dig goes 2..7, for
# 10^(k-1) > k*9! if k>7

using Combinatorics
let
  s=0
  for k in 2:7
    for mc in multiset_combinations(0:9,fill(k,10),k)
      sfact=sum(factorial.(mc))
      d=digits(sfact)|>sort
      if length(d)==k && mc == d
        mcVal |> println
        s+=mcVal
      end
    end
  end
  println("s=",s)
end