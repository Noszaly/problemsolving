#prob53

using Combinatorics
let
  s=0
  for n in 23:100
    b=binomial.(big(n),2:n-2)
    s+=sum(b .> 10^6)
  end
  s |> println
end