#prob62
let
  # Bentley/indexing with sorted instance
  N=10000 # ad-hoc
  d=Dict()
  for k in 1:N
    fx=big(k)^3
    x=parse(BigInt,digits(fx)|>sort|>reverse|>join)
    d[x]=push!(get(d,x,[]),fx)
  end


  # at N there is a num of digit increase (->13), so we see the final lists for
  # at most 12 digit lists, we have two of them, 
  # just pick the minimal one
  mn=typemax(Int)
  for (a,b) in d
    if length(b)==5
      println(a, " ", b)
      mn=min(mn, minimum(b))
    end
  end
  mn |> println
end