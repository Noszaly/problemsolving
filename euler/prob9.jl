let
  S=1000
  for a in 1:S÷2
    for b in a+1:S
      c2=a^2+b^2
      c=floor(sqrt(c2)) |> Int
      if c^2==c2 && a+b+c==S
        println(a," ",b," ",c," ",a*b*c)
      end
    end

  end
end