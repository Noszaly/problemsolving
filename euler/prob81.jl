#prob81

using DelimitedFiles

# A=[
#   131 673 234 103 18;
#   201 96 342 965 150;
#   630 803 746 422 111;
#   537 699 497 121 956;
#   805 732 524 37 331
# ]

let
  A=readdlm("p081_matrix.txt",',',Int,'\n')
  n,_=size(A)

  INF=typemax(Int)
  for j=1:n
    for i=1:n
      t=INF
      (i>1) && (t=min(t,A[i-1,j]))
      (j>1) && (t=min(t,A[i,j-1]))
      (t==INF) && (t=0)
      A[i,j]+=t
    end
  end
  A[end,end] |> println
end