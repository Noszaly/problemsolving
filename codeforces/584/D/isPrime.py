# bf3@dmoj
from math import sqrt
def isp(x):
  if 1==x: return False
  if x<4: return True
  if 0==x%2 or 0==x%3: return False
  L=int(sqrt(x))
  d=5; delta=2
  while d<=L:
    if 0==x%d: return False
    d+=delta
    delta=6-delta
  return True

n=int(input())
if n<10:
  while False==isp(n): n+=1
else:
  r=n%6
  delta=4
  while True:
    if 0==r: n+=1; delta=4; break
    if 1==r: break
    n+=(5-r); delta=2
    break
  while False==isp(n): 
    n+=delta
    delta=6-delta
print(n)
    
  