# bf3@dmoj
def isp(x):
  if 1==x: return False
  if x<4: return True
  if 0==x%2 or 0==x%3: return False
  d=5; delta=2
  while d*d<=x:
    if 0==x%d: return False
    d+=delta
    delta=6-delta
  return True
def solve(n):
  t=n
  while isp(t)==False:
    t-=2
  d=n-t
  if d==0:
    return [t]
  if d==2:
    return [2,t]
  if d==4:
    return [2,2,t]
  if d==6:
    return [3,3,t]
  if d==8:
    return [3,5,t]

  v=d//2
  if isp(v):
    return [v,v,t]
  #print(t,d)

  v=5; delta=2
  while v<d-v and (isp(v)==False or isp(d-v)==False):
    v+=delta
    delta=6-delta
  #print(isp(v),isp(d-v))
  if v<d-v:
    return [v,d-v,t]
  return ['sorry...'] # a n<=10^9-re nem jut el

ans=solve(int(input()))
print(len(ans))
print(' '.join(str(v) for v in ans))
