def kat(c):
  if c.isdigit():return 1
  if c.isalpha():return 2
  return 0

def szetszed(s):  
  arr=[]; akt=''; p=0
  for c in s:
    k=kat(c)
    if p==k:
      akt+=c
    else:
      arr.append(akt)
      p=k
      akt=c
  return arr

dd={};ff={}
for a,da in zip('ABCDEFGHIJKLMNOPQRSTUVWXYZ',range(1,26+1)): 
  dd[a]=da
  ff[da-1]=a

for _ in range(int(input())):
  s=input().strip()
  arr=szetszed(s+'_')
  if len(arr)==5: # RxCy
    x=int(arr[4])
    num=[]
    while True:
      x-=1
      num.append(x%26)
      x//=26
      if x==0: break
    num.reverse()
    #print(num)
    print(''.join([ff[c] for c in num])+arr[2])
  else: # CCx
    x=0
    for v in arr[1]: x=26*x+dd[v]
    print("R{0:s}C{1:d}".format(arr[2],x))
