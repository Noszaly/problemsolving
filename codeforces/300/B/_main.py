from sys import stdin; input=stdin.readline
n,k=map(int,input().split())
gg=[[] for _ in range(n+1)]
szabad=set(range(1,n+1))
vis=[1]*(n+1)
ans=[]
for _ in range(k):
  a,b=[int(v) for v in input().split()]
  szabad-=set([a,b])
  gg[a].append(b)
  gg[b].append(a)
  vis[a]=vis[b]=0
# print('gg:',gg)
def gen(a,akt):
  for b in gg[a]:
    if vis[b]>0:continue
    # print('a:b',a,b)

    vis[b]=1
    akt.append(b)
    gen(b,akt)

fail=False
for a in range(1,n+1):
  if vis[a]==1: continue
  vis[a]=1
  akt=[a]
  gen(a,akt)
  # print('akt:',akt)
  # print('szabad:',szabad)

  if len(akt)>3: 
    fail=True
    break
  while len(akt)<3 and len(szabad)>0:
    akt+=[szabad.pop()]
  if len(akt)<3: 
    fail=True
    break
  ans.append(' '.join(str(v) for v in akt))

if len(szabad)%3>0:
  fail=True

if fail==False:
  while len(szabad)>0:
    ans.append(' '.join([str(szabad.pop()),str(szabad.pop()),str(szabad.pop())]))

  for v in ans:
    print(v)
else:
  print('-1')