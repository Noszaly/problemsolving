n,p,k=map(int,input().split())
lo=max(p-k,1)
hi=min(n,p+k)
ans=""
if lo>1: ans="<<"
for i in range(lo,p):
  ans+=" "+str(i)
if p>1: ans+=" "
ans+="({0:d})".format(p)
for i in range(p+1,hi+1):
  ans+=" "+str(i)
if hi<n: ans+=" >>"
print(ans)