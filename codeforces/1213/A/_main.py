import sys; input=sys.stdin.readline

n=int(input())
arr=sorted(map(int,input().split()))

ans=-1
p=-1
for i in range(n):
  v=arr[i]
  if v==p: continue
  p=v
  t=sum(abs(v-x)%2 for x in arr)
  if ans<0: ans=t
  else: ans=min(t,ans)
print(ans)

