import sys; input=sys.stdin.readline
n=int(input())
order=map(int,input().split())
dd=[1]*(2*n+1)
akt=0
mx=0
for v in order:
  if dd[v]:
    dd[v]=0
    akt+=1
    mx=max(mx,akt)# lefogyhat kicsire, kell a max
  else:
    akt-=1
print(mx)