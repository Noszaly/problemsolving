from sys import stdin; input=stdin.readline

from collections import deque

n,k=map(int,input().split())
arr=map(int,input().split())
dd=deque()
ss=set()
for v in arr:
  if v not in ss:
    if len(dd)>=k: 
      ss.remove(dd.pop())
    ss.add(v)
    dd.appendleft(v)
print(len(dd))
print(' '.join(str(v) for v in dd))
      
