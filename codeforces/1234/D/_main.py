from sys import stdin; input=stdin.readline

def fenwick(N):
  tree=[0]*(N+1)
  def update(x,val=1):
    nonlocal N
    while x<=N:
      tree[x]+=val
      x+=(x&(-x))
  def query(x):
    ans=0
    while x>0:
      ans+=tree[x]
      x-=(x&(-x))
    return ans
  return (update,query,tree)

s=[0]+list(input().strip())
ns=len(s)
ff=[fenwick(ns) for i in range(26)]
for i in range(1,ns):
  ff[ord(s[i])-ord('a')][0](i,1)

for _ in range(int(input())):
  a,b,c=input().split()
  if a=='1':
    b=int(b)
    if s[b]==c:continue
    ff[ord(s[b])-ord('a')][0](b,-1)
    ff[ord(c)-ord('a')][0](b,1)
    s[b]=c
  else:
    b,c=int(b),int(c)
    r=0
    for i in range(26):
      r+=((ff[i][1](c)-ff[i][1](b-1))>0)
    print(r)
      
