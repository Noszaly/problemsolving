from sys import stdin; input=stdin.readline

n=int(input())
arr=[int(v) for v in input().split()]

M=(1<<22)-1
volt={}
parok={}
for v in arr:
  if volt.get(v)==None: volt[v]=0
  volt[v]+=1

  for w in volt:
    if v!=w:
      x=1
      vw=(v<<22)+w
      if parok.get(vw)!=None: x=parok[vw]
      parok[(w<<22)+v]=x+1
ans=max(volt.values())
if len(parok)>0:
  ans=max(ans,max(parok.values()))
print(ans)