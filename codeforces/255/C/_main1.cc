#include <bits/stdc++.h>
using namespace std;

vector<int> tx;

int frek[1000000+8];
int nums[4000+8];

typedef long long int LLI;

int main(){
  unordered_map<LLI,int,std::hash<LLI>> parok;

  int nn=0;
  int n;scanf("%d",&n);
  for(int i=0;i<n;i++){
    int v;scanf("%d",&v);
    if(++frek[v]==1){
      nums[nn++]=v;
      continue;
    }
    for(int j=0;j<nn;j++){
      int w=nums[j];
      if(v==w){continue;}
      if(frek[w]==1){
        parok[(w<<22)+v]=2;
        continue;
      }
      //printf("%d\n",w);
      auto x=parok.find((v<<22)+w);
      if(x==parok.end()){
        parok[(w<<22)+v]=2;
      }else{
        parok[(w<<22)+v]=1+(x->second);
      }
    }
  }
  int ans=0;
  for(int j=0;j<nn;j++){
    ans=max(ans,frek[nums[j]]);
  }
  for(auto pp:parok){
    ans=max(ans,pp.second);
  }
  printf("%d\n",ans);


  return 0;
}