#include <bits/stdc++.h>
using std::max;
#include <cstdio>

int volt[1000000+8];
int const INF=4000+8;
int first[INF];
int arr[INF];
int frek[INF]={0};
int next[INF];
int merge(int i, int j){
  int ans=0;
  while(i!=INF||j!=INF){
    if(i<j){
      while(i<j){
        i=next[i];
      }
      ans+=1;
    }else{
      while(j<i){
        j=next[j];
      }
      ans+=1;
    }
  }
  return ans;
}


int main(){
  int ndiff=0;
  int n;scanf("%d",&n);
  for(int i=1;i<=n;i++){
    int v;scanf("%d",&v);
    if(volt[v]==0){
      ndiff+=1;
      volt[v]=ndiff;
    }
    frek[volt[v]]+=1;
    arr[i]=volt[v];
  }
  printf("ndiff: %d\n",ndiff);
  for(int i=1;i<=ndiff;i++){first[i]=INF;}
  for(int i=n;i>0;i--){
    int v=arr[i];
    next[i]=first[v];
    first[v]=i;
  }


  int ans=0;
  for(int j=1;j<=ndiff;j++){
    ans=max(ans,frek[j]);
  }
  printf("1. %d\n",ans);
  for(int i=1;i<ndiff;i++){
    for(int j=i+1;j<=ndiff;j++){
      if(frek[i]==1||frek[j]==1){
        ans=max(ans,2);
        continue;
      }
      if(frek[i]+frek[j]<=ans){continue;}
      ans=max(ans,merge(first[i],first[j]));      
    }
  }


  printf("%d\n",ans);


  return 0;
}