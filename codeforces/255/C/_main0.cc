#include <bits/stdc++.h>
using namespace std;

vector<int> tx;

int main(){

  unordered_map<int,int,std::hash<int>> volt;
  typedef unsigned long long int LLI;
  unordered_map<LLI,int,std::hash<LLI>> parok;

  int n;scanf("%d",&n);
  for(int i=0;i<n;i++){
    int v;scanf("%d",&v);
    if(++volt[v]==1){continue;}
    for(auto pp:volt){
      int w=pp.first;
      if(v==w){continue;}
      //printf("%d\n",w);
      auto x=parok.find((v<<22)+w);
      if(x==parok.end()){
        parok[(w<<22)+v]=2;
      }else{
        parok[(w<<22)+v]=1+x->second;
      }
    }
  }
  int ans=0;
  for(auto pp:volt){
    ans=max(ans,pp.second);
  }
  for(auto pp:parok){
    ans=max(ans,pp.second);
  }
  printf("%d\n",ans);


  return 0;
}