from sys import stdin; input=stdin.readline

n=int(input())
arr=[int(v) for v in input().split()]

volt={}
parok={}
for v in arr:
  if volt.get(v)==None: volt[v]=0
  volt[v]+=1

  for w in volt:
    if v!=w:
      x=1
      if parok.get((v,w))!=None: x=parok[(v,w)]
      parok[(w,v)]=x+1
ans=max(volt.values())
if len(parok)>0:
  ans=max(ans,max(parok.values()))
print(ans)