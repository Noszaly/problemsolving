from sys import stdin; input=stdin.readline
n,k=map(int,input().split())
r=[0]*k
for v in input().split():
  r[int(v)%k]+=1
ans=2*(r[0]//2)
i,j=1,k-1
while i<j:
  ans+=2*min(r[i],r[j])
  i+=1
  j-=1
if i==j: ans+=2*(r[i]//2)
print(ans)
