#include <bits/stdc++.h>
using namespace std;
int pw(int n,int const p){
  if(n==0){return 0;}
  int m=0;
  while(n%p==0){ 
    n/=p;
    m+=1;
  }
  return m;
}

string ext(vector<vector<char>> d){
  int i=0,j=0;
  string p;
  while(d[i][j]!='_'){
    if(d[i][j]=='D'){
      p+="D";
      i+=1;
    }else{ 
      p+="R";
      j+=1;
    }
  }
  return p;
}
  
int main(){
  int cin>>n;
  vector<vector<int>> tab(n);

  for(int i=0;i<n;i++){
    for(int j=0;j<n;i++){
      int t; cin>>t;
      tab[i].push_back(t);
    }
  }

  
  vector<vector<int>> tt(n);
  vector<vector<int>> dist(n);
  vector<vector<char>> dirs(n);
  for(int i=0;i<n;i++){
    tt[i].resize(n);
    dist[i].resize(n);
    dirs[i].resize(n);
  }

  opt,path=[],[]
for p in [2,5]:
  for i in range(n):
    for j in range(n):
      tt[i][j]=pw(tab[i][j],p)

  dist[n-1][n-1]=tt[n-1][n-1]
  for j in range(n-2,-1,-1):
    dist[n-1][j]=dist[n-1][j+1]+tt[n-1][j]
    dirs[n-1][j]='R'
  for i in range(n-2,-1,-1):
    dist[i][n-1]=dist[i+1][n-1]+tt[i][n-1]
    dirs[i][n-1]='D'
    for j in range(n-2,-1,-1):
      oR=dist[i][j+1]; oD=dist[i+1][j]
      if oR<oD:
        dist[i][j]=tt[i][j]+oR
        dirs[i][j]='R'
      else:
        dist[i][j]=tt[i][j]+oD
        dirs[i][j]='D'

  opt.append(dist[0][0])
  path.append(ext(dirs)) 
  
if opt[0]<opt[1]:
  print(opt[0])
  print(path[0])
else:
  print(opt[1])
  print(path[1])
