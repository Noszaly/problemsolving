def pw(n,p):
  if n==0: return 0
  m=0
  while n%p==0: 
    n/=p
    m+=1
  return m

tt,dist,dirs=[],[],[]

def ext(d):
  i,j,p=0,0,''
  while d[i][j]!='_':
    p+=d[i][j]
    if d[i][j]=='D':i+=1
    else: j+=1
  return p
  
tab=[]
n=int(input())
for i in range(n):
  tab.append(list(map(int,input().split())))



tt=[[0]*n for _ in range(n)]
dist=[[0]*n for _ in range(n)]
dirs=[['_']*n for _ in range(n)]

opt,path=[],[]
for p in [2,5]:
  for i in range(n):
    for j in range(n):
      tt[i][j]=pw(tab[i][j],p)

  dist[n-1][n-1]=tt[n-1][n-1]
  for j in range(n-2,-1,-1):
    dist[n-1][j]=dist[n-1][j+1]+tt[n-1][j]
    dirs[n-1][j]='R'
  for i in range(n-2,-1,-1):
    dist[i][n-1]=dist[i+1][n-1]+tt[i][n-1]
    dirs[i][n-1]='D'
    for j in range(n-2,-1,-1):
      oR=dist[i][j+1]; oD=dist[i+1][j]
      if oR<oD:
        dist[i][j]=tt[i][j]+oR
        dirs[i][j]='R'
      else:
        dist[i][j]=tt[i][j]+oD
        dirs[i][j]='D'

  opt.append(dist[0][0])
  path.append(ext(dirs)) 
  
if opt[0]<opt[1]:
  print(opt[0])
  print(path[0])
else:
  print(opt[1])
  print(path[1])
