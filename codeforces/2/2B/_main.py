import sys
sys.setrecursionlimit(30000)

def pw(n,p):
  m=0
  while n>0 and n%p==0: 
    n/=p
    m+=1
  return m

t2,t5=[],[]
d2,d5=[],[]
n=-1
INF=1000000
def gen(i,j):
  global n, INF
  if i>=n or j>=n: return INF,INF
  if d2[i][j][0]>=0: return d2[i][j][0],d5[i][j][0]
  o2,o5=t2[i][j],t5[i][j]
  D2,D5=gen(i+1,j)
  R2,R5=gen(i,j+1)
  if D2<R2:
    d2[i][j]=(o2+D2,'D')
  else:
    d2[i][j]=(o2+R2,'R')
  if D5<R5:
    d5[i][j]=(o5+D5,'D')
  else:
    d5[i][j]=(o5+R5,'R')
  return d2[i][j][0],d5[i][j][0]

def ext(d):
  i,j,p=0,0,''
  while d[i][j][1]!='_':
    p+=d[i][j][1]
    if d[i][j][1]=='D':i+=1
    else: j+=1
  return p
  
n=int(input())
for i in range(n):
  ti=list(map(int,input().split()))
  t2.append([pw(ti[j],2) for j in range(n)])
  t5.append([pw(ti[j],5) for j in range(n)])
  
d2=[[(-1,'_')]*n for _ in range(n)]
d2[n-1][n-1]=(t2[n-1][n-1],'_')
d5=[[(-1,'_')]*n for _ in range(n)]
d5[n-1][n-1]=(t5[n-1][n-1],'_')

gen(0,0)
#print(d2)
#print(d5)
opt,path=-1,''
if d2[0][0]<d5[0][0]:
  opt,path=d2[0][0][0],ext(d2)
else:
  opt,path=d5[0][0][0],ext(d5)
print(opt)
print(path)
  
