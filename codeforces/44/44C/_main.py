n,m=map(int,input().split())
jel=[0]*(n+1)
for _ in range(m):
  a,b=map(int,input().split())
  for t in range(a,b+1): jel[t]+=1
i=1
while i<=n and jel[i]==1: i+=1
if i>n:
  print('OK')
else:
  print(i,jel[i])
