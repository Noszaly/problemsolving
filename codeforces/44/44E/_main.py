k,a,b=map(int,input().split())
ss=input()
h=len(ss)
q,r=divmod(h,k)
if q<a or q>b or (q==b and r>0):
  print('No solution')
else:
  i=0
  while r>0:
    print(ss[i:i+q+1])
    r-=1
    i=i+q+1
  while i<h:
    print(ss[i:i+q])
    i=i+q
    
