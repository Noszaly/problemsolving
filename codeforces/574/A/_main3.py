from sys import stdin; input=stdin.readline

narr=int(input())
arr=list(map(int,input().split()))
li=arr[0]
jel={}
mx=li
for v in arr[1:]:
  if v>=li: 
    if jel.get(v)==None:
    jel[v]+=1
    mx=max(mx,v)

while True:
  if mx<li:
    print('0')
    break
  if mx==li:
    print(0+(jel[mx]>0))
    break
  
  c=0
  v=mx
  while li<v:
    if jel[v]==0:
      v-=1
    else:
      jel[v]-=1
      jel[v-1]+=1
      li+=1
      c+=1      
  if li==v: c+=(jel[v]>0)
  print(c)
  break