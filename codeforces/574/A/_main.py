from sys import stdin; input=stdin.readline
narr=int(input())
arr=list(map(int,input().split()))
li=arr[0]
mx=li
nmx=1
for v in arr[1:]:
	if v>mx:
		mx,nmx=v,1
	elif v==mx:
		nmx+=1

if mx<li:
	print('0')
else:
	if mx==li:
		print(0+(nmx>1))
	else:
		print((mx-li)//2+1)

