from sys import stdin; input=stdin.readline
n,k=map(int,input().split())
if k>=n:
  pass
else:
  s=input().strip()
#  print(s)
  si=sorted((s[i],i) for i in range(n))
#  print(si)
  si=si[k:]
  si.sort(key=lambda x:x[1])
#  print(si)
  print(''.join(v[0] for v in si))
