from sys import stdin; input=stdin.readline

n,jmp=map(int,input().split())
#print(n,jmp)
arr=list(input().strip())
#print(arr)
arr[0]=0
for i in range(n-1):
  if type(arr[-1])==int: break
  if type(arr[i])==str: continue
  #print(i,arr)

  v=arr[i]+1
  for j in range(i+1,min(n,i+jmp+1)):
    if arr[j]==int: 
      continue
#      arr[j]=min(arr[j],v)
    elif arr[j]=='1':
      arr[j]=v

if type(arr[-1])==str:
  print('-1')
else:
  print(arr[-1])
