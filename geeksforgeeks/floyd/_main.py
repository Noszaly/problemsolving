# tle
from sys import stdin; input=stdin.readline

INF=10000000
def mstr(x):
  global INF
  if x==INF: return 'INF'
  else: return str(x)

a=[[0]*111 for _ in range(111)]


for _ in range(int(input())):
  n=int(input())
  for i in range(n):
    j=0
    for v in map(int,input().split()): a[i][j]=v;j+=1
  for k in range(n):
    for i in range(n):
      if a[i][k]<INF:
        for j in range(n):
          a[i][j]=min(a[i][j],a[i][k]+a[k][j])
  print('\n'.join(sor for sor in [' '.join(map(mstr,v[:n])) for v in a[:n]]))
