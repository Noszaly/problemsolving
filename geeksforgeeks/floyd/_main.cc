#include <bits/stdc++.h>
using namespace std;
int const INF=10000000;

vector< vector<int> > a(111);
int main(){
  for(int i=0;i<111;i++){
    a[i].resize(111);
  }
  int T;scanf("%d",&T);
  while(T--){
    int n; scanf("%d",&n);
    for(int i=0;i<n;i++){
      for(int j=0;j<n;j++){
        scanf("%d",&a[i][j]);
      }
    }

    for(int k=0;k<n;k++){
      for(int i=0;i<n;i++){
        vector<int>& ai=a[i];
        if(ai[k]==INF){continue;}
        for(int j=0;j<n;j++){
          a[i][j]=min(a[i][j],ai[k]+a[k][j]);
        }
      }
    }
    
    for(int i=0;i<n;i++){
      vector<int>& ai=a[i];
      bool volt=false;
      for(int j=0;j<n;j++){
        if(volt){
          printf(" ");
        }
        volt=true;
        if(ai[j]==INF){
          printf("INF");
        }else{
          printf("%d",ai[j]);
        }
      }
      printf("\n");
    }
    
  }

  return 0;
}
