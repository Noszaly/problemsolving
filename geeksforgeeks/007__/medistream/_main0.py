from heapq import heappush, heappop, heapreplace 
n=int(input())
bal,jobb=[1000000000],[1000000000]
for _ in range(n):
  x=int(input())
  if x>=jobb[0]:
    if len(bal)==len(jobb):
      heappush(bal,-heapreplace(jobb,x))
      print(-bal[0])
    else:
      heappush(jobb,x)
      print((-bal[0]+jobb[0])//2)
  else:
    if len(bal)==len(jobb):
      heappush(bal,-x)
      print(-bal[0])
    else:
      if -bal[0]<=x:
        heappush(jobb,x)
      else:
        heappush(jobb,-heapreplace(bal,-x))
      print((-bal[0]+jobb[0])//2)
      
