from heapq import heappush, heappop 
n=int(input())
mx,mn=[1000000000],[1000000000]
for _ in range(n):
  x=int(input())
  heappush(mx,-x)
  if -mx[0]>mn[0]:
    x=heappop(mx); y=heappop(mn)
    heappush(mx,-y); heappush(mn,-x)
  if len(mx)>len(mn)+1:
    x=heappop(mx); heappush(mn,-x)
  if len(mx)==len(mn):
    print((-mx[0]+mn[0])//2)
  else:
    print(-mx[0])
