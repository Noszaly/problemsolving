for _ in range(int(input())):
  f=[0]*256
  for c in input().strip(): f[ord(c)]+=1
  ff=[f[ord(c)] for c in 'abcdefghijklmnopqrstuvwxyz' if f[ord(c)]>0]
  n=len(ff)
  mini=min(ff);cmini=ff.count(mini)
  maxi=max(ff);cmaxi=ff.count(maxi)
  if cmaxi==n or (cmaxi==1 and maxi-mini==1) or (cmaxi==n-1 and mini==1):print('1')
  else:print('0')
