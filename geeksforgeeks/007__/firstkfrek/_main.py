for _ in range(int(input())):
  n,k=map(int,input().split())
  dd,t={},0
  for v in map(int,input().split()):
    if dd.get(v)==None: t+=1; dd[v]=[0,t]
    dd[v][0]+=1
  mn=[-1,n+11]
  for key in dd: 
    val=dd[key]
    if val[0]==k and val[1]<mn[1]: mn=[key,val[1]]
  print(mn[0])
