b=[ [],
["Alice","Ariel","Aurora","Phil","Peter","Olaf","Phoebus","Ralph","Robin"],
["Bambi","Belle","Bolt","Mulan","Mowgli","Mickey","Silver","Simba","Stitch"],
["Dumbo","Genie","Jiminy","Kuzko","Kida","Kenai","Tarzan","Tiana","Winnie"]]
dd={}
for i in range(1,4):
  for c in b[i]: dd[c]=i

n=int(input())
loc=1
S=0
for _ in range(n):
  t=dd[input().strip()]
  S+=abs(loc-t)
  loc=t
print(S)       