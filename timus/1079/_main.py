arr=[]
npre=-1
while True:
  t=int(input())
  if 0==t:break
  arr.append(t)
  npre=max(npre,t)

pre=[0]*(npre+1)
pre[0],pre[1]=0,1
for i in range(2,npre): 
  ii=i//2 
  pre[i]=pre[ii] 
  if 1==i%2:
    pre[i]+=pre[ii+1] 

for i in range(2,npre+1): 
  pre[i]=max(pre[i-1],pre[i])

for q in arr:
  print(pre[q])  
  