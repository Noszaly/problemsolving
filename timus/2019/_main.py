n=int(input())
ans=[-1]*(n+1)
st=[]
ia=0; iA=0  
for c in input().strip():
  if c==c.upper(): 
    iA+=1
    st.append([ord(c),iA])
  else: 
    ia+=1
    st.append([-ord(c.upper()),ia])

  if len(st)>1 and 0==st[-1][0]+st[-2][0]:
    if st[-1][0]<0: 
      ans[st[-2][1]]=st[-1][1]
    else: 
      ans[st[-1][1]]=st[-2][1]
    st.pop(); st.pop()


#print(st)
if len(st)>0:
  print("Impossible")
else:
  print(" ".join([str(x) for x in ans[1:]]))
