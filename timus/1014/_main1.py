# product of digits, igy mar ok
from itertools import repeat
n=int(input())
ans=str(n)
while True:
  if n==0: ans='10'; break
  if n<10: break
  dig=[0]*10
  for p in [2,3,5,7]:
    while 0==n%p:
      n=n//p
      dig[p]+=1
#  print(dig)

  if n>1:ans="-1"; break

  dig[8],dig[2]=divmod(dig[2],3)
  dig[9],dig[3]=divmod(dig[3],2)
  if dig[2]>0 and dig[3]>0: 
    dig[6]=1
    dig[2]-=1 # itt
    dig[3]-=1
  dig[4],dig[2]=divmod(dig[2],2)
  ans=""
  for d in range(2,10):
    ans=ans+"".join(repeat(str(d),dig[d]))
  break

print(ans)