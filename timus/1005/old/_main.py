def gen(level,asum):
  global opt,S
  if level<0: opt=min(opt,abs(S-2*asum)); return
#  if asum+arr[level]<=S-arr[level]:
  gen(level-1,asum+arr[level])
  gen(level-1,asum)

n=int(input())
arr=list(map(int,input().split()))
S=sum(arr)
#print(sum(arr))
opt=S+1
gen(n-1,0)
print(opt)