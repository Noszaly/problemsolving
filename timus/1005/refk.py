import math
n=int(input())
allk=[int(i) for i in input().split()]
ans=s1=s2=0
for i in range(n):
  ans +=allk[i]

for i in range(2**n):
  s1=s2=0
  ii=i
  for k in range(n):
    if(ii%2==0): s1 +=allk[k]
    else: s2 +=allk[k]
    ii//=2 # itt a hibaja, de amugy tle
  k=math.fabs(s1-s2)
  ans=min(k,ans)
print(int(ans))