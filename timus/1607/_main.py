# examining cases
P,dP,T,dT=map(int,input().split())
if P<=T:
  k,r=divmod(T-P,dT+dP)
  # k kor megvan es P<=T is teljesul:
  P=P+k*dP; T=T-k*dT
  # mintha az elejen lennenk:
  if P+dP>=T:
    print(T)
  else:
    print(P+dP)
else:
  print(P)