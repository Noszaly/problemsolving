#N,K=12,5
N,K=map(int,input().split())
akt=[0]*K
s=0
def gen(lev,i):
  global s
  if lev==K: 
    if akt[0]>1 or akt[K-1]<N:
      #print(akt)
      s+=1
      return
  if i>N: return 
  akt[lev]=i
  gen(lev+1,i+2)
  gen(lev,i+1)


gen(0,1)
print(s)
