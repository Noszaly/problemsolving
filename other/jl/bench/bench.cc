#include <bits/stdc++.h>
using namespace std;

typedef long long int LLI;
struct makef{
  LLI s;
  makef(LLI _s=1):s(_s){}
  LLI operator()(){
    return s=(113*s+51)%77;
  }
};

typedef vector<LLI> vec;
typedef vector<vec> mat;

void ir(vec& v){
  for(auto x:v){
    cout<<x<<" ";
  }
  cout<<"\n";
}

void ir(mat& A){
  for(auto& v:A){
    ir(v);
  }
}


LLI sum(vec& v){
  LLI ret=0;
  for(auto x:v){ret+=x;}
  return ret;
}
LLI sum(mat& A){
  LLI ret=0;
  for(auto& v:A){ret+=sum(v);}
  return ret;
}


int main(){
  auto f=makef(10);
  //~ for(int i=0;i<1000;i++){
    //~ cout<<f(LLI(1))<<"\n";
  //~ }
  //~ return 0;
  
  LLI ret=0;
	for (auto L : {10, 100, 1000}){
		mat A(L); for(int i=0;i<L;A[i++].resize(L));

		int N = 100;
		for (int i = 0; i < N; i++){
      for(auto& v:A){
        for(auto& x:v){
          x=f();
        };
      }
      //ir(A);
			ret+=sum(A);
		}
	}
  cout<<ret<<"\n";
	return 0;
}

