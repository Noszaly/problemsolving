#include <bits/stdc++.h>
using namespace std;

typedef long long int LLI;
typedef vector<LLI> vec;
typedef vector<vec> mat;

void ir(vec& v){
  for(auto x:v){
    cout<<x<<" ";
  }
  cout<<"\n";
}

void ir(mat& A){
  for(auto& v:A){
    ir(v);
  }
}

int main(){
  int const L=3;
  std::vector<std::vector<LLI>> A(L, std::vector<LLI>(L,1));
  A[0][0]=11111;
  for(auto& v:A){
    ir(v);
  }
	return 0;
}

