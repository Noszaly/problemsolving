from timeit import default_timer as timer


def pmul(p,q):
  lp=len(p)
  lq=len(q)
  h=lp+lq-1
  res=[0]*h
  for i in range(lp):
    for j in range(lq):
      res[i+j]+=p[i]*q[j]
  return res


# Karatsuba
# p=p0,...,p2m-1-> 0,...,m-1 +x^m(m,...,2m-1)
# res: 4m-2 deg -> 4m-1 len
# p=pL+x^m*pH
# pq=pL*qL + (pL*qH+pH*qL)x^m + pH*qH x^2m
#     0...2m-2   m  3m-2         2m...2m+2m-2     
# paratlan hossz: (xp'+p0)(xq'+q0)=x^2p'q'+x(p'q0+q'p0)+p0q0 

def kara(p,q):
  n=len(p)
  if n==1: #konstans
    return [p[0]*q[0]]
  if n==2: # linearis
    return [p[0]*q[0],p[0]*q[1]+p[1]*q[0],p[1]*q[1]]
  if n==3: # kvadratikus
    return [p[0]*q[0],p[0]*q[1]+p[1]*q[0],p[0]*q[2]+p[1]*q[1]+p[2]*q[0],p[1]*q[2]+p[2]*q[1],p[2]*q[2]]
  
  # felosztas
  r=n%2
  if r==1: 
    p=[*p,0]
    q=[*q,0]
    n+=1
  m=n//2
  pL=p[:m]
  pH=p[m:]
  qL=q[:m]
  qH=q[m:]
  #print(type(pL))
  LL=kara(pL,qL)
  HH=kara(pH,qH)
  for i in range(m):
    pL[i]+=pH[i]
    qL[i]+=qH[i]
  LH=kara(pL,qL)
  res=[0]*(4*m-1)
  
  # osszekombinalas 
  for i in range(2*m-1):
    res[i]+=LL[i]
    res[i+m]+=(LH[i]-(LL[i]+HH[i]))
    res[i+2*m]+=HH[i]
  if r==1: 
    res.pop() 
    res.pop()
  return res


def karaH(p,q):
  n=len(p)
  if n<32:
    return pmul(p,q)

  # felosztas
  r=n%2
  if r==1: 
    p=[*p,0]
    q=[*q,0]
    n+=1
  m=n//2
  pL=p[:m]
  pH=p[m:]
  qL=q[:m]
  qH=q[m:]
  #print(type(pL))
  LL=karaH(pL,qL)
  HH=karaH(pH,qH)
  for i in range(m):
    pL[i]+=pH[i]
    qL[i]+=qH[i]
  LH=karaH(pL,qL)
  res=[0]*(4*m-1)
  
  # osszekombinalas 
  for i in range(2*m-1):
    res[i]+=LL[i]
    res[i+m]+=(LH[i]-(LL[i]+HH[i]))
    res[i+2*m]+=HH[i]
  if r==1: 
    res.pop() 
    res.pop()
  return res



# p=pE+x*pO
# pq=pE*qE+x(pE*qO+pO*qE)+x^2*pO*qO
def karaH2(p,q):
  n=len(p)
  if n<32:
    return pmul(p,q)

  # felosztas
  m,r=divmod(n,2)
 
  pE=p[::2]
  pO=p[1::2]+[0]*r
  qE=q[::2]
  qO=q[1::2]+[0]*r

  EE=karaH2(pE,qE)
  OO=karaH2(pO,qO)
  for i in range(m):
    pE[i]+=pO[i]
    qE[i]+=qO[i]
  EO=karaH2(pE,qE)
  res=[0]*(4*m-1)
  
  # osszekombinalas 
  for i in range(2*m-1):
    res[2*i]+=EE[i]
    res[2*i+1]+=(EO[i]-(EE[i]+OO[i]))
    res[2*i+2]+=OO[i]
  if r==1: 
    res.pop() 
    res.pop()
  return res



p=[1,1,1,1, 1,1,1,1,  1,1,1,1,1,1,1,1,1,1,11]
q=[1,1,1,1,-1,1,1,1,-10,1,1,1,1,1,1,1,1,1,11]
E=8

P=p[:]
Q=q[:]
for _ in range(E):
  P.extend(P)
  Q.extend(Q)

print("deg=",len(P)[i])

t=timer()
kar=kara(P,Q)
print("kara:",timer()-t)
#print(kar)



P=p[:]
Q=q[:]
for _ in range(E):
  P.extend(P)
  Q.extend(Q)
t=timer()
karH=karaH(P,Q)
print("karaH:",timer()-t)
#print(karH)


P=p[:]
Q=q[:]
for _ in range(E):
  P.extend(P)
  Q.extend(Q)
t=timer()
karH2=karaH2(P,Q)
print("karaH2:",timer()-t)
#print(karH2)



P=p[:]
Q=q[:]
for _ in range(E):
  P.extend(P)
  Q.extend(Q)

t=timer()
sima=pmul(P,Q)
print("sima:",timer()-t)

#print(sima)

print(sima==karH2)
# for i in range(len(sima)):
#   if kar[i]!=sima[i]:
#     print(i,kar[i],sima[i])
#   break
