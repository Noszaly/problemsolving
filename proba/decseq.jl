function seq(x0,x1,n)
  z=[]
  for _ in 2:n
    x1,x0=4x1-x0,x1
    push!(z,x0//x1)
  end
  z
end

println(seq(4,15,5)')
