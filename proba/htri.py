# 2k(2k+1)/2 (2k+1)(2k+2)/2
def ndiv(x):
  ans=2 # 1+x 
  d=2
  while d*d<x:
    if x%d==0: ans+=2
    d+=1
  if d*d==x: ans+=1
  return ans

k=3
while True:
  t=ndiv(2*k+1)
  if ndiv(k)*t>=500: 
    print(2*k,k*(2*k+1))
    break
  if ndiv(k+1)*t>=500: 
    print(2*k+1,(k+1)*(2*k+1))
    break
  k+=2
