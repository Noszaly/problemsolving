from math import sqrt
from fractions import Fraction
from decimal import Decimal
A=870020034000
dig=1

def problem():
  def sqrt(A,prec):
    x=A
    while True:
      nx=(x+A/x)/2
      if abs(x-nx)<prec: break
      x=nx
    return x

  def format(x,dig):
    sx='{:.18f}'.format(float(x))
    ip,fp=sx.split('.')
    if dig:
      return ip+'.'+fp[:dig]
    return ip

  def sol(A,dig):
    return format(sqrt(A,10**(-dig-1)),dig)
  
  return sol
# ~ sA=Lsqrt(Fraction(A,1),Fraction(1,10**(dig+1)))  
# ~ print('{:.18f}'.format(float(sA)))
# ~ print('{:.18f}'.format(sqrt(float(A))))

sol=problem()
print(sol(10,2))
print(sqrt(10))
print(sol(101,2))
print(sqrt(101))
print(sol(8765,8))
print(sqrt(8765))


# ~ sA=Lsqrt(A,10**(-dig-1))  
# ~ print('{:.18f}'.format(float(sA)))
# ~ print('{:.18f}'.format(sqrt(float(A))))



# ~ print(Lformat(sA,dig))
