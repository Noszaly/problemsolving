#include <bits/stdc++.h>
using namespace std;

int main(){
  typedef pair<int,int> IP; //default max(high) priority on the top
  priority_queue<IP> pq;
  
  auto arr={1,2,3,4,3,2,1};
  for(auto v:arr){
    pq.push(IP{v,v-1});
  }
  while(pq.size()>0){
    printf("%d\n",pq.top().first);
    pq.pop();
  }

  return 0;
}
