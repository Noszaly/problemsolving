let
  c=collect∘string
  I(x)=parse(Int,join(x))
  b=fill(0,1<<22)
  h=0
  a=[]
  s=[]
  function p(i)
    if i>h
      S=I(s)
      b[S]=1
      (S%d==0)&&(a=[a;S÷d])
      return
    end
    for j=i:h
      s[[i,j]]=s[[j,i]]
      p(i+1)
      s[[i,j]]=s[[j,i]]
    end
  end
  n,d=4006,2
  a=[n]
  while length(a)>0
    n=pop!(a)
    s=c(n)
    h=length(s)
    b[n]==0&&p(1)
  end
  print(findfirst(isone,b))
end

