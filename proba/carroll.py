from random import randint
# roll will generate numbers (uniformly) between LO,HI (HI is inclusive)
LO,HI=1,10
roll=lambda : randint(LO,HI)


n=4
# generate the participiants of the race
# here the first element is the cumulative point
parti=[[0,"car"+str(i)] for i in range(n)]
print(parti)

R=10 # num of rounds
r=0
while r<R:
  r+=1
  for i in range(n):
    parti[i][0]+=roll()
  # compute the top point
  mx=max(v[0] for v in parti)
  # select the top contestants
  top=','.join([v[1] for v in parti if v[0]==mx])
  # report the state
  print('after the {0:d}. round {1:s} on the top with {2:d} points'.format(r,top,mx))
