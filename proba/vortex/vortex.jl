function load_dat(fname)
  lines = String[]
  open(fname,"r") do f
    for line in eachline(f)
      push!(lines, line)
    end
  end
  nvortex,nsteps = parse.(Int,split(lines[1]))
  delta_t,strength = parse.(Float64,split(lines[2]))
  xyg=zeros(nvortex,3)
  for v = 1:nvortex
    xyg[v, 1:end] = parse.(Float64,split(lines[v+2]))
  end
  nvortex, nsteps, delta_t, xyg
end


function vortex(nvortex,nsteps,delta_t,xyg)
  xy_time = zeros(nsteps, nvortex, 2)
  function find_vel()
    vxy = zeros(nvortex,2)
    for v1 in 1:nvortex,v2 in 1:nvortex
        if v1 == v2 continue end
        dx = xyg[v1,1] - xyg[v2, 1]
        dy = xyg[v1,2] - xyg[v2, 2]
        cf=xyg[v2,3]/(dx^2 + dy^2)
        vxy[v1, 1] += cf*dy
        vxy[v1, 2] -= cf*dx
    end
    return vxy./2pi
  end
  function test()
    vxy_old = zeros(nvortex,2)

    for step=1:nsteps
      vxy = find_vel()
      if step == 1
        vxy_old = vxy
      end
      xyg[:, 1] += 0.5*(vxy[:, 1] + vxy_old[:, 1])*delta_t
      xy_time[step,:,1] = xyg[:,1]
      xyg[:, 2] += 0.5*(vxy[:, 2] + vxy_old[:, 2])*delta_t
      xy_time[step,:,2] = xyg[:,2]

      vxy_old = vxy
    end
  end
  find_vel,test

end
 

@time nvortex, nsteps, delta_t, xyg=load_dat("vortex.dat")
@time _,test = vortex(nvortex, nsteps, delta_t, xyg)
@time test()
@time test()
@time test()
@time test()
@time test()
@time test()


#using PyPlot
#hold(true)
#for v = 1:nvortex 
#  plot(xy_time[:,v,1], xy_time[:,v,2])
#end
#println("Press enter to finish: ")
#readline(STDIN)
