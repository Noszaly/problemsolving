function load_dat(fname)
  lines = String[]
  open(fname,"r") do f
    for line in eachline(f)
      push!(lines, line)
    end
  end
  nvortex,nsteps = parse.(Int,split(lines[1]))
  delta_t,strength = parse.(Float64,split(lines[2]))
  xyg=zeros(nvortex,3)
  for v = 1:nvortex
    xyg[v, 1:end] = parse.(Float64,split(lines[v+2]))
  end
  return nvortex, nsteps, delta_t, xyg
end
 
function find_vel(nvortex,xyg)
  vxy = zeros(nvortex,2)
  for v1 in 1:nvortex,v2 in 1:nvortex
      if v1 == v2 continue end
      dx = xyg[v1,1] - xyg[v2, 1]
      dy = xyg[v1,2] - xyg[v2, 2]
      cf=xyg[v2,3]/(dx^2 + dy^2)
      vxy[v1, 1] += cf*dy
      vxy[v1, 2] -= cf*dx
  end
  return vxy./2pi
end



function test(nvortex, nsteps, delta_t, xyg)
  vxy_old = zeros(nvortex,2)
  xy_time = zeros(nsteps, nvortex, 2)
  for step=1:nsteps
    vxy = find_vel(nvortex,xyg)
    if step == 1
      vxy_old = vxy
    end
    for dim = 1:2 
      xyg[:, dim] += 0.5*(vxy[:, dim] + vxy_old[:, dim])*delta_t
      xy_time[step,:,dim] = xyg[:,dim]
    end
    vxy_old = vxy
  end
end

@time nvortex, nsteps, delta_t, xyg = load_dat("vortex.dat")
@time test(nvortex, nsteps, delta_t, xyg )
@time test(nvortex, nsteps, delta_t, xyg )
@time test(nvortex, nsteps, delta_t, xyg )

#using PyPlot
#hold(true)
#for v = 1:nvortex 
#  plot(xy_time[:,v,1], xy_time[:,v,2])
#end
#println("Press enter to finish: ")
#readline(STDIN)
