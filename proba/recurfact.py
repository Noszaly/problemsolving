def recur_factorial(n, j=0):
    j += 1
    print('j=',j)
    print('n=', n)
    if n == 1:
        return(1)
    else:
        for i in range(2, n):
            print('i=', i)
            return i * recur_factorial(i - 1, j)
    print('reached end of function')

print(recur_factorial(5))
