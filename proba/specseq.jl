function pfact(n)
  ret=Int[]
  d=2
  while d*d<=n
    k=0
    while n%d==0 
      k+=1
      n=div(n,d)
    end
    (k>0)&&push!(ret,k)
    d+=1
  end
  (n>1)&&push!(ret,1)
  sort(ret)
end

K=100
ends_with=fill(0,K) # length 1 seq 
ends_with[1]=1
for k in 2:K
  s=2 # (k) and (1,k)
  for d in 2:K-1
    (k%d==0)&&(s+=ends_with[d])
  end
  ends_with[k]=s
end
for k in 1:K
  println(k," ",ends_with[k]," ",pfact(k))
end
