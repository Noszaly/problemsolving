# nem kell az osszes inverzio
# wa, megis kell?
# nem kell, az elobbi rossz volt, pelda a forrasban

from sys import stdin; input=stdin.readline

T=int(input())
while T>0:
  T-=1
  n=int(input())
  arr=[int(v) for v in input().split()]
  ok=True
  for i in range(n):
    if abs(arr[i]-(i+1))>1: # 0-bazisu
      ok=False
      break
  if ok:
    print('YES')
  else:
    print('NO')
