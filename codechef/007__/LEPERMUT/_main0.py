# fails on:
# 3
# 2 3 1
# local inv=1
# inv=2
# prints 'YES'

#nem kell az osszes inverzio

from sys import stdin; input=stdin.readline

T=int(input())
while T>0:
  T-=1
  n=int(input())
  arr=[int(v) for v in input().split()]
  volt=False
  ok=True
  for i in range(1,n):
    if arr[i]<arr[i-1]:
      if volt:
        ok=False
        break
      arr[i],arr[i-1]=arr[i-1],arr[i]
      volt=True
    else:
      volt=False
  if ok:
    print('YES')
  else:
    print('NO')
