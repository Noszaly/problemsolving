from sys import stdin; input=stdin.readline
T=int(input())
while T>0:
  T-=1
  n,nf=[int(v) for v in input().split()]
  todo=sorted(set(range(1,n+1))-set(int(v) for v in input().split()))
  print(' '.join(str(v) for v in todo[::2]))
  print(' '.join(str(v) for v in todo[1::2]))  