from sys import stdin; input=stdin.readline

T=int(input())
while T>0:
  T-=1
  n=int(input())
  arr=[int(v) for v in input().split()]
  mini=arr[0]
  ans=1
  for v in arr[1:]:
    if v<=mini: 
      ans+=1
      mini=v
  print(ans)
  