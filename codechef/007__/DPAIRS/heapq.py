# https://www.hackerrank.com/challenges/primsmstsub/problem
from heapq import heappop, heappush

nv,ne=map(int,input().split())
gg=[[] for _ in range(nv+1) ]
W=0
for _ in range(ne):
  a,b,w=map(int,input().split())
  gg[a].append((w,b))
  gg[b].append((w,a))
  W+=w
s=int(input())
hh=[]; heappush(hh,(0,s))
intree=[False]*(nv+1)
cost=0; tree=0
while tree<nv:
  wa,a=heappop(hh)
  #print(a,wa)
  if intree[a]:continue
  tree+=1;cost+=wa 
  intree[a]=True  
  for b in gg[a]:
    if intree[b[1]]: continue
    heappush(hh,b)
print(cost)
