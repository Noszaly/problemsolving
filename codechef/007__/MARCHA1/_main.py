from sys import stdin; input=stdin.readline
reach=[0]*20007 # 20*1000

T=int(input())
for t in range(1,T+1):
  n,m=[int(v) for v in input().split()]
  arr=[]
  for _ in range(n):
    arr.append(int(input()))
  M=sum(arr)
  if M>=m:
    reach[0]=t
    for v in arr:
      r=m-v
      while r>=0:
        if reach[r]==t: reach[r+v]=t
        r-=1
    if reach[m]==t:
      print('Yes')
    else:
      print('No')
  else:
    print('No')