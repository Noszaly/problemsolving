from sys import stdin; input=stdin.readline

def binom(n,k):
  if k>n:return 0
  if k==1: return n
  if k==n: return 1
  a,b=1,1
  while k>0:
    b*=k
    a*=n
    n-=1
    k-=1
  return a//b


T=int(input())
while T>0:
  T-=1
  n=int(input())
  n1=input().strip().count('1')
  print(binom(n1+1,2))
