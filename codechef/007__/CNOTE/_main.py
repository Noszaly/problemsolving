from sys import stdin; input=stdin.readline

T=int(input())
while T>0:
  T-=1
  X,Y,K,N=[int(v) for v in input().split()]
  d=X-Y
  ok=False
  for _ in range(N): 
    p,c=[int(v) for v in input().split()] # pages, cost
    if c<=K and p>=d: ok=True
  ok=ok or d<=0
  if ok:
    print('LuckyChef')
  else:
    print('UnluckyChef')