from sys import stdin; input=stdin.readline

T=int(input())
while T>0:
  T-=1
  n=int(input())
  a=[int(v) for v in input().split()]
  b=[int(v) for v in input().split()]
  print(min(sum(a[::2])+sum(b[1::2]),sum(a[1::2])+sum(b[::2])))
