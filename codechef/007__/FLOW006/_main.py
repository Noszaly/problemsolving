from sys import stdin; input=stdin.readline
T=int(input())
while T>0:
	T-=1
	print(sum(int(v) for v in input().strip()))
