from sys import stdin; input=stdin.readline
T=int(input())
while T>0:
	T-=1
	n=int(input())
	ans=1
	while n>1: 
		ans*=n
		n-=1
	print(ans)