A,N,K=[int(v) for v in input().split()]
B=N+1
d=[0]*K
for i in range(K):
  d[i]=A%B
  A//=B
  if A==0:break
print(' '.join(str(v) for v in d))