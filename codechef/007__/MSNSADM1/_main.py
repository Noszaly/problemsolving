from sys import stdin; input=stdin.readline

T=int(input())
while T>0:
  T-=1
  n=int(input())
  go=[int(v) for v in input().split()]
  fa=[int(v) for v in input().split()]
  mx=0
  for i in range(n):
    mx=max(mx,2*go[i]-fa[i])
  print(10*mx)
  