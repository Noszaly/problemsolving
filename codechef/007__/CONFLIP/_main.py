from sys import stdin; input=stdin.readline
T=int(input())
while T>0:
  T-=1
  G=int(input())
  while G>0:
    G-=1
    I,N,Q=[int(v) for v in input().split()]
    ch=N//2+N%2
    if I!=Q:
      print(ch)
    else:
      print(N-ch)