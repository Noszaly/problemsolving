from sys import stdin; input=stdin.readline

T=int(input())
while T>0:
  T-=1
  N,X=[int(v) for v in input().split()]
  arr=[int(v) for v in input().split()]
  s=sum(arr)
  mini=min(arr)
  q,r=divmod(s,X)
  if q==0 or mini<=r:
    print('-1')
  else:
    print(q)