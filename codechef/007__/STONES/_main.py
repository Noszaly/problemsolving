from sys import stdin; input=stdin.readline

T=int(input())
while T>0:
  T-=1
  j=set(input().strip())
  ans=0
  for m in input().strip():
    ans+=(m in j)
  print(ans)