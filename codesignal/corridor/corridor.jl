
function corridorsRepairing(n, cor)
  #n=parse(Int,readline())
  deg=fill(0,n)
  gg=[[] for _ in 1:n]
  dd=Dict()
  for i in 1:n
    #a,b,w=parse.(Int,split(readline()))
    a,b,w=cor[i]
    a+=1;b+=1;
    (a>b)&&((a,b)=(b,a))
    push!(gg[a],b)
    push!(gg[b],a)
    dd[(a,b)]=w
    deg[a]+=1;deg[b]+=1
  end
  #println(gg)

  qq=fill(0,n);tail=1;
  for a in 1:n
    (deg[a]==1)&&(qq[tail]=a;tail+=1)
  end
  head=1
  while head<tail
    a=qq[head];head+=1
    deg[a]=0
    #println(a)
    for b in gg[a]
      deg[b]-=1
      dd[(min(a,b),max(a,b))]=-1
      if deg[b]==1
        qq[tail]=b
        tail+=1
      end
    end
  end
  #println(dd)

  mini,f=1000000000,0
  for v in values(dd)
    (v<0)&&continue 
    if v<mini
      mini=v
      f=1
    elseif v==mini
      f+=1
    end
  end
  f
end

n=6
cor=
[[0,1,2], 
 [1,2,3], 
 [0,2,2], 
 [1,3,1], 
 [2,4,2], 
 [0,5,3]]

#  n=3
#  cor=
#  [[0,1,0], 
#   [1,2,0], 
#   [2,0,0]]

  # n=5
  # cor=
  # [[0,2,27], 
  #  [0,4,90], 
  #  [4,1,43], 
  #  [2,3,47], 
  #  [0,1,94]]

println(corridorsRepairing(n,cor))