# function visiblePoints(p)
#   a=[atan(v[2],v[1]) for v in p]*180/pi
#   n=length(a)
#   a[a.<0].+=360
#   sort!(a)
#   a=vcat(a,a.+360)
#   i,j,m=1,1,0
#   while i<n+1
#     if(a[j]-a[i])>45.0 i+=1 else j+=1;m=max(m,j-i) end
#   end
#   m
# end

visiblePoints(p,n=length(a))=(
  a=sort([v>0 ? v : v+360 for v=[atand(v[2],v[1]) for v=p]]);
  (a,i,j,m)=([a;a.+360],1,1,0);
  while i<n+1
    if(a[j]-a[i])>45.0 i+=1 else j+=1;m=max(m,j-i) end
  end;
  m
)

p=
[[1,1], 
 [3,1], 
 [3,2], 
 [3,3], 
 [1,3], 
 [2,5], 
 [1,5], 
 [-1,-1], 
 [-1,-2], 
 [-2,-3], 
 [-4,-4]]

 println(visiblePoints(p))