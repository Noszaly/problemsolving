# function M(s)
#   d=Dict()
#   function F(i,j)
#     (i>=j)&&(return 0)
#     m=get(d,(i,j),-1)
#     if m<0
#       m=d[(i,j)]=min(1+min(F(i+1,j),F(i,j-1)),s[i]==s[j] ? F(i+1,j-1) : 1000)
#     end
#     m
#   end
# end

# function kpalindrome(s, k)
#   M(s)(1,length(s))<=k
# end

d=Dict() # ha ez kivul van akkor valami nem jo a codesignalon...
F(s,i,j)=
    if i>=j 
      0 
    else
      m=get(d,(i,j),-1);
      (m<0)&&((m=d[(i,j)]=min(1+min(F(s,i+1,j),F(s,i,j-1)),
      s[i]==s[j] ? F(s,i+1,j-1) : 100)));
      m
    end

kpalindrome(s, k)=F(s,1,length(s))<=k

for (s,k) in [
  # ("abrarbra",1),
  # ("adbcdbacdb",2),
  # ("nwnk",1),
  # ("wlrbbmqbhcdarzowkkyhiddqscdxrjmowfrxsjybldbefsarcbynecdyggxxpklorellnmpapqfwkhopkmco",5),
  # ("oqmsboagu",13),
  # ("whsqmgbbuqcljjivswmdkqtbxixmvtrrbljptnsnfwzqfjmafadrrwsofsbcnuvqhffbsaqxwpqca",5),
  # ("pqwuzifwovyddwyvvbu",11),
  # ("hchzvfrkmlnozjk",2),
  ("rdtqkvwcsgs",6),  
  ]
  println(kpalindrome(s,k))
end
