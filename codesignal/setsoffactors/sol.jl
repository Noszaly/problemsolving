# function gen(m,M,sol)
#   for d=M-1:-1:2
#     if m%d==0
#       if d*d>=m push!(sol,[d,m÷d]) end
#       sol=vcat(sol,[[d,v...] for v in gen(m÷d,d,[]) if v[end]>1])
#     end
#   end
#   sol
# end

# function gen(m,p,sol)
#   for d=p:-1:2
#     if m%d==0
#       t=gen(m÷d,d,[])
#       if length(t)>0
#         sol=vcat(sol,[[d,v...] for v in t])
#       end
#     end
#   end
#   if 1<m<=p
#     [[m],sol...]
#   else
#     sol
#   end
# end

# function G(m,p,s)
#   for d=p:-1:2
#     m%d==0 && (s=vcat(s,[[d,v...] for v in G(m÷d,d,[])]))
#   end
#   (1<m<=p) ? [[m],s...] : s 
# end


G(m,p,s)=(
  for d=p:-1:2
    m%d==0 && (s=vcat(s,[[d,v...] for v in G(m÷d,d,[])]))
  end;
  1<m<=p ? [[m],s...] : s )


n=parse(Int,readline())
for v in G(n,n-1,[[n,1]])
  println(v)
end
