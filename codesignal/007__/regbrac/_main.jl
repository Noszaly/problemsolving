function regularBracketSequence1(sequence::String)
  f(x)=if x=='(' 1 else -1 end
  arr=cumsum([f(c) for c in sequence])
  all(arr.>=0)&&arr[end]==0
end