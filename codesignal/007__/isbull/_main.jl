function isBull(adj::Vector{Vector{Bool}})
  any(adj[i][i]==true for i in 1:5)&&(return false)
  function work()
    sa=sum(adj)
    ret=findfirst(x->x==1,sa)
    if ret!=nothing
      for i in 1:5 adj[i][ret]=adj[ret][i]=false end
    end
    ret
  end
  s=Set(1:5)
  setdiff!(s,[work()]); setdiff!(s,[work()])
  (length(s)!=3)&&(return false)
  (a,b,c)=s
  adj[a][b]&&adj[b][c]&&adj[c][a]
end
