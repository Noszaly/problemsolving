function isBull(adj::Vector{Vector{Bool}})
  any(adj[i][i]==true for i in 1:5)&&(return false)
  sort(sum(adj)) in [[1,1,2,2,4],[1,1,2,3,3],[1,2,2,2,3]]
end