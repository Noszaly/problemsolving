  function contoursShifting(matrix)
    r,c=length(matrix),length(matrix[1])
    ret=[fill(0,c) for _ in 1:r ]
    dd=[[0,1],[1,0],[0,-1],[-1,0]]
    fl=1
    ins()=(
      for (i,j) in zip(lay,circshift(lay,fl))
      ret[i[1]][i[2]]=matrix[j[1]][j[2]]
      end)
    sx,sy=1,1
    while sx < r+1-sx && sy < c+1-sy
      lepx,lepy=r+1-2sx,c+1-2sy
      lay=[]
      s=[sx,sy]
      for (d,lep) in zip(dd,[lepy,lepx,lepy,lepx])
        lay=vcat(lay,[s .+ i.*d for i in 1:lep])
        s=last(lay)
      end
      ins()
      sx,sy=sx+1,sy+1
      fl=-fl
    end
    if sx==r+1-sx # ide kell egy ellenorzes meg 
      lay=[[sx,k] for k in sy:c+1-sy]
      ins()
    end
    if sy==c+1-sy
      lay=[[k,sy] for k in sx:r+1-sx]
      ins()
    end

    ret
  end