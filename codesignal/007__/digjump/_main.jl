function digitJumping(grid, start, finish)
  r,c=length(grid),length(grid[1])
  INF=1000000000
  dist=[fill(INF,c) for _ in 1:r]
  dig=[[] for _ in 1:10] 
  for i in 1:r, j in 1:c
    grid[i][j]+=1
    push!(dig[grid[i][j]],(i,j))
  end
  sx,sy=start.+1
  fx,fy=finish.+1
# println(sx,sy)
# println(fx,fy)
  qq=fill((0,0),r*c)
  qq[1]=(sx,sy); dist[sx][sy]=0
  head,tail=1,2
  ins(x,y,d)=(1<=x<=r)&&(1<=y<=c&&d<dist[x][y])&&(dist[x][y]=d;qq[tail]=(x,y);tail+=1)
  while head!=tail
    sx,sy=qq[head]; head+=1
    ((sx,sy)==(fx,fy))&&break
    ad=dist[sx][sy]+1
    ins(sx-1,sy,ad)
    ins(sx+1,sy,ad)
    ins(sx,sy-1,ad)
    ins(sx,sy+1,ad)
    for (x,y) in dig[grid[sx][sy]]
      (ad<dist[x][y])&&(dist[x][y]=ad;qq[tail]=(x,y);tail+=1)
    end
    dig[grid[sx][sy]]=[]
  end
  dist[fx][fy]
end



grid=[[5,3,6]]
start=[0, 1]
finish=[0, 1]

grid=[[0,1,4,2,3], 
 [1,4,2,8,2], 
 [2,2,3,4,9], 
 [8,7,2,2,3]]
start=[0, 0]
finish=[3, 4]


println(digitJumping(grid,start,finish))