function combs(c1::String, c2::String)
  n1,n2=length(c1),length(c2)
  b1,b2=[c=='.' for c in c1],[c=='.' for c in c2]
  b2=vcat(b2,fill(true,n1))
  ans=n1+n2
  for i in 2:n2-1
    all(b1 .| b2[i:i+n1-1])&&(ans=max(n2,i+n1-1);break)
  end
  ans
end