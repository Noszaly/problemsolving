function avoidObstacles(arr)
  arr=sort!(arr)
  n=length(arr)
  mx=arr[n]
  van=zeros(Int8,mx)
  van[arr].=1
  ans=1; 
  while ans<mx
    ans+=1
    if van[ans]>0 continue end
    if any(van[2ans:ans:mx].>0) continue end
    break
  end
  if ans==mx ans+=1 end
  ans
end
