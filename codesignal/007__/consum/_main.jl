# X=k+k+1+....k+m-1=
# km+m(m-1)/2=
# m(k+(m-1)/2)
# 2X=m(2k+m-1)



# function isSumOfConsecutive(n::Int32)
#   println(n)
#   n,m=2n,2
#   while m*(m-1)<n
#     (n%m==0 && (n÷m-m+1)%2==0)&&(println(m," ",n÷m-m+1);return true)
#     m+=1
#   end
#   false
# end

function isSumOfConsecutive(n::Int32)
  n,m=2n,2
  while m*(m-1)<n
    (n%m==0 && (n÷m-m+1)%2==0)&&return true
    m+=1
  end
  false
end


for n in Int32.([9,8,1,42,55,400,128,256])
  println(isSumOfConsecutive(n))
end