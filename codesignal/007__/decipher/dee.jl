# dee decipher@codesignal:
decipher(cipher) = join([Char(parse(Int,m.match)) for m = eachmatch(r"1\d{2}|9\d",cipher)])