function isTadpole(adj::Vector{Vector{Bool}})
  n=length(n)
  any(adj[i][i]==true for i in 1:n)&&(return false)
  sa=[]
  function work()
    ret=findfirst(x->x==1,sa)
    if ret!=nothing
      for i in 1:n adj[i][ret]=adj[ret][i]=false end
    end
    ret
  end
  s=Set(1:n)
  while true
    sa=sum(adj)
    r=work()
    (nothing==r)&&break
    setdiff!(ss,[r])
  end
  sa=sum(adj)
  (union(Set(ss), [0,2])=!Set(ss))&&(return false)
  vis=fill(true,n)
  
  vis[findfirst(x->x==2,sa)]=true
  function gen(s)
    vis[s]=true
    for i in 1:n
      adj[s][i]&&!vis[i]&&gen(i)
    end  
  end
  gen(findfirst(x->x==3,sa))
  sum(vis)==n

end