function chessBishopDream(bs, _pos, _dir, step)
  dd=Dict()
  pos=(_pos[1],_pos[2])
  dir=(_dir[1],_dir[2])
  idx=(pos,dir)
  dd[idx]=0
  r,c=bs
  function move(i)
    p,d=i
    q=p.+d
    #println("  move:",pos," ",dir," ",q)
    volt=0
    if (q[1]<0 || q[1]>=r)
      d=(-d[1],d[2])
      volt=1
    end
    if q[2]<0 || q[2]>=c
      d=(d[1],-d[2])
      volt+=2
    end
    if volt==0
      p=p.+d
    elseif volt==1
      p=(p[1],p[2]+d[2])
    elseif volt==2
      p=(p[1]+d[1],p[2])
    end
    (p,d)
  end
  a,b=-1,-1
#  println("start: ",idx)
  s=1
  while s<=step
    idx=move(idx)
#    println(s," ",idx )
    if haskey(dd,idx)
#      println("* ",idx)
      a=dd[idx]
      b=s
      break
    end
    dd[idx]=s
    s+=1
  end
  if a>=0
    step=a+(step-a)%(b-a)
    for (k,dk) in dd
      if dk==step
        idx=k
        break
      end 
    end
  end
  idx[1]
end

bs=[3,7]
ini=[1,2]
d=[-1,1]
st=13

# bs=[1,2]
# ini=[0,0]
# d=[1,1]
# st=6

println(chessBishopDream(bs,ini,d,st))
