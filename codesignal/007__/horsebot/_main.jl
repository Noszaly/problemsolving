function horsebot(r, c)
  dd=fill(0,r,c)
  qq=fill((0,0),r*c)

  function bfs(a,b,n)
    head,tail=1,2
    function ins(x,y)
      (x<1 || x>r || y<1 || y>c || dd[x,y]>=n)&&return
      dd[x,y]=n
      qq[tail]=(x,y)
      tail+=1
    end
    qq[1]=(1,1)
    dd[1,1]=n
    while head!=tail
      sx,sy=qq[head]; head+=1
      ins(sx-a,sy-b)
      ins(sx-a,sy+b)
      ins(sx+a,sy-b)
      ins(sx+a,sy+b)
      (a==b)&&continue
      ins(sx-b,sy-a)
      ins(sx-b,sy+a)
      ins(sx+b,sy-a)
      ins(sx+b,sy+a)
    end
  end
  sol=0
  n=0
  D=abs(r-c)
  m,M=min(r-1,c-1),max(r-1,c-1)
  for a in 1:m, b in a:M
    n+=1
    (D%gcd(a,b)>0)&&continue
    bfs(a,b,n)
    if dd[r,c]==n
      sol+=1
      println(a," ",b)
    end
  end
  sol
end

for (r,c) in [[7,4],[3,3],[4,3],[4,4],[8,7],[7,2]]
  println(horsebot(r,c))
end