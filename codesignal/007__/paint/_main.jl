function painterBot(canvas, operations, d)
  r,c=length(canvas),length(canvas[1])
  dd=fill(0,r,c)
  qq=fill((0,0),r*c)
  println("d= ",d)

  function bfs(sx,sy,clr,n)
    head,tail=1,2
    qq[1]=(sx,sy)
    oldc=canvas[sx][sy]
    canvas[sx][sy]=clr
    dd[sx,sy]=n
    while head!=tail
      sx,sy=qq[head]; head+=1
      #      println(sx," ",sy)
      sx>1 && dd[sx-1,sy]<n && abs(canvas[sx-1][sy]-oldc)<=d && (dd[sx-1,sy]=n;qq[tail]=(sx-1,sy);tail+=1;canvas[sx-1][sy]=clr) 
      sx<r && dd[sx+1,sy]<n && abs(canvas[sx+1][sy]-oldc)<=d && (dd[sx+1,sy]=n;qq[tail]=(sx+1,sy);tail+=1;canvas[sx+1][sy]=clr) 
      sy>1 && dd[sx,sy-1]<n && abs(canvas[sx][sy-1]-oldc)<=d && (dd[sx,sy-1]=n;qq[tail]=(sx,sy-1);tail+=1;canvas[sx][sy-1]=clr) 
      sy<c && dd[sx,sy+1]<n && abs(canvas[sx][sy+1]-oldc)<=d && (dd[sx,sy+1]=n;qq[tail]=(sx,sy+1);tail+=1;canvas[sx][sy+1]=clr) 
    end
  end
  for n in 1:length(operations)
    println("op: ",operations[n])
    x,y,clr=operations[n]
    bfs(x+1,y+1,clr,n)
  end
  canvas

end

canvas=[[0,1,5,2,4,2,6], 
 [5,2,4,6,2,0,0], 
 [3,3,3,7,8,3,2], 
 [2,1,1,0,0,0,0]]
operations=[[0,0,10], 
 [1,3,3]]
d=3

canvas=[[5]]
operations=[]
d=10

canvas=[[0,20,0,7,1,3,12,0], 
 [3,20,15,7,4,3,6,11], 
 [1,9,11,6,2,13,2,11], 
 [15,17,15,10,7,9,16,9]]
operations=[[0,5,6], 
 [3,6,19], 
 [0,1,13], 
 [0,5,1], 
 [2,2,16], 
 [0,6,13], 
 [0,0,17], 
 [1,1,15], 
 [2,2,13], 
 [2,1,17]]
d=3


println(painterBot(canvas, operations, d))