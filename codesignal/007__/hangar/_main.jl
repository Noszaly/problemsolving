function sabotage(hangar)
  r,c=length(hangar),length(hangar[1])
  state=fill(0,r,c)
  function gen(i,j,l)
    (i<1 || i>r || j<1 || j>c)&&(return 1)
    if state[i,j]==1
      return 1
    end
    if state[i,j]==-1
      return -1
    end
    if state[i,j]==-2 && l>0
      return state[i,j]=-1
    end
    state[i,j]=-2
    if hangar[i][j]=="U"
      ret=gen(i-1,j,l+1)
    elseif hangar[i][j]=="D"
      ret=gen(i+1,j,l+1)
    elseif hangar[i][j]=="L"
      ret=gen(i,j-1,l+1)
    else
      ret=gen(i,j+1,l+1)
    end
    state[i,j]=ret
  end  
  for i in 1:r, j in 1:c
    if state[i,j]==0
      state[i,j]=-2
      ret=gen(i,j,0)
      state[i,j]=ret
    end
  end
  print(state)

  count(x->x==-1,state)  
end


 hangar=[["U","L","D"], 
 ["L","U","D"], 
 ["L","R","L"]]

 hangar=[["U","L"], 
 ["R","L"]]

 hangar=[["D","D","L","L"], 
 ["R","R","D","L"], 
 ["D","U","R","U"], 
 ["R","L","L","L"], 
 ["D","L","R","U"], 
 ["D","D","R","D"], 
 ["L","U","R","D"], 
 ["D","L","R","R"], 
 ["D","U","D","L"]]

 hangar=[["D","U"], 
 ["U","R"], 
 ["L","L"], 
 ["R","U"]]



 println(sabotage(hangar))