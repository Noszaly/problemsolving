function livingOnTheRoads(rr::Vector{Vector{Bool}})
  n=length(rr)
  utak=[]
  for i in 1:n-1, j in i+1:n
    rr[i][j]&&push!(utak,(i,j))
  end
  #println(utak)
  retn=length(utak)
  ret=[fill(false,retn) for _ in 1:retn]
  for i in 1:retn-1, j in i+1:retn
    a,b=utak[i]
    ((a in utak[j])|| (b in utak[j])) &&(ret[i][j]=ret[j][i]=true)
  end
  ret
end