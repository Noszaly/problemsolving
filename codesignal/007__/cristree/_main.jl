function christmasTree(lNum::Int32, lHei::Int32)
  ans=[]
  mx=5+2(lNum-1)+2(lHei-1)
  smx="*"^mx
  mrow(n)=" "^div(mx-n,2)*smx[1:n]
  mvcat(x)=ans=vcat(ans,x)
  ans=[mrow(i) for i in [1,1,3]]
  for s in 5:2:5+2(lNum-1)
    [mrow(ss) for ss in s:2:s+2(lHei-1)] |> mvcat
  end
  [mrow(lHei+1-lHei%2) for _ in 1:lNum] |> mvcat
  print(join(ans,'\n'))
end

# dee:
# christmasTree(n, h) = 
#     [" "^(n+h-1) .* [" *"," *", "***"]
#      [" "^(n+h-i-j) * "*"^(2i+2j+1) for i = 1:n for j = 1:h]
#      [" "^(n+cld(h,2))*"*"^(h|1) for i = 1:n]]