function digitDegree(n::Int32)
  function ds(x)
    local ans
    ans=0
    while x>0
      x,r=divrem(x,10)
      ans+=r
    end
    ans
  end
  ans=0
  while n>9
    println("e:",n)
    n=ds(n)
    println("u:",n)
    ans+=1
  end
  ans
end