function switchLights(a::Vector{Int32})
  a |> reverse |> cumsum |> reverse |> x->div.(x,2) .+ a |> x->div.(x,2)
end