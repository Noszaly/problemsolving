function switchLights(a::Vector{Int})
  a |> reverse |> cumsum |> reverse |> x->mod.(x,2) |> x->xor.(x,a)
end