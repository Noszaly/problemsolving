countTriangles(x,y)=(
z=zip(x,y);
f(i,j)=(i.-j).^2|>sum|>sqrt;
sum(x[1]+x[2]>x[3] for i in z,j in z,k in z x=sort([f(i,j),f(i,k),f(j,k)])))

# using LinearAlgeba
# countTriangles2(x,y)=(z=zip(x,y);f(i,j)=norm((i.-j));s=0;for i in z, j in z, k in z x=sort([f(i,j),f(i,k),f(j,k)]); s+=(x[1]+x[2]>x[3]) end;div(s,6))

x=[0,0,1,1]
y=[0,1,1,0]



println(countTriangles(x,y))