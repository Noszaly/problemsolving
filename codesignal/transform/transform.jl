let
  #n,d=501,3
  n,d=4006,2
  #n,d=321,5
  n,d=39979,9
  n,d=12722,8
  #n,d=10477,4
  m=10^(1+Int(floor(log(n)/log(10))))-1
  q=fill(n,m)
  s=Set(n)
  prev=Dict();prev[n]=-1
  a=[parse(Int,join(sort(digits(v)))) for v=1:m]
  #println(a)
  i,j=1,2
  f(y,x)=!(y in s)&&(length(string(y))≤length(string(x)))&&(push!(s,y);q[j]=y;j+=1;prev[y]=x)
  while i<j
    x=q[i];i+=1
    (x%d==0)&&f(x÷d,x)
    h(y)=f(y,x)
    h.((1:m)[a.==a[x]])
  end
  res=min(s...)
  println(res)
  t=[]
  while res>0
    push!(t,res)
    res=prev[res]
  end
  println(join(t," "))
end
