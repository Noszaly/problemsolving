def ballsRearranging(a):
  a.sort()
  r = 0
  for q in a:
    r += q-len(a) >= a[r]
  return r

arr=[
  [3, 4, 12, 14, 28, 32, 33, 36, 54, 56, 57, 61, 68, 69, 79, 81, 83, 85, 92, 94, 115],
  [6, 18, 32, 76, 86, 93, 99, 100, 109]
]
for v in arr:
  print(ballsRearranging(v))
