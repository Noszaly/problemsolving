let
function ballsRearranging(b)
  sort!(b)
  n=length(b)
  push!(b,b[n]+n+1) #; println(b)
  o=n
  i=1
  while true
    j=findnext(x->x>=b[i]+n,b,i)
    o=min(o,n-j+i) #; println(i," ",j," ",o)
    (j>n)&&break
    i+=1
  end
  o
end


ballsRearranging2(a) = (
    r = 1;
    for q ∈ sort!(a)
        r += q-length(a) >= a[r]
    end;
    r-1
)

function ballsRearranging3(a)
  r = 1
  for q ∈ sort!(a)
      r += q-length(a) >= a[r]
  end
  r-1
end


arr=[
  [3, 4, 12, 14, 28, 32, 33, 36, 54, 56, 57, 61, 68, 69, 79, 81, 83, 85, 92, 94, 115],
  [6, 18, 32, 76, 86, 93, 99, 100, 109]
]
for v=arr
  println(ballsRearranging(v))
  println(ballsRearranging2(v))
  println(ballsRearranging3(v))
end
exit(0)

using StatsBase
N=100
for i=1:100
  n=rand(10:20)
  a=sample(1:N,n,replace=false)
#inp=[2,5,4]
#inp=[2, 27, 16, 5, 21, 3, 25, 28, 7, 10]
  (ballsRearranging(a)!=ballsRearranging3(a))&&(println(a);break)
end
end