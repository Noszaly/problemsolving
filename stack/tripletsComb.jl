using Combinatorics
H(l)=[Set(v for v in combinations(sort(l),3)if sum(v) in 2v)...]
#H(l)=collect(Set(v for v in combinations(sort(l),3) if sum(v) in 2v))
#using Combinatorics;H(l)=[Set(v for v in combinations(sort(l),3) if sum(v) in 2v)...]
println("hossz=",length("using Combinatorics
H(l)=[Set(v for v in combinations(sort(l),3) if sum(v) in 2v)...]"))



for l in eachline(stdin)
  ans=sort(["("*join(v," ")*")" for v in parse.(Int,split(l)) |> H ])
  println(join(ans))
end
println(H([]))