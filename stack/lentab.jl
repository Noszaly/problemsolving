#F(l)=sort([(v,[w for w in l if length(w)==v]) for v in Set(length.(l))])
G(l)=(X=length.(l);sort([(v,[l[X .==v]]) for v in Set(X)]))
H(l,L=length.(l))=sort([(v,l[L.==v]) for v in Set(L)])


data=[
  ["Code","Golf","and","Coding","Challenges","Meta"],
  split("My v3ry 3xc3113nt m0th3r ju5t 53rv3d u5 nin3 pizz@5 #JusticeForPluto",' ')
]

for v in data
  println(H(v))
end