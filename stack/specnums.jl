let
  nums=[big(0),1,1,1,1,1,1,1,1,1]
  rules=[ # rules for prev number
    [0,1,2,3,4,5,6,7,8,9], 
    [0],
    [0,2],
    [0,3], 
    [0,2,4],
    [0,5],
    [0,2,3,6],
    [0,7],
    [0,2,4,8],
    [0,3,9]
  ]

  tmp=fill(big(0),10)
  n=10^4
  for _ in 2:n
    #println(nums,"   ",sum(nums))
    for d in 0:9
      tmp[d+1]=sum(nums[rules[d+1].+1])
    end
    tmp,nums=nums,tmp
  end
  println(nums,"   ",sum(nums))
end
# 10,...,90
# -
# 22,
# 33,
# 24,44
# 55
# 26,36,66
# 77
# 28,48,88
# 39,99


