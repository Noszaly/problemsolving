
nums=[0,1,1,1,1,1,1,1,1,1]
rules=[ # rules for prev number
  [0,1,2,3,4,5,6,7,8,9], 
  [0],
  [0,2],
  [0,3], 
  [0,2,4],
  [0,5],
  [0,2,3,6],
  [0,7],
  [0,2,4,8],
  [0,3,9]
]

tmp=[0]*10
n=10
for _ in range(n):
  for d in range(10):
    tmp[d]=sum(nums[rules[d]])
  tmp,nums=nums,tmp
print(nums)