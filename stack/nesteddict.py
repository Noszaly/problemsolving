all_cap = {"cap_1":{"id":1001, "vitals":[("Temp", 101), ("HR", 60)]}, "cap_2": 
{"id":1002, "vitals":[("Temp", 104), ("HR", 60), ("RR", 12)]}}

def insertOrUpdate(ll,tup):
  found=None
  k=tup[0]
  for a,b in ll:
    if a==k:
      found=(a,b)
      break
  if found!=None: 
    ll.remove(found)
  ll.append(tup)

insertOrUpdate(all_cap['cap_1']['vitals'], ('HR',111))
insertOrUpdate(all_cap['cap_1']['vitals'], ('RR',121212))
print(all_cap)
