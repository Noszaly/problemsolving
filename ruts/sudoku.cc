#define _DBG( x )

// sudoku 
#include <cstdio>

int const K = 3 ;
int const N = K * K ;
char 
  tabla[ N ][ N ] ,
  sor[ N ][ N + 1 ] , 
  oszlop[ N ][ N + 1 ] , 
  negyzet[ N ][ N + 1 ] ;

bool tovabb ;


void olvas( char const URES = '0' )
{
  int i = 0 , j = 0 , k = 0 ;
  while( k < N * N )
  {
    if( j == N ) { j = 0 ; i++ ; }
    int c = getchar() ;
    if( ( '1' <= c ) && ( c <= '9') )
    {
      tabla[ i ][ j++ ] = c - '0' ; // 1..9
      k++ ;
      continue ;
    }
    if( URES == c )
    {
      tabla[ i ][ j++ ] = 0 ;
      k++ ;
      continue ;
    }
  }
}

// atvaltja i,j-t a negyzet k koordinatajaba (ez sorfolytonosan 0...8)
//int ij2ni( int i , int j )
//{
  //return K * ( i / K ) + ( j / K ) ;
//} 

void init()
{
  for( int i = 0 ; i < N ; i++ )
  {
    for( int c = 1 ; c <= N ; c++ )
    {
      sor[ i ][ c ] = oszlop[ i ][ c ] = negyzet[ i ][ c ] = 1 ;
    }
  }

  for( int i = 0 ; i < N ; i++ )
  {
    int ni = K * ( i / K ) ;
    for( int j = 0 ; j < N ; j++ )
    {
      int c = tabla[ i ][ j ] ;
      if( c > 0 ) // elore megadott ertek
      {
        sor[ i ][ c ] = oszlop[ j ][ c ] = negyzet[ ni + ( j / K ) ][ c ] = 0 ;
      }
    }
  }
  
}

void ir()
{
  for( int i = 0 ; i < N ; i++ )
  {
    if( i % 3 == 0 ) printf( "\n" ) ;
    for( int j = 0 ; j < N ; j++ )
    {
      if( j % 3 == 0 ) printf( " " ) ;
      printf( "%c" , tabla[ i ][ j ] + '0' ) ;
    }
    printf( "\n" ) ;
  }
}




void gen( int k )
{
  
  if( k == N * N ) // megvan egy
  {
    tovabb = false ;
    ir() ;
    return ;
  }
  
  int i = k / N ;
  int j = k % N ;
_DBG( 
printf("%d %d\n" , i , j ) ;
) ;
  
  if( tabla[ i ][ j ] > 0 ) 
  {
    gen( k + 1 ) ;
    return ;
  }
  
  int ni = K * ( i / K ) + ( j / K ) ;
  for( int c = 1 ; c <= 9 ; c++ )
  {
    if( sor[ i ][ c ] && oszlop[ j ][ c ] && negyzet[ ni ][ c ] )
    {
      sor[ i ][ c ] = oszlop[ j ][ c ] = negyzet[ ni ][ c ] = 0 ;
      tabla[ i ][ j ] = c ;
      gen( k + 1 ) ;
      if( false == tovabb ) { return ; }
      sor[ i ][ c ] = oszlop[ j ][ c ] = negyzet[ ni ][ c ] = 1 ;
      tabla[ i ][ j ] = 0 ;
    }
  }
}


int main( int npar , char** par )
{ 
  char URES = '.' ; // default, a tablazatos olyan hogy "pont==ures hely"
  if( npar > 1 ) { URES = par[ 1 ][ 0 ] ; }


  olvas( URES ) ; 
  //ir() ;
  init() ;
  tovabb = true ;
  gen( 0 ) ;
  
  return 0 ;
}