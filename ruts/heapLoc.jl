let
  # heap with delete functionality
  function makeHeapLoc(N,T,lt) #lt=less than
    h=Array{T,1}(undef,N)
    idx=Dict{T,Int}()
	  n=0
	  function _push(v)
      haskey(idx,v) && idx[v]!=-1 && return
      n+=1
      h[n]=v; idx[v]=n
      _down(_up(n))
	  end
	  function _up(k)
	    while k>1
	      kk=div(k,2)
	      !lt(h[k],h[kk]) && break
	      h[k],h[kk]=h[kk],h[k] 
        idx[h[k]],idx[h[kk]]=idx[h[kk]],idx[h[k]]
	      k=kk
	    end
	    k
	  end
	  function _down(k)
	    while k<n
	      kk=2k; 
	      kk>n && break
	      kk+1<=n && lt(h[kk+1],h[kk]) && (kk+=1)
	      !lt(h[kk],h[k]) && break
        h[k],h[kk]=h[kk],h[k] 
        idx[h[k]],idx[h[kk]]=idx[h[kk]],idx[h[k]]
	      k=kk
	    end
	    k
	  end
	  function _peek()
	    if n>0 h[1] else nothing end
	  end
	  function _pop()
      0==n && return 
      idx[h[1]]=-1
      n-=1
      0==n && return
      idx[h[n+1]]=1
      h[1]=h[n+1]
      _down(1)  
	  end
	  function _del(v)
      nothing==v && return
      (!haskey(idx,v) || (-1==idx[v])) && return
	    iv=idx[v]
	    idx[v]=-1
	    n-=1
	    iv==n+1 && return
      h[iv]=h[n+1]
      idx[h[n+1]]=iv #forget about setting it
      _down(iv)
	  end
	  function _println()
	    println(h[1:n])
	  end
    _push,_peek,_pop,_del,_println
  end


# QHEAP1@hackerrank
  Q=parse(Int,readline())
  pu,pe,po,de,pr=makeHeapLoc(Q,Int,<)
  for _ in 1:Q
    arr=parse.(Int,split(readline()))
    if 1==length(arr)
      println(pe())
      continue
    end
    if 1==arr[1]
      pu(arr[2])
      continue
    end
    de(arr[2])
#    pr()
  end


end

