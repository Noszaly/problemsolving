# codechef/CSUMD
M=1000000007
def mkfib():
  A=[[0,1],[2,2]]
  B=[]
  def mul(X,Y):
    ret=[[0,0],[0,0]]
    ret[0][0]=(X[0][0]*Y[0][0]+X[0][1]*Y[1][0])%M
    ret[0][1]=(X[0][0]*Y[0][1]+X[0][1]*Y[1][1])%M
    ret[1][0]=(X[1][0]*Y[0][0]+X[1][1]*Y[1][0])%M
    ret[1][1]=(X[1][0]*Y[0][1]+X[1][1]*Y[1][1])%M
    return ret
  def f(n):
    nonlocal B
    if n==1:
      B=[v[:] for v in A]
      return
    f(n//2)
    B=mul(B,B)
    if n%2>0: B=mul(B,A)
  def getB(): return B
  return f,getB

T=int(input())
while T>0:
  T-=1
  n=int(input())-1
  if n<5:
    print([1,3,8,22,60][n])
  else:
    f,getB=mkfib()
    f(n)
    A=getB()
    print((A[0][0]+A[0][1]*3)%M)
    