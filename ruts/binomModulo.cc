#include <bits/stdc++.h>
using namespace std;
int const M=1000000007;
typedef unsigned long long int LLI;

LLI prod(LLI a,LLI b){//0<a<=b
  LLI ans=a;
  while(++a<=b){ans=(ans*a)%M;}
  return ans;
}
LLI mpow(LLI a,int n){
  if(n==0){return 1;}
  if(n==1){return a;}
  LLI x=mpow(a,n>>1);
  x=(x*x)%M;
  if(n&1){x=(x*a)%M;}
  return x;
}

//~ template<typename T> void dbg(string s,T& v){
  //~ cerr<<s<<v<<"\n";
//~ }
LLI binom(LLI N,LLI K){
  if(K>N){return 0;}
  if(K==0 || K==N){return 1;}
  if(K==1 || K==(N-1)){return N%M;}
  LLI num=prod(N-K+1,N);
  LLI den=prod(1,K);
//~ dbg("num:",num);
//~ dbg("den:",den);
  
  return (num*mpow(den,M-2))%M;
}

int main(){
  std::ios::sync_with_stdio( false ) ;
  int T; cin>>T;
  while(T--){
    vector<int> books;
    unordered_map<int,int,hash<int>> dd;
    int n,B; cin>>n>>B;
    int allb=0;
    for(int i=0;i<n;i++){
      int p,k;cin>>p>>k;
      if(k==0){continue;}
      if(dd.find(p)==dd.end()){
        dd[p]=0;
        books.push_back(p);
      }
      allb+=1;
      dd[p]+=1;
    }
    //for(const auto v:books){nb+=dd[v];}
    LLI ans=0;
    if(allb>=B){
      sort(books.begin(),books.end());
      reverse(books.begin(),books.end());
      int sb=0,psb=0,isb=0;
      while(true){
        psb=sb;
        sb+=dd[books[isb]];
        if(sb>=B){break;}
        isb+=1;
      }
      ans=1;
      if(sb>B){
        int N=dd[books[isb]];
        int K=B-psb;
        //~ dbg("N: ",N);
        //~ dbg("K: ",K);
        
        ans=binom(N,K); //mod M
      }
    }
    cout<<ans<<"\n";
  }

  return 0;
}
