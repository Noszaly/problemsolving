def gcd(a,b):
  while True:
    c=a%b
    if 0==c: break
    a,b=b,c
  return b

def egcd(a,b):
  xa,xb=[1,0],[0,1]
  while True:
    q,c=divmod(a,b)
    if 0==c: break
    xc=[xa[0]-q*xb[0],xa[1]-q*xb[1]]
    xa,xb=xb,xc
    a,b=b,c
  return b,xb

a=int(input())
b=int(input())
x=int(input())
lcm=(a*b)//gcd(a,b)
print(1+(x-1)//lcm)