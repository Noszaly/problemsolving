from sys import stdin
# input=stdin.readline
def lList(B,N,v):
  beg,end=[-1]*B,[-1]*B
  arr=[[v,0] for _ in range(N)]
  E=-1
  def ins(b,x):
    nonlocal E
    E+=1
    if end[b]<0:
      beg[b]=end[b]=E
      arr[E]=[x,-1]
    else:
      arr[end[b]][1]=E
      arr[E]=[x,-1]
      end[b]=E
  def gen(b):
    i=beg[b]
    while i>=0:
      j=arr[i][1]
      yield arr[i][0]
      i=j   
  return ins,gen
      
N=int(stdin.readline())
B=100
ins,gen=lList(B+1,N,-1)
for line in stdin:
  i,p=line.split()
  ins(int(p),int(i))
for p in range(B,-1,-1):
  for i in gen(p): print(i,p)
