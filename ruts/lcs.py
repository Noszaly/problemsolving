# abbreviation @ hackerrank
# longest common subsequence
# tle on test 13

def precheck(src,tar):
  oA=ord('A')
  f1=[0]*26
  for v in tar:# kisbetu a tar-ban
    if v.islower(): return -1,False
    f1[ord(v)-oA]+=1
  f2=f1[:]
  ncsrc=0
  for v in src: # sok nagybetu az src-ben?
    if v.isupper(): 
      f2[ord(v)-oA]-=1
      ncsrc+=1
  if any(v<0 for v in f2): return -1,False
  tsrc=[] # egyszerusites
  for v in src:
    if v.isupper() or f2[ord(v.upper())-oA]>0: tsrc.append(v)
  src=tsrc
  if len(src)<len(tar): return -1,False
  for v in src: # van-e eleg az src-ben
    f1[ord(v.upper())-oA]-=1
  if any(v>0 for v in f1): return -1,False
  return ncsrc,True
  
plus=lambda x,y: (x[0]+y[0],x[1]+y[1])
T=int(input())
for _ in range(T):
  src=list(input())
  tar=list(input())
  ans='NO'
  ncsrc,pre=precheck(src,tar)
  if True==pre:    
    ntar=len(tar)
    nsrc=len(src)
    tar=['_',*tar]
    src=['_',*src]
    akt,prev=[(0,0)]*(nsrc+1),[(0,0)]*(nsrc+1)
    for i in range(1,ntar+1):
      prev,akt=akt,prev
      ti=tar[i]
      for j in range(1,nsrc+1):
        sj=src[j]
        tmp=max(prev[j],akt[j-1])
        if ti==sj: 
          tmp=max(tmp,plus(prev[j-1],(1,0)))
        elif ti==sj.upper():
          tmp=max(tmp,plus(prev[j-1],(1,-1)))
        akt[j]=tmp
    tmp=akt[nsrc]
    if tmp[0]==ntar and tmp[0]+tmp[1]==ncsrc: ans="YES"
    
  print(ans)
