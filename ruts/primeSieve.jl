  function pSieve(N)
    sieve=fill(Int8(1),N)
    sieve[4:2:N] .= 0
    sieve[1]=0
    p=3
    while p*p<=N
      if 1==sieve[p]
        sieve[p*p:2p:N] .= 0
      end
      p+=2
    end
    sieve
  end

  function pSieve(N) # modified for distinct factors
    sieve=fill(1,N)
    sieve[4:2:N] .= 2
    sieve[1]=0
    p=3
    while p*p<=N
      if 1==sieve[p]
        sieve[p*p:2p:N] .= p
      end
      p+=2
    end
    for p in 4:N
      d=sieve[p]
      (1==d)&&continue
      pp=p
      while 0==pp%d
        pp=pp÷d
      end
      sieve[p]=sieve[pp]+1
    end
    sieve
  end



function pSieve(N) # modified for Euler's phi
  sieve=fill(1,N)
  sieve[4:2:N] .= 2
  
  n=3
  while n*n<=N
    if 1==sieve[n]
      sieve[n*n:2n:N] .= n
    end
    n+=2
  end

  sieve[1]=1 # by def it is zero, but it is needed for multiplicative prop.
  #sieve[2]=1
  sieve[3]=2
  for n in 4:N
    p=sieve[n]
    (1==p)&&(sieve[n]=n-1;continue)
    
    pk=1
    nn=n÷p
    while 0==nn%p
      nn=nn÷p
      pk=pk*p
    end
    sieve[n]=sieve[nn]*pk*(p-1)
  end
  sieve
end

