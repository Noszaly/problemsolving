function mkCum(x)
	cx=cumsum(x)
	lx=length(x)
	function cum(i)
		(i<1)&&(return 0)
		(i>lx)&&(return cx[lx])
		cx[i]
	end
end
