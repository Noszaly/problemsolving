from sys import stdin; input=stdin.readline

tree=[]
N=0
def update(x,val):
  global tree,N
  while x<=N:
    tree[x]+=val
    x+=(x&(-x))
def query(x):
  ans=0
  while x>0:
    ans+=tree[x]
    x-=(x&(-x))
  return ans

ng=int(input())
narr=int(input())
arr=[]
mx=0
for _ in range(narr):
  t=int(input())
  mx=max(mx,t)
  arr.append(t)
tree=[0]*(mx+1)
N=mx
for x in range(1,mx+1): update(x,1)

update(arr[0],-1)
ok=1
for i in range(1,narr):
  x=arr[i]
  k=query(x)
  if 0==k: break
  ok+=1
  a=1; b=x;
  while a<b:
    mid=(a+b)//2
    if k<=query(mid): b=mid
    else: a=mid+1
  update(b,-1)    
print(ok)  
