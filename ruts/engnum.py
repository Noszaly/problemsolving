
dd={
0 : "",
1 : "one",     11 : "eleven",      30 : "thirty",           1000 : "thousand", 
2 : "two",     12 : "twelve",      40 : "forty",            
3 : "three",   13 : "thirteen",    50 : "fifty",            
4 : "four",    14 : "fourteen",    60 : "sixty",            
5 : "five",    15 : "fifteen",     70 : "seventy",          
6 : "six",     16 : "sixteen",     80 : "eighty",           
7 : "seven",   17 : "seventeen",   90 : "ninety",           1000000 : "million",
8 : "eight",   18 : "eighteen",    100 : "hundred",      
9 : "nine",    19 : "nineteen",    
10 : "ten",    20 : "twenty",      1000000000 : "billion" }

#print(dd)

ddd={}
for i in range(20):
  ddd[i]=len(dd[i])
for i in range(20,100,10):
  for j in range(10):
    ddd[i+j]=len(dd[i])+ddd[j]
ddd[100]=len("hundred")

pr=lambda x: print(*x,sep='',end='')
def sab(x):
  x=str(x)
  nx=len(x)
  ezres=[]
  for v in range(nx,0,-3):
    ezres.append(int(x[max(0,v-3):v]))
  nezres=len(ezres)
  nam=["","thousand","million","billion"]
  i=nezres
  while i>0:
    i-=1
    if 0==ezres[i]: continue
    q=ezres[i]//100
    if q>0: pr([dd[q],"hundred"])
    r=ezres[i]%100
    if r<20: pr([dd[r]])
    else: pr([dd[10*(r//10)],dd[r%10]])
    pr(nam[i])
  print()

def sabnum(x):
  x=str(x)
  nx=len(x)
  ezres=[]
  for v in range(nx,0,-3):
    ezres.append(int(x[max(0,v-3):v]))
  nezres=len(ezres)
  #nam=["","thousand","million","billion"]
  nam=[0,8,7,7]
  ans=0
  i=nezres
  while i>0:
    i-=1
    if 0==ezres[i]: continue
    q=ezres[i]//100
    if q>0: ans+=(ddd[q]+ddd[100])
    r=ezres[i]%100
    if r<20: ans+=ddd[r]
    else: ans+=(ddd[10*(r//10)]+ddd[r%10])
    ans+=nam[i]
  return ans


num=int(input())
while True:
  num=sabnum(num)
  print(num)
  if num==4: break
