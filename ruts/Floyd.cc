// a getint()-et is a print()-et kivettem
#include <bits/stdc++.h>
using namespace std;

using LLI_=long long int;
LLI_ const INF=LLI_(1000000000000);
using P_=pair<LLI_,int>;

#define V_(T_) vector<T_>
#define x_ first
#define y_ second
#define pb_ push_back
LLI_ D[501][501];

int main(){
  int N,E,nq; 
  N=getint();
  E=getint();
  nq=getint();
  //V_(V_(LLI_)) D(N+1);



  memset(D, 0x0F, sizeof D);
  for(int a=1;a<=N;a++){
    //D[a].resize(N+1,INF);
    D[a][a]=0;
  }

  for(int i=0;i<E;i++){
    int a,b,w;
    a=getint();
    b=getint();
    w=getint();
    if(w<D[a][b]){
      D[a][b]=D[b][a]=w;
    }
  }

  
  for(int k=1;k<=N;k++){
    for(int a=1;a<=N;a++){
      //LLI_ dak=D[a][k];
      for(int b=1;b<=N;b++){
        D[a][b]=min(D[k][b]+D[a][k],D[a][b]);
      }
    }
  }

  for(int q=0;q<nq;q++){
    int a,b;
    a=getint();
    b=getint();
    LLI_ d=D[a][b];
    if(d<INF){
      print(d);
      puts("");
    }else puts("-1");
    //printf("%lld\n",d<INF ? d : -1);
  }

  return 0; 
}
