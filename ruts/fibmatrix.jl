let
  x=[1 2 4]'
  A=[0 1 0; 0 0 1; 1 1 1]
  M=10^9+7
  function pow(n)
    if n>1
      B=(pow(n÷2)^2).%M
      (n%2>0)&&(B=(B*A).%M)
      B
    else
      #A[:,:]
      A
    end
  end

  readline()
  s=0
  for v=parse.(Int,split(readline()))
    s+= if v>3 (pow(v-1)*x)[1]%M else x[v] end
  end  
  println(s%M)

end