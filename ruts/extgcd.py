import sys; input=sys.stdin.readline

def egcd(a,b):
  xa,xb=(1,0),(0,1)
  while True:
    q,c=divmod(a,b)
    if 0==c: break
    xa,xb=xb,(xa[0]-q*xb[0],xa[1]-q*xb[1])
    a,b=b,c
  return (b,xb)

