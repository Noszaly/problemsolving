# prob49

let
  function primeGen(N)
    pr=fill(0,N)
    pr[1:5].=[2,3,5,7,11]
    npr=5

    delta=4
    x=13
    while true
      isp=true
      y=2
      while pr[y]^2<=x
        if x%pr[y]==0
          isp=false
          break
        end
        y+=1
      end
      
      if true==isp
        npr+=1
        pr[npr]=x
        if npr==N
          break
        end
      end
    
      x+=delta
      delta=6-delta
    end  
    pr
  end

  N=10000
  prs=primeGen(N)
  println(pr[N])
end
