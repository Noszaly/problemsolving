#prob26
using OffsetArrays

# x<y, expansion of x/y in base B
function expand(x,y,B=10)
  dig=[]
  rep=(0,0)
  used=OffsetArray(fill(0,B*y),0:B*y-1)
  k=1
  r=x
  while true
    r=r*B
    if used[r]>0
      rep=(used[r],k-1)
      break
    end
    used[r]=k
    q,r=divrem(r,y)
    push!(dig,q)
    k+=1
  end
  dig,rep
end

let
  opt=(d=0,len=0)
  for y in 2:999
    #expand(1,d,10) |> println
    (_,(a,b))=expand(1,y)
    if b-a+1>opt.len
      opt=(d=y,len=b-a+1)
    end
  end
  opt|>println
end

expand(1,983,10)