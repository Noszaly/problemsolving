function mkSearch(x)
	function search(a,b,k)
		(k>=x[b])&&(return b+1)
		(k<=x[a])&&(return a)
		while a<b
			m=div(a+b,2)
			a,b=if k<=x[m] a,m else m+1,b end
		end
		b
	end
end
