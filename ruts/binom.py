def binom(n,k):
  if k>n:return 0
  if k==1: return n
  if k==n: return 1
  a,b=1,1
  while k>0:
    b*=k
    a*=n
    n-=1
    k-=1
  return a//b

n=int(input())
print(binom(n,5)+binom(n,6)+binom(n,7))
 