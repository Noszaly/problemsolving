template<typename T> struct Fenwick{
	int aN;
	vector<T> tree;
	void init(int _aN){//aN<=N
		aN=_aN;
		if(aN+1>tree.size()){
			tree.resize(aN+1);
		}
		fill(tree.begin(),tree.begin()+aN+1,0);
	}
	T query(int x){
		T ans=0;
		while(x>0){
			ans+=tree[x];
			x-=(x&(-x));
		}
		return ans;
	}
	void update(int x, const T val=1){
		while(x<=aN){
			tree[x]+=val;
			x+=(x&(-x));
		}
	}
};

Fenwick<int> bit;
//init+update+query
