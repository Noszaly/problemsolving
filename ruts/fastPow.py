def mkPow(MOD):
  def pw(x,n):# x<MOD
    if 0==n: return 1
    if 1==n: return x
    ans=pw(x,n//2)
    ans=ans*ans
    if n%2>0: ans*=x
    return ans%MOD # 
  return pw
pw=mkPow(111)
print(pw(99,2323))
