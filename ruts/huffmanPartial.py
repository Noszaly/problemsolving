fa=[[-1,-1]]
def ins(seq,s,c=None):
  ls=len(seq)
  vege=len(fa)
  f=0
  while True:
    if len(fa[f])==1:break
    if s==ls: break
    v=int(seq[s]); s+=1
    if fa[f][v]<0:
      fa.append([-1,-1])
      fa[f][v]=vege
      vege+=1
    f=fa[f][v]
  if c!=None: 
    fa[f]=[c]
  return fa[f],s
    
N=int(input())
for _ in range(N):
  c,seq=input().split()
  ins(seq,0,c)

#print(fa)

msg=[]
seq=input().strip()
s=0
while s<len(seq):
  c,s=ins(seq,s)
  msg.append(*c)
print(''.join(msg))
