bool isPrime(int x){
  if(x<2){return false;}
  if(x<4){return true;}
  if(0==x%2 || 0==x%3){return false;}
  int d=5;
  while(d*d<=x){
    if(0==x%d){return false;}
    d+=2;
  }
  return true;
}