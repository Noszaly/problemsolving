# adott egy permutáció. 
# keressük meg a tőle nagyobbal közül 
# a legkisebb fixpontmentes permutációt

function mkGen(p)
  n=length(p)
  q=fill(0,n)
  used=fill(false,n)
  ok=false
  function find()
    gen(1,false)
    return ok,q
  end
  function gen(lev,nov)
    if lev==n+1 
      ok=nov||ok
      return
    end
    pl=p[lev]
    i=pl
    if nov i=1 end 
    while i<=n && !ok
      if i!= lev && false==used[i]
        used[i]=true
        q[lev]=i
        if i>pl 
          gen(lev+1,true) 
        else
          gen(lev+1,nov)
        end 
        used[i]=false
      end
      i+=1
    end
  end
  return find
end

# sample: StatsBase