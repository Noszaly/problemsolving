INF=10**9
n,tar=[int(v) for v in input().split()]
coin=sorted(int(v) for v in input().split())
if tar<coin[0]:
  print(-1)
else: #1->greedy, not imp.
  arr=[INF]*(tar+1)
  arr[0]=0
  for v in coin: arr[v]=1
  for v in range(2,tar+1):
    if arr[v]<INF: continue
    mn=INF
    for c in coin:
      if c>v: break
      mn=min(arr[v-c]+1,mn)
    arr[v]=mn
  print(-1 if arr[tar]==INF else arr[tar])
