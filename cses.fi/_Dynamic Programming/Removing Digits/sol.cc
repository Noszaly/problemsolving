// Removing Digits
#include <bits/stdc++.h>
using namespace std;

int main(){

  int n; scanf("%d",&n);
  vector<int> dd(n+1,0);
  vector<int> qq(n+1);
  qq[0]=n;
  int head=0,tail=1;
  while(head<tail){
    int h=qq[head];
    head+=1;
    if(0==h){break;}
    int d=dd[h]+1;
    int hh=h;
    while(hh>0){
      int r=hh%10;
      if(r>0 && dd[h-r]==0){ 
        dd[h-r]=d;
        qq[tail]=h-r;
        tail+=1;
      }
      hh/=10;
    }
  }
  printf("%d\n",dd[0]);

  return 0;
}