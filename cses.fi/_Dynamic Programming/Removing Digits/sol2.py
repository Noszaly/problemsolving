# Removing Digits
n=int(input())
arr=[1]*(n+1)
arr[0]=0
for i in range(10,n+1):
  mn,ii=1000000000,i
  while ii>0:
    r=ii%10
    if r>0: mn=min(mn,arr[i-r])
    ii//=10
  arr[i]=1+mn
print(' '.join(str(v) for v in arr))
