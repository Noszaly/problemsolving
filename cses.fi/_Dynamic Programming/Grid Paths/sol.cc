// Grid Paths
#include <bits/stdc++.h>
using namespace std;
main(){
  int n,i,j; cin>>n;
  string L;
  vector<int> A(n,0), P(n,0);
  A[0]=1;
  for(i=0;i<n;i++){
    cin>>L;
    swap(A,P);
    for(j=0;j<n;j++)
      if(L[j]=='*')
        A[j]=0;
      else
        A[j]=(P[j]+(j>0 ? A[j-1] : 0))%1000000007;
  }
  cout<<A[n-1];
}
