// sorrend szamit
#include <bits/stdc++.h>
using namespace std;

int main(){
  int const MOD=1000000007;
  int nc,tar; scanf("%d%d",&nc,&tar);
  vector<int> coin(nc);
  int tn=0;
  for(int i=0;i<nc;i++){
    int t;scanf("%d",&t);
    if(t>tar){continue;}
    coin[tn++]=t;
  }
  nc=tn;
  sort(coin.begin(),coin.begin()+nc);
  auto it=unique(coin.begin(),coin.begin()+nc);
  nc=int(it-coin.begin());

  if(tar<coin[0]){
    printf("0\n");
    return 0;
  }
  //reverse(coin.begin(),coin.begin()+nc);

  vector<int> arr(tar+1,0);
  arr[0]=1;

  for(int i=0;i<tar;i++){
    int ai=arr[i];
    if(ai==0){continue;}
    for(int j=0;j<nc;j++){
      int cj=coin[j];
      if(i+cj>tar){break;}
      arr[i+cj]=(arr[i+cj]+ai)%MOD;
    }
  }

  printf("%d\n",arr[tar]);

	return 0;
}
