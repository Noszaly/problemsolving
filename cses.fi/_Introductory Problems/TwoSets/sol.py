P=print
def w(n,x,i,j):
  P(n)
  if len(x)>0:
    P(x,end='')
  while i<j:
    P('',i,j,end='')    
    i+=2
    j-=2
  P()

n=int(input())
if n%4 in [1,2]:
  P("NO")
else:
  P("YES")
  if n%4==3:
    w(n//2+1,'1 2',4,n)    
    w(n//2,'3',5,n-1)    
  else:
    w(n//2,'1 {}'.format(n),3,n-2)    
    w(n//2,'2 {}'.format(n-1),4,n-3)    
