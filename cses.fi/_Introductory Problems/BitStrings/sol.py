def mkPow(M):
  def pw(x,n):# x<MOD
    if 0==n: return 1
    if 1==n: return x
    ans=pw(x,n//2)
    ans=ans*ans
    if n%2>0: ans*=x
    return ans%M 
  return pw
print(mkPow(10**9+7)(2,int(input())))
