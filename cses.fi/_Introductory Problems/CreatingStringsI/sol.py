s=sorted(input())
ns=len(s)
akt=['_']*ns
fog=[0]*ns
nout,out=0,[]
def gen(lev):
  global nout,out,akt
  if lev==ns:
    nout+=1
    out+=[''.join(akt)]
    return
  p='_'
  for i in range(ns):
    if s[i]==p or fog[i]>0: continue
    p=s[i]
    fog[i]=1
    akt[lev]=p
    gen(lev+1)
    fog[i]=0

gen(0)
print('\n'.join([str(nout),*out]))
