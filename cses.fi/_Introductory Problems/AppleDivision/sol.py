n=int(input())
p=sorted([int(v) for v in input().split()],reverse=True)
s=sum(p)
s2=s//2
opt=10**10
def gen(lev,akt,m):
  global opt
  if lev==n or akt>=s2:
    opt=min(opt,abs(s-2*akt))
    return
  gen(lev+1,akt+p[lev],m)
  if m-p[lev]>=s2:
    gen(lev+1,akt,m-p[lev])
gen(0,0,s)
print(opt)
