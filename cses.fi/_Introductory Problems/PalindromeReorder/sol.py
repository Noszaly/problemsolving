s=input().strip()
#print(s)
f=[0]*26
for c in s: f[ord(c)-65]+=1
m=-1
for i in range(26):
  if f[i]%2==1:
    if m>-1:
      m=-10
      break
    else:
      m=i
if m<-1:
  print('NO SOLUTION')
else:
  if m>-1: f[m]-=1
  n=len(s)
  o=[' ']*n
  i,j=0,n-1
  for k in range(26):
    v,c=f[k],chr(k+65)
    while v>0: 
      o[i],o[j]=c,c
      i+=1
      j-=1
      v-=2
  if m>-1:
    o[n//2]=chr(m+65)
  print(''.join(o))
