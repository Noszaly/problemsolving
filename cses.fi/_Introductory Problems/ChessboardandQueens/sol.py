t=[input() for _ in range(8)]
c=[1]*8
d=[1]*16
a=[1]*16
n=0
def g(i):
  global n
  if i>7: 
    n+=1
    return
  k,m=i,i+7
  for j in range(8):
    if (t[i][j]=='.')*c[j]*d[k]*a[m]:
      c[j]=d[k]=a[m]=0
      g(i+1)
      c[j]=d[k]=a[m]=1
    k+=1
    m-=1
g(0)
print(n)
