t=[input() for _ in range(8)]
c=[1]*8
d=[1]*16
a=[1]*16
n=0
def g(i):
  global n
  if i>7: 
    n+=1
    return
  for j in range(8):
    if (t[i][j]=='.')*c[j]*d[i+j]*a[i-j+7]:
      c[j]=d[i+j]=a[i-j+7]=0
      g(i+1)
      c[j]=d[i+j]=a[i-j+7]=1
g(0)
print(n)
