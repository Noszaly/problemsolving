tab=[input() for _ in range(8)]
col=[1]*8
d1=[1]*16
d2=[1]*16
nsol=0
def gen(i):
  global nsol
  if i==8: 
    nsol+=1
    return 
  for j in range(8):
    if tab[i][j]=='.' and col[j]>0 and d1[i+j]>0 and d2[i-j+7]:
      col[j]=d1[i+j]=d2[i-j+7]=0
      gen(i+1)
      col[j]=d1[i+j]=d2[i-j+7]=1
gen(0)
print(nsol)
