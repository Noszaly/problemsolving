# https://cses.fi/problemset/task/1727/
# T-szer feldobva egy F-oldalu "kockat", mennyi a kapott szamok maximumanak 
# varhato erteke?
 
def solve(t,f):
#  f-sum(exp.(t*(log.(1:f-1).-log(f))))
  return f-sum((v/f)**t for v in range(1,f))
 
 
T,F=[int(v) for v in input().split()]
print("{:.6f}".format(solve(T,F)))