// Exponentiation
#include <bits/stdc++.h>
using namespace std;
using LLI=long long int;

int fexp(int a,int b){
  if(b==0){return 1;}
  if(b==1){return a;}
  int t=fexp(a,b/2);
  t=(LLI(t)*t)%1000000007;
  if(b%2){t=(LLI(t)*a)%1000000007;}
  return t;
}
int main(){
  int n;scanf("%d",&n);
  while(n--){
    int a,b;scanf("%d%d",&a,&b);
    printf("%d\n",fexp(a,b));
  }
  return 0;
}
