R=range
N,l,u=[int(v) for v in input().split()]
A,B=[0]*7*N,[0]*7*N
for i in R(1,7):
  A[i]=1/6

for n in R(2,N+1):
  for s in R(n,n*6+1):
    B[s]=sum(A[s-k] for k in R(1,7) if s-k>=n-1)/6
  A,B=B,A
print("{:.6f}".format(sum(A[l:u+1])))


