#include <bits/stdc++.h>
using namespace std;
#define ff first
#define ss second
int main(){

  int n=20;
  vector<int> A(n);
  for(int i=0;i<n;i++){
    A[i]=i;
    printf("%d ",i);
  }
  printf("\n");


  int x=3;
  for(int k=0;k<10;k++){
    auto beg=next(A.begin(),k);
    auto fnd=lower_bound(beg,A.end(),x);
    if(fnd==A.end() || *fnd!= x){
      printf("fail\n");
    }else{
      printf("%d found at %d.\n",x,int(fnd-A.begin()));
    }
  }

  return 0;
}