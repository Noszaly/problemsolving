#include <bits/stdc++.h>
using namespace std;

#define RC getchar_unlocked
int getnum(){
  int c;
  while((c=RC())<'0' || c>'9');
  int x=c-'0';
  while((c=RC())>='0' && c<='9'){
    x=10*x+c-'0';
  }
  return x;
}


#define ff first
#define ss second
int main(){
  int n=getnum();
  int x=getnum();
  typedef long long int LLI;
  typedef pair<int,int> mp;
  vector<mp> A(n);
  for(int i=0;i<n;i++){
    int a=getnum();
    A[i]=mp({a,i+1});
  }
  sort(A.begin(),A.end());
  int mx=A[n-1].ff;

  bool ok=false;
  int ii,jj,kk;
  int vi,vj;
  for(int i=0;i<n-2;i++){
    if(ok==true){break;}
    ii=A[i].ss;vi=A[i].ff;
    if(3*LLI(vi)>x){break;}
    if(vi+2*LLI(mx)<x){continue;}
    for(int j=i+1;j<n-1;j++){
      jj=A[j].ss;vj=A[j].ff;
      if(vi+LLI(2*vj)>x){break;}
      if(vi+vj+LLI(mx)<x){continue;}

      auto it=lower_bound(next(A.begin(),j+1),A.end(),mp({x-vi-vj,-1}));
      if(x==(vi+vj+(it->ff))){
        ok=true;
        kk=it->ss;
        goto VEGE;
        break;
      }
    }
  }
  VEGE:
  if(ok==true){
    printf("%d %d %d\n",ii,jj,kk);
  }else{
      printf("IMPOSSIBLE\n");
  }

  return 0;
}
