# MaximumSubarraySum
n=int(input())
a=[int(v) for v in input().split()]
l=a[0]
g=l
for v in a[1:]:
  l=max(v,l+v)
  g=max(g,l)
print(g)