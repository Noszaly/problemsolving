nA,nB,d=[int(v) for v in input().split()]
mind=[0]*(2*nA+nB)
m=0
for a in input().split():
  a=int(a)
  mind[m]=(1+((10**9+a-d)<<2)); m+=1
  mind[m]=(3+((10**9+a+d)<<2)); m+=1
for b in input().split():
  b=int(b)
  mind[m]=(2+((10**9+b)<<2)); m+=1
mind.sort()
ok,akt,felh=0,0,0
for v in mind:
  if v&3==1:   # nyit
    akt+=1 
    continue
  if v&3==2:    # elem
    if felh<akt:
      felh+=1
      ok+=1
    continue
  akt-=1         # zar
  if felh>0: felh-=1 # egyforma hosszu (nincs egymasba agyazott intervallum)
print(ok)