#include <bits/stdc++.h>
using namespace std;

#define __getchar getchar_unlocked
int getnum(){
  int c;
  while((c=__getchar())<'0' || c>'9');
  int x=c-'0';
  while((c=__getchar())>='0' && c<='9'){
    x=10*x+c-'0';
  }
  return x;
}


int main(){
  int n=getnum();
  typedef pair<int,int> mp;
  auto arr=vector<mp>(2*n);

  for(int i=0;i<n;i++){
    int a=getnum();
    int b=getnum();
    arr[2*i]=mp({a,1});
    arr[2*i+1]=mp({b,0});
  }
  sort(arr.begin(),arr.end());
  int akt=0,mx=0;
  for(int i=0; i<2*n; i++){
    if(arr[i].second>0){
      akt+=1;
      if(akt>mx){mx=akt;}
    }else{
      akt-=1;
    }
  }
  printf("%d\n",mx);

  return 0;
}
