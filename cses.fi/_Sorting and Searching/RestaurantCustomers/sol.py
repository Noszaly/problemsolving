# RestaurantCustomers
n=int(input())

mind=[(0,0)]*(2*n)
for i in range(n):
  a,b=[int(v) for v in input().split()]
  mind[2*i]=(a,1)
  mind[2*i+1]=(b,-1)
mind.sort()
akt,mx=0,0
for v in mind:
  if v[1]>0:
    akt+=1
    if akt>mx: mx=akt
  else:
    akt-=1

print(mx)
