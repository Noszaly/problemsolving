n=int(input())
a=[int(v) for v in input().split()]
mx,akt,nakt=1,set([a[0]]),1
i=0
for v in a[1:]:
  if not v in akt:
    akt.add(v)
    nakt+=1
    mx=max(mx,nakt)
  else:
    ki=[]
    while a[i]!=v:
      ki.append(a[i])
      i+=1
    i+=1
    nakt-=len(ki)
    #akt=akt.difference(ki)
    akt.difference_update(ki)
print(mx)
