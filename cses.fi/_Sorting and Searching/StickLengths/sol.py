# StickLengths
n=int(input())
a=sorted(int(v) for v in input().split())
me=a[(n-1)//2]
print(sum(abs(v-me) for v in a))
