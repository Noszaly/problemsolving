#include <bits/stdc++.h>
using namespace std;

#define __getchar getchar_unlocked
int getnum(){
  int c;
  while((c=__getchar())<'0' || c>'9');
  int x=c-'0';
  while((c=__getchar())>='0' && c<='9'){
    x=10*x+c-'0';
  }
  return x;
}


int main(){
  int n=getnum();
  typedef pair<int,int> mp;
  auto arr=vector<mp>(n);

  for(int i=0;i<n;i++){
    int a=getnum();
    int b=getnum();
    arr[i]=mp({b,a});
  }
  sort(arr.begin(),arr.end());
  int p=-1,ok=0;
  for(int i=0; i<n; i++){
    if(arr[i].second>=p){
      p=arr[i].first;
      ok+=1;
    }
  }
  printf("%d\n",ok);

  return 0;
}
