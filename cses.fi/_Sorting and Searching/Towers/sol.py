import bisect
input()
l=[]
L=0
for v in [int(u) for u in input().split()]:
  i=bisect.bisect_right(l,v)
  if i>=L:
    l+=[v]
    L+=1
  else:
    l[i]=v
print(L)
