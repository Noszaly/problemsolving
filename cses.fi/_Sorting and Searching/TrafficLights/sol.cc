// TrafficLights
// io nem szamit
#include <bits/stdc++.h>
using namespace std;
#define __getchar getchar_unlocked
int getnum(){
  int c;
  while((c=__getchar())<'0' || c>'9');
  int x=c-'0';
  while((c=__getchar())>='0' && c<='9'){
    x=10*x+c-'0';
  }
  return x;
}

int main(){
  //int x,n; scanf("%d%d",&x,&n);
  int x=getnum(),n=getnum(); 
  vector<int>p(n);
  for(int i=0;i<n;i++){
    p[i]=getnum();
  }
  typedef pair<int,int> mp;
  auto inter=set<mp>();
  auto len=set<mp>();
  inter.insert(mp({x,0})); // jobb-bal sorrend
  len.insert(mp({x,x}));
  for(int i=0;i<n;i++){
    int t=p[i];
    auto fnd=inter.lower_bound(mp({t,-1}));
    int b=fnd->first, a=fnd->second;
    inter.erase(fnd);
    inter.insert(mp({t,a}));
    inter.insert(mp({b,t}));
    len.erase(mp({b-a,b}));
    len.insert(mp({t-a,t}));
    len.insert(mp({b-t,b}));
    printf("%d ",len.rbegin()->first);
  }
  printf("\n");

  return 0;
}
