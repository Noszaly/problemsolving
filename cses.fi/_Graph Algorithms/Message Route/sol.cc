#include <bits/stdc++.h>
using namespace std;
#define V vector<int>
#define P push_back

main(){
  int N,E,a,b,h,t=1;
  cin>>N>>E;
  vector<V> G(N+1);
  for(h=0;h<E;h++){
    cin>>a>>b;
    G[a].P(b);
    G[b].P(a);
  }
  V F(N+1,0);
  V Q(N);
  Q[h=0]=N;
  F[N]=-1;
  while(h<t&&!F[1]){
    a=Q[h++];
    for(auto b:G[a])
      if(!F[b]){
        F[b]=a;
        Q[t++]=b;
      }
  }

  if(F[1]){
    V& T=G[0];
    T.P(h=1);
    while((h=F[h])>0) T.P(h);
    cout<<T.size()<<'\n';
    for(auto b:T) cout<<b<<' ';
    cout<<'\n';
  }else
    cout<<"IMPOSSIBLE\n";
}
