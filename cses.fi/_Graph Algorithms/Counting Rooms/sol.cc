#include <bits/stdc++.h>
using namespace std;

char T[1008][1008];
int h,w,i,j,s=0;
void G(int x,int y){
  if(!(x<0||x>=h||y<0||y>=w||T[x][y]=='#')){
    T[x][y]='#';
    G(x-1,y);
    G(x+1,y);
    G(x,y-1);
    G(x,y+1);
  }
}
main(){
  scanf("%d%d",&h,&w);
  for(i=0;i<h;i++)
    scanf("%s",T[i]);    
  
  for(i=0;i<h;i++)
    for(j=0;j<w;j++)
      if(T[i][j]=='.'){
        s++;
        G(i,j);
      }
  
  printf("%d\n",s);
}