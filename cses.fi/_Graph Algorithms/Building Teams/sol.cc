#include <bits/stdc++.h>
using namespace std;
#define __getchar getchar_unlocked
int getnum(){
  int c;
  while((c=__getchar())<'0' || c>'9');
  int x=c-'0';
  while((c=__getchar())>='0' && c<='9'){
    x=10*x+c-'0';
  }
  return x;
}
char o[200022];
main(){
  int N,E,a,b,h,t=1,i,f;
  N=getnum();E=getnum();
  vector<vector<int>> G(N+1);
  for(i=0;i<E;i++){
    a=getnum();b=getnum();
    G[a].push_back(b);
    G[b].push_back(a);
  }
  vector<int> F(N+1,0),Q(N);
  for(i=1;i<=N&&t;i++)
    if(!F[i]){
      F[Q[h=0]=i]=t=1;
      while(h<t){
        a=Q[h++];
        f=F[a];
        for(int b:G[a]){
          if(F[b]==f){t=0;break;}
          if(!F[b]){
            F[b]=3-f;
            Q[t++]=b;
          }
        }
      }
    }

  if(t){
    t=0;
    for(i=1;i<=N;i++){
      if(F[i]==1){
        o[t++]='1';
      }else{
        o[t++]='2';
      }
      o[t++]=' ';
    }
    o[t-1]=0;
    puts(o);
  }
  else
    puts("IMPOSSIBLE");

}
