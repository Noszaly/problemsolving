//kiszedni az osszes egyfokut (2-core marad)
#include <bits/stdc++.h>
using namespace std;
#define __getchar getchar_unlocked
int getnum(){
  int c;
  while((c=__getchar())<'0' || c>'9');
  int x=c-'0';
  while((c=__getchar())>='0' && c<='9'){
    x=10*x+c-'0';
  }
  return x;
}
main(){
  int N,E,a,b,h,t,i,f,m;
  N=getnum();E=getnum();
  vector<vector<int>> G(N+1);
  vector<int> D(N+1,0);
  for(i=0;i<E;i++){
    a=getnum();b=getnum();
    G[a].push_back(b);
    G[b].push_back(a);
    ++D[a];++D[b];
  }
  vector<int> F(N+1,0),Q(N+1);
  t=0;m=N;
  for(a=1;a<=N;a++){
    if(D[a]<2){F[a]=-1;m--;}
    if(D[a]==1)Q[t++]=a;
  }
  h=0;
  while(h<t){
    a=Q[h++];
    for(int b:G[a]){
      if(!F[b]){
        if(--D[b]==1){F[b]=-1;Q[t++]=b;m--;}
      }
    }
  }
  
  // for(i=1;i<=N;i++){
  //   printf("%d ",D[i]);
  // }printf("\n");
  // for(i=1;i<=N;i++){
  //   printf("%d ",F[i]);
  // }printf("\n");
  
  if(m>2){
    a=1;while(F[a])a++;
    F[a]=1;//D-beli sorszam
    D[1]=a;
    t=2;h=0;
    while(!h){
      for(auto b:G[a]){
        if(F[b]>0&&F[b]<t-2){
          D[t]=b;
          h=F[b];
          break;
        }
        if(!F[b]){
          F[b]=t;
          a=D[t]=b;
          t+=1;
          break;
        }
      }
    }
    printf("%d\n",t-h+1);
    while(h<=t){
      printf("%d ",D[h]);
      h+=1;
    }
    printf("\n");
  }
  else
    printf("IMPOSSIBLE\n");

}
