// Labyrinth
#include <bits/stdc++.h>
using namespace std;

typedef pair<int,int> mp;
#define ff first
#define ss second

int const N=1008;
char tab[N][N];
char sol[N*N];
mp A,B;
int n,m;
void keres(int c,mp& cc){
  for(int i=0;i<n;i++){
    for(int j=0;j<m;j++){
      if(tab[i][j]==c){
        cc.ff=i;
        cc.ss=j;
        return;
      }
    }
  }
}
bool probal(int i,int j, int c){
  if(i<0||i>=n||j<0||j>=m||tab[i][j]!='.'){return false;}
  tab[i][j]=c;
  return true;
}

int main(){
  scanf("%d%d",&n,&m);
  for(int i=0;i<n;i++){
    scanf("%s",tab[i]);
  }
  keres('A',A);
  keres('B',B);
  int Ai=A.ff, Aj=A.ss;
  tab[Ai][Aj]='.';

  // printf("%d %d\n",Ai,Aj);
  // printf("%d %d\n",B.ff,B.ss);
  vector<mp> qq(n*m);
  qq[0]=B;
  tab[B.ff][B.ss]='_';
  bool ok=false;
  int head=0,tail=1;
  while(head<tail){
    mp& h=qq[head];head+=1;
    int hi=h.ff, hj=h.ss;
    if(hi==Ai && hj==Aj){
      ok=true;
      break;
    }
    if(probal(hi+1,hj,'U')){
      qq[tail++]=mp({hi+1,hj});
    }
    if(probal(hi-1,hj,'D')){
      qq[tail++]=mp({hi-1,hj});
    }
    if(probal(hi,hj-1,'R')){
      qq[tail++]=mp({hi,hj-1});
    }
    if(probal(hi,hj+1,'L')){
      qq[tail++]=mp({hi,hj+1});
    }
  }

  if(ok==true){
    printf("YES\n");
    int nsol=0;
    while(true){
      int c=tab[Ai][Aj];
      if(c=='_'){break;}
      sol[nsol++]=c;
      switch(c){
        case('U'):{
          Ai-=1;
          break;
        }
        case('D'):{
          Ai+=1;
          break;
        }
        case('L'):{
          Aj-=1;
          break;
        }
        case('R'):{
          Aj+=1;
          break;
        }
      }
    }
    sol[nsol]=0;
    printf("%d\n%s\n",nsol,sol);

  }else{
    printf("NO\n");
  }

  return 0;

}
