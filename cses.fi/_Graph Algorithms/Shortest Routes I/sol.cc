#include <bits/stdc++.h>
using namespace std;
using LLI_=long long int;
using P_=pair<LLI_,int>;

#define V_(T_) vector<T_>
#define x_ first
#define y_ second
#define pb_ push_back

int main(){
  int N,E; scanf("%d%d",&N,&E);
  V_(V_(P_)) G(N+1);
  for(int i=0;i<E;i++){
    int a,b,w;scanf("%d%d%d",&a,&b,&w);
    G[a].pb_(P_({w,b}));
  }
  V_(bool) ok(N+1,false);
  V_(LLI_) D(N+1,numeric_limits<LLI_>::max());
  priority_queue<P_,V_(P_),greater<P_>> H; // minHeap
  H.push(P_({0,1}));
  D[1]=0;
  int nok=0;
  while(1){    
    auto t=H.top(); H.pop();
    LLI_ d=t.x_;int a=t.y_;
    if(!ok[a]){
      ok[a]=true;
      if(++nok==N){break;}
    }else{
      continue;
    }
    for(auto s:G[a]){
      LLI_ w=s.x_;int b=s.y_;
      if(d+w<D[b]){
        D[b]=d+w;
        H.push(P_({d+w,b}));
      }
    }
  }
  for(int a=1;a<=N;a++){
    printf("%lld ",D[a]);
  }printf("\n");

  return 0; 
}
