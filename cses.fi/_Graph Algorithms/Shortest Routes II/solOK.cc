#include <bits/stdc++.h>
using namespace std;
using LLI_=long long int;
LLI_ const INF=LLI_(1000000000000);
using P_=pair<LLI_,int>;

#define V_(T_) vector<T_>
#define x_ first
#define y_ second
#define pb_ push_back

int main(){
  int N,E,nq; scanf("%d%d%d",&N,&E,&nq);
  V_(V_(LLI_)) D(N+1);
  for(int a=1;a<=N;a++){
    D[a].resize(N+1,INF);
    D[a][a]=0;
  }
  for(int i=0;i<E;i++){
    int a,b,w;scanf("%d%d%d",&a,&b,&w);
    if(w<D[a][b]){
      D[a][b]=D[b][a]=w;
    }
  }

  
  for(int k=1;k<=N;k++){
    for(int a=1;a<=N;a++){
      LLI_ dak=D[a][k];
      if(dak>=INF){continue;}
      for(int b=1;b<=N;b++){
        LLI_ d=D[k][b]+dak;
        if(d>=INF){continue;}
        if(d<D[a][b]){
          D[a][b]=D[b][a]=d;
        }
      }
    }
  }

  for(int q=0;q<nq;q++){
    int a,b;scanf("%d%d",&a,&b);
    LLI_ d=D[a][b];
    printf("%lld\n",d<INF ? d : -1);
  }

  return 0; 
}
