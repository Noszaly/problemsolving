// Building Roads
#include <bits/stdc++.h>
using namespace std;

#define RC getchar_unlocked
int getnum(){
  int c;
  while((c=RC())<'0' || c>'9');
  int x=c-'0';
  while((c=RC())>='0' && c<='9'){
    x=10*x+c-'0';
  }
  return x;
}


typedef pair<int,int> mp;
#define ff first
#define ss second

vector<vector<int>> gg;
vector<bool> vis;
void gen(int a){
  for(auto b:gg[a]){
    if(vis[b]==false){
      vis[b]=true;
      gen(b);
    }
  }
}

int main(){
  int nv=getnum();
  int ne=getnum();

  gg.resize(nv+1);
  for(int i=0;i<ne;i++){
    int a=getnum();
    int b=getnum();
    gg[a].push_back(b);
    gg[b].push_back(a);
  }
  vis.resize(nv+1,false);

  vector<mp> sol;
  int p=-1;
  for(int a=1;a<=nv;a++){
    if(vis[a]==false){
      vis[a]=true;
      gen(a);
      if(p>0){
        sol.push_back(mp({p,a}));
      }
      p=a;
    }
  }
  printf("%d\n",int(sol.size()));
  for(auto v:sol){
    printf("%d %d\n",v.ff,v.ss);
  }

  return 0;

}
