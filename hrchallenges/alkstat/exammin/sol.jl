# N tétel van K-t tud. 2-t húz visszatevés nélkül
# sikeres vizsga valsége,
# nem huzza mindkettőt a N-K közül

solve(N,K)=1.0-binomial(N-K,2)/binomial(N,2)
function solve2(n,P)
  K=0
  while solve(n,K)<P
    K+=1
  end
  K
end

n,P=split(readline())
n=parse(Int,n)
P=parse(Float64,P)
println(solve2(n,P))

