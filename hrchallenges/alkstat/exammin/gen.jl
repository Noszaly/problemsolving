# N tétel van K-t tud. 2-t húz visszatevés nélkül
# sikeres vizsga valsége,
# nem huzza mindkettőt a N-K közül

solve(N,K)=1.0-binomial(N-K,2)/binomial(N,2)
function solve2(n,P)
  K=0
  while solve(n,K)<P
    K+=1
  end
  K
end



for fn=3:50
  fin=open("input/input$(fn).txt","w")

  n=rand(10:30)
  P=round(rand(),digits=2)
  println(fin,n," ",P)


  K=solve2(n,P)

  fout=open("output/output$(fn).txt","w")
  println(fout,K)

  close(fin)
  close(fout)
end
