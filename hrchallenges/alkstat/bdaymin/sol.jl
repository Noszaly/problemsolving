function solve(P)
  qM,m,M=1.0,365,1 # qM: "M emberbol nincs ket egyforma napon szuletett" valsege
  while true 
    if 1.0-qM>P
      break
    end
    M+=1
    m-=1
    qM=qM*(m/365)
  end
  M
end

P=parse(Float64,readline())
println(solve(P))

