function solve(p,s)
  function gen(w)
    ww=Dict(zip('A':'Z',fill(0,26)))
    nw=length(w)
    for c=w
      ww[c]+=1
    end
    ww,nw
  end
  pp,np=gen(p)
  ss,ns=gen(s)

  # with replacement
  r,R=0.0,0.0
  if issubset(Set(s),Set(p))
    r=1.0
    for (k,v)=ss
      if v==0 continue end
      r=r*(pp[k]/np)^v
    end
    R=1.0
    for (k,v)=ss
      if v==0 continue end
      if v>pp[k] R=0.0; break end
      R=R*binomial(pp[k],v)
    end
    if R>0.0 R=R/binomial(np,ns) end
  end
  r,R
end




using StatsBase
mround(x,d=4)=round(x,digits=d)
msimp(x,d=6)=floor(x*10.0^d)/10.0^d


Q=10
#println(fin,Q)
for n=15:Q+15
  fin=open("input/input$(n).txt","w")

  np=rand(10:30)
  p=sample('A':'Z',30,replace=true)
  ns=rand(2:np)
  s=sample(p,ns,replace=true)
  p,s=join.([p,s])
  println(fin,p)
  println(fin,s)


  r,R=solve(p,s)
  r,R=round.([r,R],digits=12)
  fout=open("output/output$(n).txt","w")
  println(fout,r,"\n",R)

  close(fin)
  close(fout)
end

