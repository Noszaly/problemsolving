let
  p=strip(readline())
  s=strip(readline())

  function gen(w)
    ww=Dict(zip('A':'Z',fill(0,26)))
    nw=length(w)
    for c=w
      ww[c]+=1
    end
    ww,nw
  end
  pp,np=gen(p)
  ss,ns=gen(s)

  # with replacement
  r,R=0.0,0.0
  if issubset(Set(s),Set(p))
    r=1.0
    for (k,v)=ss
      if v==0 continue end
      r=r*(pp[k]/np)^v
    end
    R=1.0
    for (k,v)=ss
      if v==0 continue end
      if v>pp[k] R=0.0; break end
      R=R*binomial(pp[k],v)
    end
    if R>0.0 R=R/binomial(np,ns) end
  end

  r,R=round.([r,R],digits=12)
  println(r,"\n",R)
end

