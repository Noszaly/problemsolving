N=sscanf(fgetl(stdin),"%d");
%N=50;
dp=zeros(N,N*6);
dp(1,1:6)=1/6;
for n=2:N
  for s=n:n*6
    P=0.0;
    for k=1:6
      if (s-k<n-1) break end  
      P+=dp(n-1,s-k);
    end
    dp(n,s)=P/6.0;
  end
end

fprintf(stdout,"%.12f\n",dp(N,N:6*N));



