mround(x,d=4)=round(x,digits=d)
msimp(x,d=6)=floor(x*10.0^d)/10.0^d

N=50
dp=fill(0.0,N,N*6)
dp[1,1:6].=1/6
for n=2:N
  for s=n:n*6
    P=0.0
    for k=1:6
      (s-k<n-1)&&break  
      P+=dp[n-1,s-k]
    end
    dp[n,s]=P/6.0
  end
end

n=parse(Int,readline())
dat=mround.(dp[n,n:6*n],12)
println(join(dat,"\n"))


