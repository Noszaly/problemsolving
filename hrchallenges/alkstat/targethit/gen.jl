using StatsBase

let
  function f(x)
    n=length(x)
    r=fill(1.0,n+1)
    for i=1:n
      r[i+1]=(1-x[i])*r[i]
      r[i]=x[i]*r[i]
    end
    r
  end
  

  for fn=0:2
    fin=open("input/input$(fn).txt","w")
    h=rand(2:3)
    p=sample(20:80,h,replace=false)./100
    println(fin,join(p," "))

    p=f(p)
    p[1:end-1]./=(1.0-p[end])
    fout=open("output/output$(fn).txt","w")
    println(fout,join(p[1:end-1],"\n"))

    close(fin)
    close(fout)
  end
end