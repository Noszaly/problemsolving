def f(x):
  n=len(x)
  r=[1.0]*(n+1)
  for i in range(0,n):
    r[i+1]=(1-x[i])*r[i]
    r[i]=x[i]*r[i]
  return r

p=[float(v) for v in input().split()]
p=f(p)
oszt=1.0-p[-1]
out="\n".join(str(v/oszt) for v in p[:-1])
print(out)