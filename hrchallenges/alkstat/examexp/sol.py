def factorial(N):
  ret=1
  while N>1:
    ret*=N
    N-=1
  return ret

def binomial(N,K):
  if K>N:
    return 0.0
  if K==0 or K==N:
    return 1.0
  return factorial(N)/(factorial(N-K)*factorial(K))

def solve(N,K):
  p=binomial(N-K,2)/binomial(N,2)
  #print(p)
  return p+(1.0-p)*(2+3+4+5)/4.0



n,k=[int(v) for v in input().split()]
#print(solve(n,k))
print(round(solve(n,k)))

