# N tétel van K-t tud. 2-t húz visszatevés nélkül
# sikeres vizsga valsége: nem huzza mindkettőt a N-K közül
# de jegyet is kap, és az átlagos jegy érdekel

#solve(N,K)=(1.0-binomial(N-K,2)/binomial(N,2))*(2+3+4+5)/4.0
solve(N,K)=(p=binomial(N-K,2)/binomial(N,2);println(p);p+(1.0-p)*(2+3+4+5)/4.0)


n,k=parse.(Int,split(readline()))
println(solve(n,k))
println(round(solve(n,k),digits=0))

