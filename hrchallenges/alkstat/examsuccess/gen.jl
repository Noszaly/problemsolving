# N tétel van K-t tud. 2-t húz visszatevés nélkül
# sikeres vizsga valsége,
# nem huzza mindkettőt a N-K közül

solve(N,K)=1.0-binomial(N-K,2)/binomial(N,2)



for fn=0:2
  fin=open("input/input$(fn).txt","w")

  n=rand(10:30)
  k=rand(0:2)
  println(fin,n," ",k)


  r=solve(n,k)
  r=round(r,digits=12)

  fout=open("output/output$(fn).txt","w")
  println(fout,r)

  close(fin)
  close(fout)
end
