# T-szer feldobva egy F-oldalu "kockat", mennyi a kapott szamok maximumanak 
# varhato erteke?

function solve(t,f)
  f-sum(exp.(t*(log.(1:f-1).-log(f))))
end

for fn=2:50
  fin=open("input/input$(fn).txt","w")
  T=rand(1:33)
  F=rand(1:33)
  println(fin,T," ",F)

  fout=open("output/output$(fn).txt","w")
  println(fout,solve(T,F))

  close(fin)
  close(fout)
end

