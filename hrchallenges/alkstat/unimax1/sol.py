t,f=[int(v) for v in input().split()]
print("{:.6f}".format(f-sum((v/f)**t for v in range(1,f))))