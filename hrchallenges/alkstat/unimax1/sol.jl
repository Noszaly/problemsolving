# T-szer feldobva egy F-oldalu "kockat", mennyi a kapott szamok maximumanak 
# varhato erteke?

function solve(t,f)
#  f-sum(exp.(t*(log.(1:f-1).-log(f))))
  f-sum(((1:f-1)./f).^t)
end


T,F=parse.(Int,split(readline()))
println(solve(T,F))
