let
X,Y,a,d=parse.(Int,split(readline()))
sol=fill(0,d)
for it=1:d
  sol[it]=X÷Y
  X=a*(X%Y)
end
sol="0."*join(string.(sol[2:end]))
println(sol)
end