import sys,os,re
from konfig import *

def lang(pname):
  p=pname.split('.')[-1]
  if p=='py':
    return 'python3 {} < '.format(pname)
  if p=='jl':
    return '/home/nosy/bin/julia {} < '.format(pname)
  if p=='m':
    return 'octave {} < '.format(pname)

def tokenek(nev):
  f=open(nev)
  d=f.read().split()
  f.close()
  return [v.strip() for v in d if len(v.strip())>0 ] #paranoia

def testit(d,t):

  if len(d)!=len(t):
    return('length error')

  for sd,st in zip(d,t):
    akt=str
    while True:
      try:
        int(st)
        akt=int
        break
      except:
        pass

      try:
        float(st)
        akt=float
        break
      except:
        pass
      break

    try:
      akt(sd)
    except:
      return('type error')

    vt=akt(st)
    vd=akt(sd)

    while True:
      if akt==str:
        if vt!=vd:
          return('string error')
      if akt==int:
        if vt!=vd:
          return('integer error')
      if akt==float:
        if abs(vt-vd)>1e-6:
          return('float error')
      break
  return('ok')



progname=sys.argv[1]

try:
  open(progname).close()
except:
  print('{} not found'.format(progname))
  exit(1)

if not os.path.isdir('input') or not os.path.isdir('output'):
  print('input/output dir(s) not found')
  exit(1)

precmd=lang(progname)


myfiles=os.scandir('input/')
for inp in myfiles:
  #print(inp)
  if inp.is_file() and inp.name[0:2]=='in':
    print('testing {0:s} '.format(inp.name),end='')
    num=int(inp.name[5:].split('.')[0])
    cmd=precmd+'input/{:s} > o'.format(inp.name)
    res=os.system(cmd)
    if res!=0:
      print('runtime error')
      continue

    outp='output/output{:d}.txt'.format(num)
    print(testit(tokenek('o'),tokenek(outp)))
