# x is a string
def f2s(path):
  with open(path, 'r') as f:
    data = f.read().split()
  return data
  
def tip(x):
  try:
    y=int(x)
    return int,y
  except:
    pass

  try:
    y=float(x)
    return float,y
  except:
    pass

  return str,x



def check(ok,cand):# two list of strings
  if len(ok)!=len(cand):
    return False,"length error"

  for o,c in zip(ok,cand):
    to,tc=tip(o),tip(c)
    while True:
      if to[0]!=tc[0]:
        return False,"type error"
      if to[0]==float:
        if abs(to[1]-tc[1])>1e-4:
          return False,"float error"
      if to[0]==int:
        if to[1]!=tc[1]:
          return False,"integer error"
      if to[1]!=tc[1]:
        return False,"string error"
      break
  return True,"ok"


def run_custom_checker(t_obj, r_obj):
    # Don't print anything to STDOUT in this function
    # Enter your custom checker scoring logic here

    r_obj.result = True;
    r_obj.score = 1.0;
    r_obj.message = "Success";

    try:
        result_data = open(t_obj.testcase_output_path, 'r').read().split()
    except IOError:
        r_obj.result = False
        r_obj.score = 0
        r_obj.message = 'Error reading result file'
        return
    if len(result_data)<3:
        r_obj.result = False
        r_obj.score = 0
        r_obj.message = 'Unsufficient data'
        return
    if tip(result_data[0])[0]!=int or tip(result_data[1])[0]!=float or tip(result_data[2])[0]!=str:
        r_obj.result = False
        r_obj.score = 0
        r_obj.message = 'Wrong answer'
        return 
    return    
