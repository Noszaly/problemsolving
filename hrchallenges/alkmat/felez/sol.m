inp=scanf("%f",[1,4]);
d=inp(1);
a=inp(2);
b=inp(3);
tol=inp(4);
p=scanf("%f",[1,d+1]);

pa=polyval(p,a);
pb=polyval(p,b);
if pa*pb>0
  printf("Inf\n");
else
  while b-a>tol
    m=0.5*(a+b);
    pm=polyval(p,m);
    if pm*pb<=0
      a=m;
      pa=pm;
    else
      b=m;
      pb=pm;
    end
  end
  printf("%.9f\n",0.5*(a+b));
end
