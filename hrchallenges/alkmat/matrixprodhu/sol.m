1;
function M=readmatrix()
  inp=scanf("%d",[1,2]);
  r=inp(1);
  c=inp(2);
  M=[];
  for it=1:r
    M=[M; scanf("%f",[1,c])];
  end
end

A=readmatrix();
B=readmatrix();
cA=size(A)(2);
rB=size(B)(1);
if cA!=rB
  printf("-1\n");
else
  C=A*B;
  printf("%.9f\n",abs(sum(sum(C))));
end
