using LinearAlgebra
mround(x,d=4)=round(x,digits=d)

Q=20
#println(fin,Q)
for n=2:Q
  fin=open("input/input$(n).txt","w")
  r,c=rand(4:50,2)
  println(fin,r," ",c)
  A=rand(1:100,r,c).-50
  A=A./rand(2:10)
  sA=join([ join(string.(A[i,1:end])," ") for i in 1:r], "\n" )
  println(fin,sA)
  
  delta=rand([0,1])
  delta*=rand([-1,1])
  
  r=c+delta
  c=rand(4:100)
  println(fin,r," ",c)
  B=rand(1:100,r,c).-50
  B=B./rand(2:10)
  sB=join([ join(string.(B[i,1:end])," ") for i in 1:r], "\n" )
  println(fin,sB)
  
  
  ret=-1
  if delta==0
    C=A*B
    ret=abs(sum(C))
  end
  fout=open("output/output$(n).txt","w")
  println(fout,round(ret,digits=9))

  close(fin)
  close(fout)
end

