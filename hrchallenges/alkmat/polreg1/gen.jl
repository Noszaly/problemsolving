using LinearAlgebra, Polynomials
mround(x,d=4)=round(x,digits=d)

Q=30
#println(fin,Q)
for q=2:40
  fin=open("input/input$(q).txt","w")
  d=rand(1:5)
  n=rand(4:20)
  lim=rand(4:10)
  t=rand(-lim:lim,n)
  f=rand(-lim:lim,n)
  println(fin,d)
  println(fin,n)
  println(fin,join([string(v) for v in t], " "))
  println(fin,join([string(v) for v in f], " "))

  A=ones(n,1);
  tt=copy(t)
  for k=1:d
    A=hcat(A,tt)
    tt=tt.*t
  end
  AtA=A'*A

  fout=open("output/output$(q).txt","w")

  if cond(AtA,1)>1e6
    println(fout,"-1")
  else
    pol=Poly(AtA\(A'*f))
    
    mult,jovo=minimum(t)-1,maximum(t)+1
    res=polyval(pol,[mult,jovo])
    println(fout, join([string(v) for v in res],"\n"))
  end  

  close(fin)
  close(fout)
end

