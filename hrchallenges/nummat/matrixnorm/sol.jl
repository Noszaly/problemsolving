let
  function mreadmtx(r,c)
    A=fill(0.0,r,c)
    for i=1:r
      A[i,:].=parse.(Float64,split(readline()))
    end
    A
  end
  mround(x,d=4)=round(x,digits=d)

  Q=parse(Int,readline())
  for i=1:Q
    n=parse(Int,readline())
    A=mreadmtx(n,n)
    AA=abs.(A)
    r1=maximum( [ sum(AA[:,i]) for i=1:n ] ) |> mround
    rinf=maximum( [ sum(AA[i,:]) for i=1:n ] ) |> mround
    println(r1,"\n",rinf)
  end
end