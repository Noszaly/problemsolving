def mreadmtx(r,c):
  return [[float(v) for v in input().split()] for i in range(r)]
def mabs(A):
  return [[abs(v) for v in rows] for rows in A]
def mcol(A,j):
  return [row[j] for row in A]

Q=int(input())
for i in range(Q):
  n=int(input())
  A=mreadmtx(n,n)
  AA=mabs(A)
  rinf=max(sum(row) for row in AA)
  r1=max(sum(mcol(AA,i)) for i in range(n))
  print("{0:.4f}\n{1:.4f}".format(r1,rinf))