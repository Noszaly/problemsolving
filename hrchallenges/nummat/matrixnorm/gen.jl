mround(x,d=4)=round(x,digits=d)
msimp(x,d=8)=floor(x*10.0^d)/10.0^d
fin=open("in2","w")
fout=open("out2","w")

Q=99
println(fin,Q)
for i=1:Q
  n=rand(20:80)
  println(fin,n)
  A=rand(n,n).-0.5
  A=msimp.(A)
  println(fin, join( [join(string.(A[i,:])," ") for i in 1:n] ,"\n"))

  AA=abs.(A)
  r1=maximum( [ sum(AA[:,i]) for i=1:n ] ) |> mround
  rinf=maximum( [ sum(AA[i,:]) for i=1:n ] ) |> mround

  println(fout,r1,"\n",rinf)
end

close(fin)
close(fout)
