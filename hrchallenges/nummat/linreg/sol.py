t=[float(v) for v in input().split()]
f=[float(v) for v in input().split()]
x=[float(v) for v in input().split()]
n=len(t)
s=n*sum([v*v for v in t])-sum(t)**2
if s<1e-12:
  print("ambigous")
else:
  b1=(n*sum([v*w for v,w in zip(t,f)])-sum(t)*sum(f))/s
  b0=(sum(f)-b1*sum(t))/n
  Lx=[str(b1*v+b0) for v in x]
  print(" ".join(Lx))
