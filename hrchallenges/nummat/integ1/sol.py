def horner(p):
  def val(x):
    px=0
    for v in p:
      px=px*x+v
    return px 
  return val

p=[float(v) for v in input().split()]
a,b=[float(v) for v in input().split()]
pol=horner(p)
m=0.5*(a+b)
mp=(b-a)*pol(m)
trap=(b-a)*0.5*(pol(a)+pol(b))
simp=(b-a)/6*(pol(a)+4*pol(m)+pol(b))

out=["{0:.4f}".format(v) for v in [mp,trap,simp]]
print("\n".join(out))

