mround(x,d=4)=round(x,digits=d)
msimp(x,d=6)=floor(x*10.0^d)/10.0^d
function horner(p)
  function val(x)
    px=0
    for v=p
      px=px*x+v
    end
    px
  end
  val
end

Q=22
#println(fin,Q)
for i=6:Q
  fin=open("input/input$(i).txt","w")
  fout=open("output/output$(i).txt","w")
  d=rand(3:11)
  p=msimp.(rand(-4:4,d+1).*rand())
  #p=rand(-4:4,d+1)

  #p=msimp.(p)
  println(fin,join(p," "))
  a=rand(-10:10)*rand()
  #a=rand(-10:10)
  #b=a+2
  b=a+rand(1:3)*rand()
  #eps=1.0/10^rand(4:8)
  #dat=msimp.([a,b])
  dat=[a,b]
  println(fin,join(dat," "))

  pol=horner(p)
  m=0.5*(a+b)
  mp=(b-a)*pol(m)
  trap=(b-a)*0.5*(pol(a)+pol(b))
  simp=(b-a)/6*(pol(a)+4*pol(m)+pol(b))
  dat=mround.([mp,trap,simp])
  println(fout,join(dat,"\n"))

  close(fin)
  close(fout)
end

