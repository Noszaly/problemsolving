let
  function readmtx(r,c)
    A=fill(0.0,r,c)
    for i=1:r
      A[i,:].=parse.(Float64,split(readline()))
    end
    A
  end

  Q=parse(Int,readline())
  for i=1:Q
    rA,cA=parse.(Int,split(readline()))
    A=readmtx(rA,cA)
    rB,cB=parse.(Int,split(readline()))
    B=readmtx(rB,cB)
    if cA==rB
      println(round(1+abs(sum(A*B)),digits=4))
    else
      println(0.0)
    end
  end
end