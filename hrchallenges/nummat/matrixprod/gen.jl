fin=open("in2","w")
fout=open("out2","w")

n=33
println(fin,n)
for i=1:n
  rA=rand(20:21)
  cA=rand(20:21)
  println(fin,rA," ",cA)
  A=rand(rA,cA).-0.5
  println(fin, join( [join(string.(A[i,:])," ") for i in 1:rA] ,"\n"))

  rB=rand(20:21)
  cB=rand(20:21)
  println(fin,rB," ",cB)
  B=rand(rB,cB).-0.5
  println(fin, join( [join(string.(B[i,:])," ") for i in 1:rB] ,"\n"))

  if cA==rB
    println(fout,round(1+abs(sum(A*B)),digits=4))
  else
    println(fout,0.0)
  end
end

close(fin)
close(fout)
