def mreadmtx(r,c):
  return [[float(v) for v in input().split()] for i in range(r)]
def mprod(A,B):
  rA,rB=len(A),len(B)
  cA,cB=len(A[0]),len(B[0])
  if cA!=rB: return None
  C=[[0]*cB for i in range(rA)]
  for i in range(rA):
    for j in range(cB):
      s=0.0
      for k in range(cA):
        s+=A[i][k]*B[k][j]
      C[i][j]=s
  return C
def msum(A):
  return sum([sum(v) for v in A])  
Q=int(input())
for i in range(Q):
  rA,cA=[int(v) for v in input().split()]
  A=mreadmtx(rA,cA)


  rB,cB=[int(v) for v in input().split()]
  B=mreadmtx(rB,cB)
  if cA==rB:
    res=msum(mprod(A,B))
    print("{0:.04f}".format(1+abs(res)))
  else:
    print(0.0)