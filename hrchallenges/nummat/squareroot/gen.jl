mround(x,d=4)=round(x,digits=d)
msimp(x,d=6)=floor(x*10.0^d)/10.0^d

Q=30
#println(fin,Q)
for i=1:Q
  fin=open("input/input$(i).txt","w")
  fout=open("output/output$(i).txt","w")
  n=rand(2:100)
  t=rand(-50:50,n)
  f=rand(-50:50,n)
  m=rand(2:100)
  x=rand(-50:50,m)
  

  println(fin,join(t," "))
  println(fin,join(f," "))
  println(fin,join(x," "))


  s=n*sum(t.*t)-sum(t)^2
  if s<1e-12
    println(fout,"ambigous")
  else
    b1=(n*sum(t.*f)-sum(t)*sum(f))/s
    b0=(sum(f)-b1*sum(t))/n
    L(x)=b1*x+b0
    println(fout,join(L.(x)," "))
  
  close(fin)
  close(fout)
end

