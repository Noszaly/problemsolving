let
  a,an,n=split(readline())
  a,an=parse.(Float64,[a,an])
  n=parse(Int,n)
  while n>0
    n-=1
    an=0.5*(an+a/an)
  end
  println(an)
end