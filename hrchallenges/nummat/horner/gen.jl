mround(x,d=4)=round(x,digits=d)
msimp(x,d=6)=floor(x*10.0^d)/10.0^d
fin=open("in2","w")
fout=open("out2","w")
function horner(p)
  function val(x)
    px=0
    for v=p
      px=px*x+v
    end
    px
  end
  val
end

Q=33
println(fin,Q)
for i=1:Q
  d=rand(3:11)
  p=rand(-3:3,d+1).*rand(d+1)
  p=msimp.(p)
  println(fin,join(p," "))
  n=rand(10:20)
  x=rand(-3:3,n).*rand(n)
  x=msimp.(x)
  println(fin,join(x," "))

  pol=horner(p)
  px=pol.(x)
  println(fout,join(mround.(px),"\n"))
end

close(fin)
close(fout)
