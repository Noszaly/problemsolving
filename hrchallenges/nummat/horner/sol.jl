mround(x,d=4)=round(x,digits=d)
msimp(x,d=6)=floor(x*10.0^d)/10.0^d
function horner(p)
  function val(x)
    px=0
    for v=p
      px=px*x+v
    end
    px
  end
  val
end

Q=parse(Int,readline())
for i=1:Q
  p=parse.(Float64,split(readline()))
  x=parse.(Float64,split(readline()))
  pol=horner(p)
  px=mround.(pol.(x))
  println(join(px,"\n"))
end

