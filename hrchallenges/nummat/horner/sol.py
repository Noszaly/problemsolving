def horner(p):
  def val(x):
    px=0
    for v in p:
      px=px*x+v
    return px 
  return val

Q=int(input())
for i in range(Q):
  p=[float(v) for v in input().split()]
  x=[float(v) for v in input().split()]
  pol=horner(p)
  px=["{0:.4f}".format(pol(v)) for v in x]
  print("\n".join(px))

