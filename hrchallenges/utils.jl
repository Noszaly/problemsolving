# for gen.jl
mround(x,d=4)=round(x,digits=d)
msimp(x,d=8)=floor(x*10.0^d)/10.0^d


# for sol.jl
function mreadmtx(r,c)
  A=fill(0.0,r,c)
  for i=1:r
    A[i,:].=parse.(Float64,split(readline()))
  end
  A
end
