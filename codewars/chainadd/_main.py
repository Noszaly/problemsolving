def add(a):
  def _add(aa=None):
    if aa==None: return _add.val
    _add.val+=aa
    return _add
  _add.val=a
  return _add


# class add(int):
#   def __call__(self, n):
#     return add(self + n)

print(add(1))
print(add(1)(2))
print(add(1)(2)(3))

