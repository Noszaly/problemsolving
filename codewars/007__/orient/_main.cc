#include <bits/stdc++.h>
int orienteeringBeginner(int n, std::vector<std::vector<std::vector<int>>> roads) {
  int const INF=1000000000;
  std::vector<int> dist(n,INF);
  typedef std::pair<int,int> IP;
  std::priority_queue<IP,std::vector<IP>,std::greater<IP>> pq;
  dist[0]=0;
  pq.push({0,0});
  while(pq.size()>0){
    auto ds=pq.top();pq.pop();
    int d=ds.first, s=ds.second;
    if(s==n-1){break;}
    for(auto& r:roads[s]){
      int t=r[0], w=r[1];
      if(d+w<dist[t]){
        dist[t]=d+w;
        pq.push({d+w,t});
      }
    }
  }
  if(dist[n-1]>=INF){
    dist[n-1]=-1;
  }
  return dist[n-1];
}

int main(){
  typedef std::vector<std::vector<std::vector<int>>> GR;
  
  // GR roads={
  //   {{1,50},{3,10}}, 
  //   {{2,15}}, 
  //   {{4,55}}, 
  //   {{1,5}}, 
  //   {}
  // };
  // int n=5;

  GR roads={{{1, 3}},
         {{4, 1}, {3, 2}},
         {{1, 0}},
         {{5, 3}},
         {{5, 5}},
         {}};
  int n=6;

  std::cout<<orienteeringBeginner(n,roads)<<"\n";
  return 0;
}

