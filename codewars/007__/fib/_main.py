def fib(n):
  """Calculates the nth Fibonacci number"""
  nn=abs(n)
  ret=0
  if nn>4:
    def mul(x,y):
      z=[[0,0],[0,0]]
      z[0][0]=x[0][0]*y[0][0]+x[0][1]*y[1][0]
      z[0][1]=x[0][0]*y[0][1]+x[0][1]*y[1][1]
      z[1][0]=x[1][0]*y[0][0]+x[1][1]*y[1][0]
      z[1][1]=x[1][0]*y[0][1]+x[1][1]*y[1][1]
      return z
      
    def _fib(m,B):
      if m>1: 
        B=_fib(m//2,B)
        B=mul(B,B)
        if m%2==1:
          B=mul(B,[[0,1],[1,1]])
      return B
    A=_fib(nn-1,[[0,1],[1,1]])
    ret=A[0][0]+A[0][1]
  else:
    ret=[0,1,1,2,3][nn]
  if n<0 and nn%2==0:
    ret=-ret
  return ret

for n in range(-100000,100001,100000):
  print(fib(n))
