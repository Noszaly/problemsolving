from collections import deque
def peak_height(omt):
  # equal length rows?
  r,c=len(omt),len(omt[0])
  mt=[' '*(c+2)]+[' '+v+' ' for v in omt]+[' '*(c+2)]
  #print(mt)
  qq=deque()
  INF=1000000000
  hh=[[INF]*(c+2) for _ in range(r+2)]
  for i in range(r+2):
    for j in range(c+2):
      if mt[i][j]!='^': 
        hh[i][j]=0
        qq.append((i,j))
  
  def disc(i,j,d):
    nonlocal r,c
    if i<1 or i>r or j<1 or j>c: return
    if d<hh[i][j]:
      hh[i][j]=d
      qq.append((i,j))
  mx=0
  while len(qq)>0:
    i,j=qq.popleft()
    d=hh[i][j]
    mx=max(d,mx)
    d+=1
    # edge or side?
    disc(i-1,j,d)
    disc(i+1,j,d)
    disc(i,j-1,d)
    disc(i,j+1,d)
  
  return mx

mts = [
[
  "^^^^^^        ",
  " ^^^^^^^^     ",
  "  ^^^^^^^     ",
  "  ^^^^^       ",
  "  ^^^^^^^^^^^ ",
  "  ^^^^^^      ",
  "  ^^^^        "
],
[
'^^^^^',
' ^^^ ',
'^^^^^'
],
[
'^^^^^       ',
'^^^^^       ',
'^^^^^       ',
'            ',
'     ^^^^^^^',
'     ^^^^^^^',
'     ^^^^^^^',
'     ^^^^^^^',
'     ^^^^^^^',
'     ^^^^^^^',
'     ^^^^^^^'
],
[
'^^   ^^^  ^^',
'^ ^^  ^^^   ',
'  ^^^   ^^  ',
'    ^^ ^^   ',
'   ^  ^     ',
'    ^^      ',
' ^^^^^^^^   ',
'  ^^^^^^^^  ',
' ^^ ^^^   ^^',
'^^^    ^^ ^^',
'     ^^^^^^^'
]
]
for mt in mts:
  print(peak_height(mt))
