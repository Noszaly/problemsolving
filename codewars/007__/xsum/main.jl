# geometric series
# u(x)+1/(1-x)=1/(1-x)^2
# u(x)=1/(1-x)^2-1/(1-x)+1=m
# 1/(1-x)^2-1/(1-x)-m=0
# 1/(1-x)=y,x=1-1/y
module Solve
    export solve
#    fun(y)=1/(1-y)^2-1/(1-y)
    function solve(m)
      D=sqrt(1.0+4.0*m)
      y1=0.5*(1.0+D)
      y2=0.5*(1.0-D)
      y1=1-1/y1
      y2=1-1/y2
#      println(y1," ",y2)
      if abs(y1)<1 y1 else y2 end
    end
end 


using .Solve
for m in [0.5, 2.0, 4.0, 5.0, 8.0, 1200]
  println(solve(m))
end

