from math import sqrt
def harvester_rescue(data):
  H=data['harvester']
  W,w=data['worm']
  C,c=data['carryall']
  dW=sqrt((W[0]-H[0])**2+(W[1]-H[1])**2)
  tW=dW/w
  dC=sqrt((C[0]-H[0])**2+(C[1]-H[1])**2)
  tC=dC/c
  if tC+1<tW:
    return 'The spice must flow! Rescue the harvester!'
  else:
    return "Damn the spice! I'll rescue the miners!"
    
data1 = {'harvester': [345,600], 'worm': [[200,100],25], 'carryall': [[350,200],32]}
data2 = {'harvester': [200,400], 'worm': [[200,0],40], 'carryall': [[500,100],45]}
data3 = {'harvester': [850,125], 'worm': [[80,650],20], 'carryall': [[80,600],20]}
data4 = {'harvester': [0,320], 'worm': [[250,680],42], 'carryall': [[550,790],58]}
data5 = {'harvester': [0,0], 'worm': [[0,600],50], 'carryall': [[0,880],80]}

assert_equals=print
assert_equals(harvester_rescue(data1)=='The spice must flow! Rescue the harvester!')
assert_equals(harvester_rescue(data2)=='Damn the spice! I\'ll rescue the miners!')
assert_equals(harvester_rescue(data3)=='The spice must flow! Rescue the harvester!')
assert_equals(harvester_rescue(data4)=='Damn the spice! I\'ll rescue the miners!')
assert_equals(harvester_rescue(data5)=='Damn the spice! I\'ll rescue the miners!')

