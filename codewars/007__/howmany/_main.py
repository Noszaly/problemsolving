def find_all(sd, nd):
  if nd*9<sd or nd>sd: return []
  dp={}
  for d in range(10):
    dp[(d,1,d)]=1 # starting digit, num of digits ,sum of digits : number of possibilities
  def gen(d,ndig,sumdig):
    if ndig*d>sumdig or d+9*(ndig-1)<sumdig: 
      dp[(d,ndig,sumdig)]=0
      return
    if dp.get((d,ndig,sumdig))!=None: return
    s=0
    for dd in range(d,10):
      if dd+9*(ndig-1)<sumdig: continue
      if dd+(ndig-1)>sumdig: break
      gen(dd,ndig-1,sumdig-d)
      s+=dp[(dd,ndig-1,sumdig-d)]
    dp[(d,ndig,sumdig)]=s

  s=0
  for d in range(1,10):
    gen(d,nd,sd)
    if dp.get((d,nd,sd))!=None: s+=dp[(d,nd,sd)]
    
  #print(s)
  mn,pmn,smn=0,1,sd
  mx,pmx,smx=0,1,sd
  for h in range(nd,0,-1):
    for t in range(pmn,10):
      if dp.get((t,h,smn))!=None and dp[(t,h,smn)]>0:
        pmn=t
        break
    pmn=t
    mn=mn*10+pmn
    smn-=pmn
    for t in range(9,pmx-1,-1):
      if dp.get((t,h,smx))!=None and dp[(t,h,smx)]>0:
        pmx=t
        break
    pmx=t
    mx=mx*10+pmx
    smx-=pmx
  return [s,mn,mx]
    
for nd,sd in [(3,10),(3,27),(4,84),(6,35),(12,65)]:
  print(find_all(sd,nd))
