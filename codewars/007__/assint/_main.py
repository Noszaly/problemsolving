def simple_assembler(program):
  pr,dd=[],{}
  def ins(v):
    if type(v)==int: return
    if dd.get(v)==None: dd[v]=None

  for lin in program:
    v=lin.split()
    if len(v)==0: continue
    if v[0]=='mov':
      x,y=v[1],v[2]
      if not y[0].isalpha(): y=int(y)
      pr.append(['mov',x,y])
      ins(x); ins(y)
      continue
    if v[0]=='jnz':
      x,y=v[1],v[2]
      if not x[0].isalpha(): x=int(x)
      if not y[0].isalpha(): y=int(y)
      pr.append(['jnz',x,y])
      ins(x)
      continue
    if v[0]=='inc':
      pr.append(['inc',v[1]]);
      ins(v[1]) 
      continue
    if v[0]=='dec':
      pr.append(['dec',v[1]])
      ins(v[1])
      continue
  
  npr=len(pr); i=0
  while True:
    if not 0<=i<npr: break
    v=pr[i]
    if v[0]=='mov':
      x,y=v[1],v[2]
      if type(y)==str: y=dd[y]
      dd[x]=y
      i+=1
      continue
    if v[0]=='jnz':
      x,y=v[1],v[2]
      if type(x)==str: x=dd[x]
      if type(y)==str: y=dd[y]
      if x!=0: i+=y
      else: i+=1
      continue
    if v[0]=='inc':
      dd[v[1]]+=1
      i+=1
      continue
    if v[0]=='dec':
      dd[v[1]]-=1
      i+=1
      continue
  ret={k:dd[k] for k in dd.keys() if dd[k]!=None}
  return ret
  
code = '''
mov a 5
inc a
dec a
dec a
jnz a -1
inc a'''
print(simple_assembler(code.splitlines()), {'a': 1})

code = '''\
mov c 12
mov b 0
mov a 200
dec a
inc b
jnz a -2
dec c
mov a b
jnz c -5
jnz 0 1
mov c a'''

print(simple_assembler(code.splitlines()), {'a': 409600, 'c': 409600, 'b': 409600})  

code = '''\
mov a 10
mov b a
jnz 1 2
mov c 10
inc a
inc b
'''
print(simple_assembler(code.splitlines()))  
