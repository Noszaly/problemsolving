module Kata
export spidertofly

function spidertofly(s, f)
  fi=[2,1,0,7,6,5,4,3]*pi*0.25

  fis=fi[s[1]-'A'+1]
  rs=(s[2]-'0')*1.0
  fif=fi[f[1]-'A'+1]
  rf=(f[2]-'0')*1.0
  ps=[cos(fis),sin(fis)]*rs
  pf=[cos(fif),sin(fif)]*rf

  (ps[1]-pf[1])^2+(ps[2]-pf[2])^2 |> sqrt
end

end # module

using .Kata
for (s,f) in [("H3","E2"),("A0","A0"),("H1","H2"),("G4","C4")]
  println(spidertofly(s,f))
end

