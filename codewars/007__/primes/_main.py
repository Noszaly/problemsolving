class Primes:
  H=[2,3,5,7,11,13,17,19,23]
  @classmethod
  def gen(cls,N):
    n=len(cls.H)
    p=cls.H[-1]+2
    while n<N:
      ok=True
      for d in cls.H:
        if d*d>p: break
        if p%d==0: ok=False; break
      if ok: cls.H.append(p); n+=1
      p+=2
  @classmethod
  def first(cls,N):
    cls.gen(N)
    return cls.H[:N]

print(Primes.first(2))
print(Primes.first(100))
