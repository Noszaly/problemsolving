def solvesudoku(tab,N):
  N2=N*N
  row=[ [True]*N2 for _ in range(N2) ]
  col=[ [True]*N2 for _ in range(N2) ]
  sqr=[ [True]*N2 for _ in range(N2) ]
  emp=[]
  for i in range(N2):
    for j in range(N2):
      k=tab[i][j]
      if k<0: emp.append((i,j)); continue
      row[i][k]=False; col[j][k]=False; sqr[N*(i//N)+(j//N)][k]=False
  nEmp=len(emp)
  ok=False
  def gen(level):
    nonlocal N,N2,nEmp,ok
    if level==nEmp: 
      ok=True #; print(tab); 
      return
    i,j=emp[level]
    r=row[i]; c=col[j]; s=sqr[N*(i//N)+(j//N)]
    for k in range(N2):
      if r[k] and c[k] and s[k]:
        r[k]=c[k]=s[k]=False
        tab[i][j]=k
        gen(level+1)
        r[k]=c[k]=s[k]=True
        if True==ok: return
  gen(0)        

def sudoku(puzzle):
  for i in range(9):
    for j in range(9): puzzle[i][j]-=1
  solvesudoku(puzzle,3)
  for i in range(9):
    for j in range(9): puzzle[i][j]+=1
  return puzzle


puzzle = [[5,3,0,0,7,0,0,0,0],
          [6,0,0,1,9,5,0,0,0],
          [0,9,8,0,0,0,0,6,0],
          [8,0,0,0,6,0,0,0,3],
          [4,0,0,8,0,3,0,0,1],
          [7,0,0,0,2,0,0,0,6],
          [0,6,0,0,0,0,2,8,0],
          [0,0,0,4,1,9,0,0,5],
          [0,0,0,0,8,0,0,7,9]]
print(sudoku(puzzle))
