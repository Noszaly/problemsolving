module Factors
  export prime_factors

  function tostr(n,p,ret)
    r=0
    while n%p==0
      r+=1
      n=n÷p
    end
    if r>0
      ret*="($p"
      (r>1)&&(ret*="**$r")
      ret*=")"
    end
    n,ret
  end
  function prime_factors(n)
    n,ret=tostr(n,2,"")
    n,ret=tostr(n,3,ret)
    p,delta=5,2  
    while p*p<=n
      n,ret=tostr(n,p,ret)
      p+=delta; delta=6-delta
    end
    (n>1)&&(ret*="($n)")
    ret
  end

end

using .Factors
for n in [1,2,3,4,5,6,7,8,9,10,11,12,123,321,234,432,86240,7919,125]
  println(prime_factors(n))
end

