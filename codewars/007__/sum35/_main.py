def find(n):
  n3=n//3; n5=n//5; n15=n//15
  return 3*n3*(n3+1)//2 + 5*n5*(n5+1)//2 - 15*n15*(n15+1)//2

for n in [5,10,11,12]:
  print(find(n))
