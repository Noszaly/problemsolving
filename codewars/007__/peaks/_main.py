def pick_peaks(arr):
  ret={"pos":[],"peaks":[]}
  la=len(arr)
  if la>0:
    g=[(arr[i],i) for i in range(la) if i==0 or arr[i]!=arr[i-1]]
    for i in range(1,len(g)-1):
      if g[i-1]<g[i]>g[i+1]:
        ret["pos"].append(g[i][1])
        ret["peaks"].append(g[i][0])
  return ret

print(pick_peaks([1,2,1,1,1,1,2,1]))
