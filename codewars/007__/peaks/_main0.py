from itertools import groupby
def pick_peaks(arr):
  ret={"pos":[],"peaks":[]}
  if arr:
    g=[(k,len(list(l))) for k,l in groupby(arr)]
    _,idx=g[0]
    for i in range(1,len(g)-1):
      k,l=g[i]
      if g[i-1][0]<k>g[i+1][0]:
        ret["pos"].append(idx)
        ret["peaks"].append(k)
      idx+=l
  return ret

print(pick_peaks([1,2,1,1,1,1,2,1]))
