def find_missing_letter(c):
    c=list(map(ord,c))
    p=c[0]
    for a in c[1:]:
        if a>p+1: break
        p=a
    return chr(p+1)
print(find_missing_letter(input()))
