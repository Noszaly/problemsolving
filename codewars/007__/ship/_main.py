def validate_battlefield(fld):
  def disc(a,b,da,db):
    arr=[]
    n=1
    while True:
      a+=da;b+=db
      if a>10 or b>10 or fld[a][b]!=1: break
      arr.append((a,b))
      n+=1
    return n,arr
    
  def clear(a,b):
    fld[a-1][b-1:b+2]=[-1]*3
    fld[a][b-1:b+2]=[-1]*3
    fld[a+1][b-1:b+2]=[-1]*3
      
  
  for i in range(10): fld[i]=[0]+fld[i]+[0]
  fld=[[0]*12] + fld + [[0]*12]
  if sum(sum(v) for v in fld)!=20: return False
  
  ss=[0,4,3,2,1]
  for r in range(1,11):
    for c in range(1,11):
      if fld[r][c]==1:
        nr,ar=disc(r,c,1,0)
        nc,ac=disc(r,c,0,1)
        mx=max(nr,nc)
        mn=min(nr,nc)
        if mx>4 or mn>1: return False
        ss[mx]-=1
        clear(r,c)
        for a,b in ar+ac: clear(a,b)
        #print(nr,nc)
  return 0==sum(abs(s) for s in ss)

b1          =   [[1, 0, 0, 0, 0, 1, 1, 0, 0, 0],
                 [1, 0, 1, 0, 0, 0, 0, 0, 1, 0],
                 [1, 0, 1, 0, 1, 1, 1, 0, 1, 0],
                 [1, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                 [0, 0, 0, 0, 0, 0, 0, 0, 1, 0],
                 [0, 0, 0, 0, 1, 1, 1, 0, 0, 0],
                 [0, 0, 0, 0, 0, 0, 0, 0, 1, 0],
                 [0, 0, 0, 1, 0, 0, 0, 0, 0, 0],
                 [0, 0, 0, 0, 0, 0, 0, 1, 0, 0],
                 [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]]

b2=[
[1, 0, 0, 0, 1, 0, 1, 1, 0, 0],
[1, 0, 0, 0, 1, 0, 0, 0, 0, 0],
[0, 0, 0, 0, 1, 0, 0, 0, 0, 0],
[1, 1, 0, 0, 0, 0, 0, 0, 0, 0],
[0, 0, 0, 1, 0, 1, 0, 1, 0, 0],
[0, 0, 0, 0, 0, 0, 0, 1, 0, 0],
[0, 0, 0, 0, 0, 0, 0, 1, 0, 0],
[0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
[0, 0, 0, 0, 0, 0, 1, 0, 0, 1],
[0, 0, 0, 0, 1, 1, 1, 1, 0, 0]
]
print(validate_battlefield(b2))
