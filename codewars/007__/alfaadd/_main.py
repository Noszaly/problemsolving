def add_letters(*l):
  return chr(ord('a')+(-1+sum(map(lambda x:ord(x)-ord('a')+1,l)))%26)

print(add_letters(*list(input())))
