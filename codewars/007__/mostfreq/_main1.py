def ok(c,s):
  if c.isalpha(): return 2
  if c=='\'':
    if s==2 or s==0: return 1
    return 0
  return 0
def top_3_words(text):
  dd,inw,akt={},0,[]
  for c in text+' ':
    inw=ok(c,inw)
    if inw>0:
      akt.append(c.lower())
    else:
      w=''.join(akt)
      if len(w)==0:continue
      ww=[c for c in w if c.isalpha()]
      if len(ww)==0:continue
      if dd.get(w)==None: dd[w]=0
      dd[w]+=1
      akt=[]
  top=[[-1,''],[-1,''],[-1,'']]
  for w in dd.keys():
    fw=[dd[w],w]
    if fw>top[0]: top[0],top[1],top[2]=fw,top[0],top[1]
    elif fw>top[1]: top[1],top[2]=fw,top[1]
    elif fw>top[2]: top[2]=fw
  return [w for nw,w in top if nw>0]

t=[
"""In a village of La Mancha, the name of which I have no desire to call to
mind, there lived not long since one of those gentlemen that keep a lance
in the lance-rack, an old buckler, a lean hack, and a greyhound for
coursing. An olla of rather more beef than mutton, a salad on most
nights, scraps on Saturdays, lentils on Fridays, and a pigeon or so extra
on Sundays, made away with three-quarters of his income.""",
"e e e e DDD ddd DdD: ddd ddd aa aA Aa, bb cc cC e e e",
"  , e   .. ",
"'a' 'asd asd' as''d"
]
for v in t:
  print(top_3_words(v))
