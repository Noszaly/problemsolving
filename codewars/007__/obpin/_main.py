adj={'0':('8',),'1':('2','4'),'2':('1','3','5'),'3':('2','6'),'4':('1','5','7'),'5':('2','4','6','8'),'6':('3','5','9'),'7':('4','8'),'8':('0','5','7','9'),'9':('6','8')}
def gen(lev,akt,gened):
  if lev<0:
    gened.append(''.join(akt))
    return
  gen(lev-1,akt,gened)
  al=akt[lev]
  for v in adj[al]:
    akt[lev]=v
    gen(lev-1,akt,gened)
  akt[lev]=al

def get_pins(obs):
  akt=[c for c in obs]
  ret=[]
  gen(len(akt)-1,akt,ret)
  return ret

print(get_pins('8'))
print(get_pins('11'))
