module Pass
    export play_pass

    function play_pass(s, n)
      function f(c,i)
        isdigit(c)&&(return ('0'+('9'-c)))
        if isletter(c)
          if islowercase(c)
            c='a'+(Int(c)+n-Int('a'))%26
          else
            c='A'+(Int(c)+n-Int('A'))%26
          end
          if i%2>0
            c=uppercase(c)
          else
            c=lowercase(c)
          end
        end
        c
      end
      reverse(join(f(s[i],i) for i in 1:length(s)))
      
    end

end

using .Pass
testing(a,b,c)=println(play_pass(a,b)==c)

testing("I LOVE YOU!!!", 1, "!!!vPz fWpM J")        
testing("MY GRANMA CAME FROM NY ON THE 23RD OF APRIL 2015", 2, 
            "4897 NkTrC Hq fT67 GjV Pq aP OqTh gOcE CoPcTi aO")
