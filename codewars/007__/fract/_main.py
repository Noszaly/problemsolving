from fractions import Fraction
def add_fracs(*argv):
  if len(argv)==0: return ''
  ret=Fraction()
  for v in argv:
    ret+=Fraction(v)
  return str(ret)

eq=lambda x,y: print(x==y)
eq(add_fracs(), '')
eq(add_fracs('1/2'), '1/2')
eq(add_fracs('1/2', '1/4'), '3/4')
eq(add_fracs('1/2', '3/4'), '5/4')
eq(add_fracs('2/4', '6/4', '4/4'), '3')
eq(add_fracs('2/3', '1/3', '4/6'), '5/3')
eq(add_fracs('2/3', '1/4', '5/6'), '7/4')
eq(add_fracs('-2/3', '5/3', '-4/6'), '1/3')
eq(add_fracs('-7/3', '-1/3', '-2/3'), '-10/3')
eq(add_fracs('1/4', '5/4', '-1/2', '-1/1'), '0')
  
   
   
