# ~ from itertools import permutations

# ~ def find_mult_3(num):
    # ~ num_list = tuple(map(int, str(num)))
    
    # ~ poss = set()
    # ~ for i in range(1, len(num_list)+1):
        # ~ poss |= set(permutations(num_list, i))
    
    # ~ res = set()
    # ~ for p in poss:
        # ~ if p[0] != 0 and sum(p) % 3 == 0:
            # ~ res.add(p)

    # ~ res = [sum(x * 10**n for n, x in enumerate(p[::-1])) for p in res]
    # ~ return [len(res), max(res)]


# ~ from collections import Counter
# ~ from functools import reduce
# ~ from itertools import combinations
# ~ from math import factorial
# ~ from operator import mul

# ~ def find_mult_3(n):
    # ~ mul_count, digits = 0, sorted(map(int, str(n)))
    # ~ for r in range(1, len(digits) + 1):
        # ~ for comb in sorted({c for c in combinations(digits, r) if not sum(c) % 3}):
            # ~ dig_count = Counter(comb)
            # ~ mul_count += (r - dig_count.get(0, 0)) * factorial(r) // reduce(mul, map(factorial, dig_count.values()), r)
    # ~ return [mul_count, int(''.join(map(str, comb[::-1])))]


# ~ from itertools import permutations

# ~ def find_mult_3(num):
    # ~ multiples = set()
    # ~ for t in [int(''.join(p)) for i in range(1, len(str(num)) + (sum([int(d) for d in str(num)]) % 3 == 0)) for p in permutations(str(num), i)]:
        # ~ if t % 3 == 0:
            # ~ multiples.add(t)
    # ~ multiples.discard(0)
    # ~ return [len(multiples), max(multiples)]




import itertools
def find_mult_3(num):
    # your code here
    r=[]
    l=len(str(num))
    for i in range(1,l+1):
        p=itertools.permutations(str(num),i)
        for e in set(p):
            if int(''.join(e))%3==0:
                r.append(int(''.join(e)))
    r=list(set(r)-{0})
    return [len(r),max(r)]

for num in [6063,11111112323423400011000]:
  print(find_mult_3(num))
