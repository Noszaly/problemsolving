module Pos
    export pos_average

    n2(x)=div(x*(x-1),2)
    function pos_average(s)
      delta=Int('0')
      ll=strip.(split(s,','))
      h=length(ll[1])
      fr=[fill(0,10) for _ in 1:h]
      for v in ll
        for i in 1:h
          fr[i][Int(v[i])-delta+1]+=1
        end
      end
      N=h*n2(length(ll))
      K=0
      for i in 1:h
        K+=sum(n2(v) for v in fr[i])
      end
      (K/N)*100.0 
    end

end

using .Pos

for s in ["444996, 699990, 666690, 096904, 600644, 640646, 606469, 409694, 666094, 606490", "466960, 069060, 494940, 060069, 060090, 640009, 496464, 606900, 004000, 944096","4444444, 4444444, 4444444, 4444444, 4444444, 4444444, 4444444, 4444444"]
  println(pos_average(s))
end
