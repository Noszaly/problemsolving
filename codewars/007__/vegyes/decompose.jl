module Sq
  export decompose

  found=0
  akt=fill(0,10)
  function dec(n,level,p)
  global found
    (0==n && level>2)&&(found=level;return)
    (length(akt)<level)&& push!(akt,fill(0,10)...)
    m=min(Int(floor(sqrt(n))),p-1)
    while m>0 && found==0
      akt[level]=m
      dec(n-m*m,level+1,m)
      m-=1
    end
  end
  
  function decompose(n)
    global found
    found=0
    dec(n*n,1,n)
    if found>0 reverse(akt[1:found-1]) else nothing end
  end

end

using .Sq

for n in [50,5,2,7,1,77,777,7777,77777,777777,888888888]
  show(decompose(n))
  println()
end

