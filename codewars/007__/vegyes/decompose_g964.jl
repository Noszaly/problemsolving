module Sq
    export decompose

    function decompose(n)
        function loop(s, i)
            if s < 0
                return nothing
            end
            if s == 0
                return Int[]
            end
            for j in i-1: -1: 0
                sub = loop(s - j ^ 2, j)
                if sub != nothing
                    return Int[sub; [j]]
                end
            end
        end
        loop(n ^ 2, n)
    end

end

using .Sq

for n in [50,5,2,7,1,77,777,7777,77777,777777,888888888]
  show(decompose(n))
  println()
end

