# ~ def josephus(xs, k):
    # ~ i, ys = 0, []
    # ~ while len(xs) > 0:
        # ~ i = (i + k - 1) % len(xs)
        # ~ ys.append(xs.pop(i))
    # ~ return ys

def josephus(items,k):
  if len(items)==0: return []
  if len(items)==1: return [items[0]]
  i = ((k-1)%len(items))
  return [items[i]] + josephus(items[i+1:]+items[:i], k)

print(josephus(list(range(100000)),3)[-1])
