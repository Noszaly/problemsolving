def josephus(items,k):
  n=len(items)
  ret=[]
  if n>0:
    nxt=[i+1 for i in range(n)]; nxt[n-1]=0
    pos=n-1
    k-=1
    while n>0:
      for _ in range(k%n): pos=nxt[pos]
      ret.append(items[nxt[pos]])
      nxt[pos]=nxt[nxt[pos]]
      n-=1
  return ret
  
#print(josephus([1,2,3,4,5,6,7,8,9,10],1))
# ~ print(josephus(["C","o","d","e","W","a","r","s"],4),"\n",['e', 's', 'W', 'o', 'C', 'd', 'r', 'a'],sep='')
# ~ print(josephus([],123))
# ~ print(josephus([1,2,3,4,5,6,7],3),[3, 6, 2, 7, 5, 1, 4])
print(josephus(list(range(100000)),3)[-1])
