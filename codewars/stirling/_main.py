# particiok szama
def makec():
  dp=[[-1]*1801 for _ in range(1801)]
  for n in range(1801):
    dp[n][n]=1
    dp[n][1]=1
    dp[n][0]=0
  def c(n,k):
    if k>n: return 0
    if dp[n][k]<0:
      dp[n][k]=k*c(n-1,k)+c(n-1,k-1)
    return dp[n][k]
  return c

c=makec()

def combs_non_empty_boxes(n,k):
    if k>n: combs="It cannot be possible!"
    else: combs=c(n,k)
    return combs

while True:
  a,b=[int(v) for v in input().split()]
  if a<0: break
  print(combs_non_empty_boxes(a,b)%1000000009)