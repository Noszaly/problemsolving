# tle, rossz
# https://www.codewars.com/kata/combinations-in-a-set-using-boxes-ii/train/python

def combs_non_empty_boxesII(N): # golyok 
  c=[0]*(N+1)
  c[1]=1
  for n in range(2,N+1):
    c[n]=1
    for k in range(n-1,1,-1):
      c[k]=c[k-1]+k*c[k]
  s,mx,kmx=0,0,0
  for k in range(N,0,-1):
    ck=c[k]
    s+=ck
    if ck>mx: 
      mx=ck
      kmx=k
  #print(c)
  return [s,mx,kmx]


while True:
  a=int(input())
  if a<0: break
  print(combs_non_empty_boxesII(a))