function sumofintervals(a)
	sort!(a,by=x->x[2])
	st=[a[1]]
	for v in a[2:end]
		t=st[end]
		if t[2]<v[1]
			push!(st,v)
		else
			x=min(t[1],v[1])
			while length(st)>0 && x<=st[end][2]
				x=min(x,st[end][1])
				pop!(st)
			end
			push!(st,(x,v[2]))
		end
#println(st)

	end
	s=0
	while length(st)>0
		t=pop!(st)
		s+=t[2]-t[1]
	end
	s
end

tests=[
[[(-99, -22), (73, 75), (2, 89), (-25, 30), (98, 99), (46, 62), (85, 87), (9, 96), (-43, 62), (69, 87), (-51, 100), (-71, -19)],199],
[[(76, 89), (-68, -10), (-11, 36), (54, 61), (42, 44), (-90, 66), (90, 91), (69, 72), (75, 88), (-6, 92), (-96, 97), (86, 93), (-58, 39), (7, 98), (-66, 7)],194],
[[(1,5)],4], 
[[(1,5),(6,10)],8], 
[[(1, 5), (1, 5)],4], 
[[(1, 4), (7, 10), (3, 5)],7] 
]

for t in tests
	println(sumofintervals(t[1])==t[2])
end
