def cookie(x):
    ret="Who ate the last cookie? It was "
    x=type(x)
    if x==str: ret+="Zach!"
    elif x in [int,float]: ret+="Monica!"
    else: ret+="the dog!"
    return ret
    


assert_equals=lambda x,y: print(x,y,x==y)

assert_equals(cookie("Ryan"), "Who ate the last cookie? It was Zach!")
assert_equals(cookie(2.3), "Who ate the last cookie? It was Monica!")
assert_equals(cookie(26), "Who ate the last cookie? It was Monica!")
assert_equals(cookie(True), "Who ate the last cookie? It was the dog!")