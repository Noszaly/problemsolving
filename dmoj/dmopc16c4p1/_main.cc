#include <cstdio>
char out[2000000+7];
#define __getchar getchar_unlocked
typedef long  long int LLI;
template<typename T> T getnum(){
  int c;
  while((c=__getchar())<'0' || c>'9');
  T x=c-'0';
  while((c=__getchar())>='0' && c<='9'){
    x=10*x+c-'0';
  }
  return x;
}

int main(){
  int T=getnum<int>();
  char*p=out;
  while(T--){
    long long int n=getnum<LLI>();
    if((n&(n-1))!=0){
      *(p++)='F';
    }else{
      *(p++)='T';
    }
    *(p++)='\n';
  }
  *(p-1)=0;
  puts(out);

  return 0;
}