#include<bits/stdc++.h>
using namespace std;
#define xx first
typedef long long ll;
#define yy second
vector<pair<int,int>> adj[200001];
#define gc getchar_unlocked
int gi() {
  int res=0;
  char c;
  while((c=gc()) && !(c>='0' && c<='9'));
  do {
    res=res*10+c-'0';
  }while((c=gc()) && (c>='0' && c<='9'));
  return res;
}

const int MAXN=200001;

int par[MAXN], sz[MAXN];

void init() {
	memset(par, -1, sizeof par);
	for(int i=0;i<MAXN;++i) sz[i]=1;
}

int get(int x) {
	if(par[x]==-1) return x;
	return par[x]=get(par[x]);
}

void merge(int x, int y) {
	int px=get(x), py=get(y);
	
	if(px==py) return ;
	
	if(sz[px]>sz[py]) {
		swap(px, py);
		swap(x, y); //:) lyft
	}
	
	par[px]=py;
	sz[py]+=sz[px];
}

struct el {
  int a,b,c;
  bool operator<(const el& masik) const {
    return c<masik.c;
  }
};


int main() {
  ios_base::sync_with_stdio(false);
  cin.tie(0);
  cout.tie(0);
  int n,m;
  n=gi();
  m=gi();
  vector<el> lst;
  for(int i=0;i<m;++i) {
    int a,b,c;
    a=gi();
    b=gi();
    c=gi();
    lst.push_back({a,b,c});
  }
  sort(lst.begin(), lst.end());
  init();
  for(int i=lst.size()-1;i>=0;i--) {
    if(get(lst[i].a)!=get(lst[i].b)) {
      merge(lst[i].a, lst[i].b);
      adj[lst[i].a].push_back({lst[i].b, lst[i].c});
      adj[lst[i].b].push_back({lst[i].a, lst[i].c});
    }
  }
  vector<int> dist(n+1, -1);
  deque<pair<int,int>> dq;
  dq.push_back(make_pair((int)2e9, 1));
  
  while(!dq.empty()) {
    auto akt=dq.front();dq.pop_front();
    if(akt.first<dist[akt.second]) continue ;
    for(auto i:adj[akt.second]) {
      int val=min(i.second, akt.first);
      if(val>dist[i.first]) {
        dist[i.first]=val;
        if(dq.empty() || dq.front().first>val) {
          dq.push_back(make_pair(val, i.first));
        }else {
          dq.push_front(make_pair(val, i.first));
        }
      }
    }
  }
  
  dist[1]=0;
  for(int i=1;i<=n;++i) cout<<dist[i]<<"\n";
}
