# lcs
na,nb=map(int,input().split())
pa=list(map(int,input().split()))
pb=list(map(int,input().split()))
dd=[0]*1001
for v in pa:
  dd[v]=1
for v in pb:
  if dd[v]==1: dd[v]=2

a=[0]+[v for v in pa if dd[v]==2]
na=len(a)  
b=[0]+[v for v in pb if dd[v]==2]
nb=len(b)  

    
x=[0]*nb
px=[0]*nb
for i in range(1,na):
  ai=a[i]
  x,px=px,x
  for j in range(1,nb):
    if ai==b[j]: x[j]=1+px[j-1]
    else: x[j]=max(x[j-1],px[j])

print(x[nb-1])
