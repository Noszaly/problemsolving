from collections import deque
# sys.readline nem gyorsit
for _ in range(int(input())):
  tab=[]
  R=int(input())
  C=int(input())
  for _ in range(R):
    tab.append(input())
  INF=1000000
  dd=[[INF]*C for _ in range(R)]
  qq=deque()

  def ins(i,j,d):
    global R,C
    if i<0 or i>=R or j<0 or j>=C: return
    if tab[i][j]=='*' or d>=dd[i][j]: return
    qq.append((i,j)); dd[i][j]=d


  ins(0,0,1)
  while len(qq)>0:
    x,y=qq.popleft() # early break does not count (for speed)
    d=dd[x][y]+1
    c=tab[x][y]
    if c=='+':
      ins(x-1,y,d);ins(x+1,y,d)
      ins(x,y-1,d);ins(x,y+1,d)
    elif c=='-':
      ins(x,y-1,d);ins(x,y+1,d)
    elif c=='|':
      ins(x-1,y,d);ins(x+1,y,d)

  if dd[R-1][C-1]==INF:
    print(-1)
  else:
    print(dd[R-1][C-1])
