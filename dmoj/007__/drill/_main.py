table={}
dirs={'l':[-1,0],'r':[1,0],'d':[0,-1],'u':[0,1]}
add=lambda a,b: (a[0]+b[0],a[1]+b[1])
def move(p,d,s):
  d=dirs[d]
  r=0
  for _ in range(s):
    p=add(p,d)
    if table.get(p)!=None: r=1
    table[p]=True
  return r,p


p=[0,0]
_,p=move(p,'d',3)
_,p=move(p,'r',3)
_,p=move(p,'d',2)
_,p=move(p,'r',2)
_,p=move(p,'u',2)
_,p=move(p,'r',2)
_,p=move(p,'d',4)
_,p=move(p,'l',8)
_,p=move(p,'u',2)

#print(p)

while True:
  d,s=input().split()
  if d=='q': break
  s=int(s)
  r,p=move(p,d,s)
  if r>0: 
    print('{0} {1} DANGER'.format(*p))
    break
  else:
    print('{0} {1} safe'.format(*p))
