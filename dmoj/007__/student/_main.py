pr=[0]*4
for i in range(4):
  pr[i]=int(input()) # ass: increasing
S=int(input())
akt=[0]*4
tot=0
mn=S+1  

def gen(lev,s):
  global tot, mn
  if lev==0:
    if s%pr[0]==0:
      akt[0]=s//pr[0]
      tot+=1
      mn=min(mn,sum(akt))
      print("# of PINK is {0:d} # of GREEN is {1:d} # of RED is {2:d} # of ORANGE is {3:d}".format(*akt))
    return
  p=pr[lev]
  q=s//p; s-=q*p
  for i in range(q,-1,-1):
    akt[lev]=i
    gen(lev-1,s)
    s+=p

gen(3,S)
print("Total combinations is {0:d}.".format(tot))
print("Minimum number of tickets to print is {0:d}.".format(mn))
