N=int(input())
arr=list(map(int,input().split()))
M=int(input())
bad=[(0,0)]*M
for i in range(M):
  a,d=map(int,input().split())
  bad[i]=(a,d)
#bad.sort()
#print(bad)
g=sum(arr)
for a,d in bad:
  mn=min(arr[a-1],arr[a])
  cs=min(d,mn)
  g-=cs
print(g)
