def chk(x,y):
  nx=len(x); ny=len(y)
  mn=min(nx,ny)
  i=0
  while i<mn and x[i]==y[i]: i+=1
  if i==mn: return False
  i=nx-1; j=ny-1
  while i>=0 and j>=0 and x[i]==y[j]: 
    i-=1; j-=1 
  if i<0 or j<0: return False
  return True
T=int(input())
for _  in range(T):
  a=input()
  b=input()
  c=input()
  if chk(a,b) and chk(a,c) and chk(b,c): print('Yes')
  else: print('No')
  
