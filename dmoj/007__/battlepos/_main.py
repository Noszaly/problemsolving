import sys
input=sys.stdin.readline

nst=int(input())
st=[0]*(nst+2)
nr=int(input())
nw=int(input())
for _ in range(nw):
  a,b,nt=map(int,input().split())
  st[a]+=nt
  st[b+1]-=nt
ans=0
s=0
for i in range(1,nst+1):
  s+=st[i]
  ans+=(s<nr)
print(ans)
