# a b c d e f g h i j k l m n o p r s t u v w x y z
dd={}
dd['b']='bac'
dd['c']='cad'
dd['d']='def'
dd['f']='feg'
dd['g']='geh'
dd['h']='hij'
dd['j']='jik'
dd['k']='kil'
dd['l']='lim'
dd['m']='mon'
dd['n']='nop'
dd['p']='por'
dd['r']='ros'
dd['s']='sut'
dd['t']='tuv'
dd['v']='vuw'
dd['w']='wux'
dd['x']='xuy'
dd['y']='yuz'
dd['z']='zuz'
for v in ['a','e','i','o','u']:
  dd[v]=v
  
ans=[]
for v in list(input()):
  ans.append(dd[v])
print(''.join(ans))
