from sys import stdin; input=stdin.readline

for _ in range(int(input())):
  ll=[c for c in input()]+[' ']; n=len(ll)
  szo=0
  for i in range(n):
    c=ll[i]
    if szo>0:
      if c.isalnum(): szo+=1
      else:
        if szo==4: ll[i-4:i]=['*']*4
        szo=0
    else:
      szo+=c.isalnum()
  print(''.join(ll[:-1]))
