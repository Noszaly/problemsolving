from sys import stdin
input=stdin.readline



from collections import deque
adj=[[False]*54 for _ in range(54)]

def modif(x,y,v=True):
  adj[x][y]=adj[y][x]=v


orig=[[],[6],[6],[4,5,6,15],[5,6],[6],[7],[8],[9],[10,12],[11],[12],[13],[14,15],[],[],[17,18],[18],[]]
for a in range(19): 
  for b in orig[a]:
    modif(a,b)

def ff():
  S=int(input())
  INF=10000
  dd=[INF]*54; dd[S]=0
  qq=deque([S])
  while len(qq)>0:
    S=qq.popleft()
    d=dd[S]+1
    if d>2: break
    for i in range(1,54):
      if adj[S][i]==True and d<dd[i]:
        qq.append(i)
        dd[i]=d
  return dd.count(2)  

def ss():
  S=int(input())
  T=int(input())
  INF=10000
  dd=[INF]*54; dd[S]=0
  qq=deque([S])
  while len(qq)>0:
    S=qq.popleft()
    if S==T:
      return(str(dd[S]))
    d=dd[S]+1
    for i in range(1,54):
      if adj[S][i]==True and d<dd[i]:
        qq.append(i)
        dd[i]=d
  return 'Not connected'  

while True:
  c=input()
  if c=='q': break
  if c=='i':
    x=int(input());y=int(input());
    modif(x,y)
    continue
  if c=='d':
    x=int(input());y=int(input());
    modif(x,y,False)
    continue
  if c=='n':
    print(sum(adj[int(input())]))
    continue
  if c=='f':
    print(ff())
    continue
  if c=='s':
    print(ss())
    continue
