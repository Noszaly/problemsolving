# shahir @ dmoj
def mk(N):
  H=[i for i in range(N+1)]
  def find(x):
    if H[x]!=x:
      H[x]=find(H[x])
    return H[x] 

  def unio(x,y):
    if x!=y:
      x=find(x)
      y=find(y)
      H[x]=y
  return find,unio


V,E,S,T=map(int,input().split())
ff,uu=mk(V)
for i in range(E):
  a,b=map(int,input().split())
  uu(a,b)
  
if ff(S)==ff(T):
  print("GO SHAHIR!")
else:
  print("NO SHAHIR!")
