def primeSieve(N): # spec
  p=[1]*N
  for i in range(4,N,2): p[i]=2
  p[0]=p[1]=0
  p[2]=p[3]=1;
  i=3; i2=9
  while i2<N:
    if 1==p[i]:
      j=i2; delta=2*i
      while j<N: p[j]=i; j+=delta
    i2+=4*(i+1); i+=2
  return p


Q=int(input())
N=0
data=[]
for i in range(Q):
  a,b,k=map(int,input().split())
  N=max(N,b)
  data.append([a,b,k])
N+=3
#N=10000000
p=primeSieve(N)
# for i in range(N):
#   print(i,p[i]) 

for i in range(4,N):
  d=p[i]
  if 1==d: continue
  ii=i//d
  if 0==ii%d: p[i]=p[ii]
  else: p[i]=p[ii]+1

# for i in range(N):
#   print(i,p[i]) 

mm=[0,2,2*3,2*3*5,2*3*5*7,2*3*5*7*11,2*3*5*7*11*13,2*3*5*7*11*13*17,2*3*5*7*11*13*17*19]

eset=0
for a,b,k in data:
  eset+=1
  print("Case #{0:d}: ".format(eset),end="")
  if k>8 or b<mm[k]: print("0"); continue
  print(p[a:(b+1)].count(k))