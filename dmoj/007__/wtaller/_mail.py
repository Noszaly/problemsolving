from sys import stdin; input=stdin.readline

V,E=map(int,input().split())
gg=[[] for _ in range(V+1)]
for _ in range(E):
  x,y=map(int,input().split()) # h(x)>h(y)
  gg[y].append(x)
p,q=map(int,input().split())
inQQ=[False]*(V+1)
QQ=[0]*(V+1)
tail=0
for v in gg[p]:
  if inQQ[v]==True: continue
  QQ[tail]=v; tail+=1
  inQQ[v]=True

for v in gg[q]:
  if inQQ[v]==True: continue
  QQ[tail]=v; tail+=1
  inQQ[v]=True

head=0
found=-1
while head!=tail:
  x=QQ[head];head+=1
  if x in [p,q]: found=x; break
  for v in gg[x]:
    if inQQ[v]==True: continue
    QQ[tail]=v; tail+=1
    inQQ[v]=True

if found<0: print("unknown")
else:
  if found==p: print("yes")
  else: print("no")
  
