# https://dmoj.ca/problem/ccc07s4
n=int(input())
gg=[[] for _  in range(n+1)] # reverse edges
while True:
  a,b=map(int,input().split())
  if 0==a and 0==b: break
  gg[b].append(a)

ways=[-1]*(n+1)
ways[1]=1
def gen(b):
  if ways[b]>=0: return
  w=0
  for a in gg[b]:
    gen(a)
    w+=ways[a]
  ways[b]=w

gen(n)
print(ways[n])

  
