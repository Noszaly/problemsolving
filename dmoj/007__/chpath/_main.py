from collections import deque

N=int(input())
gg=[[] for _ in range(N+1)]
for i in range(1,N+1):
  arr=list(int(v) for v in input().split())
  if arr[0]==0: continue
  gg[i]=arr[1:]

qq=deque([1])
INF=1000000000
mn=INF
dd=[INF]*(N+1)
dd[0]=0
dd[1]=1
while len(qq)>0:
  a=qq.popleft()
  d=dd[a]
  if len(gg[a])==0:
    mn=min(d,mn)
    continue
  d+=1
  for b in gg[a]:
    if d<dd[b]:
      dd[b]=d
      qq.append(b)
  
if dd.count(INF)>0:
  print('N')
else:
  print('Y')
print(mn)
