N=int(input())
K=int(input())

f=[[-1]*(N+1) for _ in range(N+1)]
for i in range(N):
  for j in range(i+1,N+1): f[i][j]=0
# f[n][k]: pozitiv tagu n osszegu monoton "novekvo" sorozatok szama
f[0][0]=1
for i in range(1,N+1): 
  f[i][1]=1
for i in range(1,N+1):
  f[0][i]=f[i][0]=0
  f[i][i]=1

def comp(n,k):
  if f[n][k]>=0: return 
  fnk=0
  for i in range(k): # i nulla keletkezik az elejen
    comp(n-k,k-i)
    fnk+=f[n-k][k-i]
  f[n][k]=fnk


comp(N,K)
print(f[N][K])
