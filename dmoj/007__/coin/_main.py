from sys import stdin
input=stdin.readline
nd,nstores=map(int,input().split())
d=list(map(int,input().split()))
stores=[]
M=0
for i in range(nstores):
  m,td=map(int,input().split())
  stores.append([td,m,i])
  M=max(M,m)

#print(M)

INF=1000000000
reach=[INF]*(M+1)
reach[0]=0
def coin(c):
  #print(M)
  i=c
  while i<=M:
    reach[i]=min(reach[i],reach[i-c]+1)
    i+=1

stores.sort(key=lambda x:x[0])
t=0
for i in range(nstores):
  td,m,_=stores[i]
  while t<td:
    coin(d[t])
    t+=1
  if reach[m]<INF:
    stores[i][1]=reach[m]
  else:
    stores[i][1]=-1
stores.sort(key=lambda x:x[2])

for v in stores:
  print(v[1])
 
