#include <cstdio>
#include <vector>
using namespace std;
typedef vector<int> VI;

struct Sudoku{
  vector<VI>&tab;
  int N,N2,nEmp;
  vector<VI> row, col, sqr;
  VI empI,empJ;
  bool ok;
  Sudoku(vector<VI>& _tab,int _N):tab(_tab),N(_N),N2(_N*_N){
    row.resize(N2); for(auto& v:row){v.resize(N2);}
    col.resize(N2); for(auto& v:col){v.resize(N2);}
    sqr.resize(N2); for(auto& v:sqr){v.resize(N2);}
  }
  void init(){
    for(int i=0;i<N2;i++){
      for(int j=0;j<N2;j++){
        row[i][j]=col[i][j]=sqr[i][j]=1;
      }
    }
    empI.clear();
    empJ.clear();
    for(int i=0;i<N2;i++){
      for(int j=0;j<N2;j++){
        auto k=tab[i][j];
        if(k<0){ 
          empI.push_back(i); 
          empJ.push_back(j); 
          continue;
        }
        row[i][k]=0; col[j][k]=0; sqr[N*(i/N)+(j/N)][k]=0;
      }
    }
    nEmp=empI.size();
    ok=false;
//printf("nEmp:%d N:%d N2:%d\n",nEmp,N,N2);
  }
  void gen(int level){
    if(level==nEmp){ ok=true; return;}
    auto i=empI[level];
    auto j=empJ[level];
    auto& r=row[i]; 
    auto& c=col[j]; 
    auto& s=sqr[N*(i/N)+(j/N)];
    for(int k=0;k<N2;k++){
      if( r[k] && c[k] && s[k]){
        r[k]=c[k]=s[k]=0;
        tab[i][j]=k;
        gen(level+1);
        r[k]=c[k]=s[k]=1;
        if(true==ok){return;}
      }
    }
  }  
  void solve(){
    init();
    gen(0);
  }    
};

int main(){
  vector<VI> tab(4);
  for(auto& v: tab){v.resize(4);}
  char line[32];
  Sudoku ss(tab,2);
  int Q;scanf("%d",&Q);
  while(Q--){
    for(int i=0;i<4;i++){
      scanf("%s",line);
      for(int j=0;j<4;j++){
        if('X'==line[j]){
          tab[i][j]=-1;
        }else{
          tab[i][j]=line[j]-'1';
        }
      }
    }
    ss.solve();
    line[4]=0;
    for(int i=0;i<4;i++){
      for(int j=0;j<4;j++){
        line[j]=tab[i][j]+'1';
      }
      printf("%s\n",line);
    }
  }
  return 0;
}