def sudoku(tab,N):
  N2=N*N
  row=[ [True]*N2 for _ in range(N2) ]
  col=[ [True]*N2 for _ in range(N2) ]
  sqr=[ [True]*N2 for _ in range(N2) ]
  emp=[]
  for i in range(N2):
    for j in range(N2):
      k=tab[i][j]
      if k<0: emp.append((i,j)); continue
      row[i][k]=False; col[j][k]=False; sqr[N*(i//N)+(j//N)][k]=False
  nEmp=len(emp)
  ok=False
  def gen(level):
    nonlocal N,N2,nEmp,ok
    if level==nEmp: ok=True; print(tab); return
    i,j=emp[level]
    r=row[i]; c=col[j]; s=sqr[N*(i//N)+(j//N)]
    for k in range(N2):
      if r[k] and c[k] and s[k]:
        r[k]=c[k]=s[k]=False
        tab[i][j]=k
        gen(level+1)
        r[k]=c[k]=s[k]=True
#        if True==ok: return
  gen(0)
        

def to(c):
  if c=='X': return -1
  return ord(c)-ord('1')


from sys import stdin
input=stdin.readline  
Q=int(input())
while Q>0:
  tab=[]
  for i in range(4):
    tab.append([ to(c) for c in input().strip() ])
#  print(tab)
  sudoku(tab,2)
  out=""
  for i in range(4):
    out=out+"".join([str(x+1) for x in tab[i]])+"\n"
  print(out,end='')
  Q-=1
