n=min(9,int(input()))
m=min(9,int(input()))
ans=0
for i in range(1,n+1):
  ans+=((10-i)<=m)
s1="are";s2="ways"
if ans==1: s1="is"; s2="way"
print("There {0:s} {1:d} {2:s} to get the sum 10.".format(s1,ans,s2))
