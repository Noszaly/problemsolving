# hatarokkal egyszerubb lenne a gen
# van szoba folytonos fal nelkul
K=int(input())
r=int(input())
c=int(input())
tab=[]
for _ in range(r):
  tab.append([c for c in input().strip()])
vis=0
def gen(i,j):
  global vis,r,c
  if tab[i][j]!='.': return
  tab[i][j]=0
  vis+=1
  if i>0: gen(i-1,j)
  if i<r-1: gen(i+1,j)
  if j>0: gen(i,j-1)
  if j<c-1: gen(i,j+1)

vv=[]
for i in range(r):
  for j in range(c):
    if '.'==tab[i][j]: 
      vis=0
      gen(i,j)
      vv.append(vis)  
vv.sort(reverse=True)
vv=vv+[K+1]
ok=0
while K>=vv[ok]:
  K-=vv[ok]
  ok+=1
if 1==ok:
  print(str(ok)+" room, "+str(K)+" square metre(s) left over")
else:
  print(str(ok)+" rooms, "+str(K)+" square metre(s) left over")
