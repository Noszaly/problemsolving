key=[ord(c)-ord('A') for c in input().strip()]
w=[c for c in input().strip() if ord('A')<=ord(c)<=ord('Z')]
g = lambda x,c: chr(ord('A')+(ord(x)-ord('A')+c)%26)
nk=len(key)
k=0
for i in range(len(w)):
  w[i]=g(w[i],key[k])
  k=(k+1)%nk
print(''.join(w))
  
