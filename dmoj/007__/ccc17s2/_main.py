n=int(input())
arr=list(map(int,input().split()))
arr.sort()
sol=[""]*n
s=0
i=(n-1)//2; j=i+1
while i>0:
  sol[s]=str(arr[i]); sol[s+1]=str(arr[j])
  s+=2; i-=1; j+=1
sol[s]=str(arr[0])
if j<n: sol[s+1]=str(arr[j])
print(" ".join(sol))
  