def sumDiv(x):
  s=1
  for d in range(2,x):
    if 0==x%d: s+=d
  return s
q=int(input())
for _ in range(q):
  x=int(input())
  sdx=sumDiv(x)
  print(x," is ",sep='',end='')
  if sdx<x: 
    print("a deficient number.")
    continue
  if sdx==x:
    print("a perfect number.")
    continue
  print("an abundant number.")
  
    
    
