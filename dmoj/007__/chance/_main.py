F=int(input())
M=int(input())
pont=[0]*5
volt=[]
for _ in range(M):
  a,b,pa,pb=map(int,input().split())
  if a>b: a,b=b,a; pa,pb=pb,pa
  volt.append((a,b))
  if pa>pb: pont[a]+=3
  elif pa==pb: pont[b]+=1; pont[a]+=1
  else: pont[b]+=3
mind=list(set([(1,2),(1,3),(1,4),(2,3),(2,4),(3,4)]).difference(volt))
M=len(mind)
#print(mind)
ans=0
def gen(i):
  global ans,F,M
  if i==M:
    if [j for j in range(1,5) if pont[j]==max(pont)] == [F]: ans+=1
    return
  a,b=mind[i]
  pont[a]+=3
  gen(i+1)
  pont[a]-=2; pont[b]+=1
  gen(i+1)
  pont[a]-=1; pont[b]+=2
  gen(i+1)
  pont[b]-=3

gen(0)
print(ans)
