from sys import stdin; input=stdin.readline

def fenwick(N):
  tree=[0]*(N+1)
  def update(x,val):
    nonlocal N
    while x<=N:
      tree[x]+=val
      x+=(x&(-x))
  def query(x):
    ans=0
    while x>0:
      ans+=tree[x]
      x-=(x&(-x))
    return ans
  return update,query,tree

ng=int(input())
narr=int(input())
arr=[]
mx=0
for _ in range(narr):
  t=int(input())
  mx=max(mx,t)
  arr.append(t)
uu,qq,_=fenwick(mx)
for x in range(1,mx+1): uu(x,1)

uu(arr[0],-1)
ok=1
for i in range(1,narr):
  x=arr[i]
  k=qq(x)
  if 0==k: break
  ok+=1
  a=1; b=x;
  while a<b:
    mid=(a+b)//2
    if k<=qq(mid): b=mid
    else: a=mid+1
  uu(b,-1)    
print(ok)  
