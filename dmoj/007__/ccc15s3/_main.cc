// igy atirva arr-mentesen sem gyorsult
#include<bits/stdc++.h>
using namespace std;

template<typename T> struct Fenwick{
	int aN;
	vector<T> tree;
	void init(int _aN){//aN<=N
		aN=_aN;
		if(aN+1>int(tree.size())){
			tree.resize(aN+1);
		}
		fill(tree.begin(),tree.begin()+aN+1,0);
	}
	T query(int x){
		T ans=0;
		while(x>0){
			ans+=tree[x];
			x-=(x&(-x));
		}
		return ans;
	}
	void update(int x, const T val=1){
		while(x<=aN){
			tree[x]+=val;
			x+=(x&(-x));
		}
	}
};


//init+update+query
int main(){
  int ng, narr; scanf("%d%d",&ng,&narr);  
  Fenwick<int> bit;  
  bit.init(ng);
  for(int i=1;i<=ng;i++){bit.update(i,1);}
  
  int ok=0;
  for(int i=0;i<narr;i++){
    int x; scanf("%d",&x);
    int k=bit.query(x);
    if(0==k){break;}
    ok+=1;
    int a=1,b=x;
    while(a<b){
      int mid=(a+b)/2;
      if(k<=bit.query(mid)){
        b=mid;
      }else{
        a=mid+1;
      }
    }
    bit.update(b,-1);
  }
  printf("%d\n",ok);
  return 0;
}