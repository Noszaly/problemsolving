from sys import stdin
input=stdin.readline
n=int(input())
nums=[int(input()) for _ in range(n)]
PL=max(nums)

def isp(x):
  if 1==x: return False
  if x<4: return True
  if 0==x%2 or 0==x%3: return False
  L=int(x**0.5)
  d=5
  while d<=L:
    if 0==x%d: return False
    d+=2
  return True

plist=[2,3]
x=5
while x*x<=PL:
  if isp(x): plist.append(x)
  x+=2

#print(plist)
plist.append(-1)
def pfact(x):
  f=[]
  plist[-1]=int(x**0.5)+3  
  i=-1
  while True:
    i+=1
    d=plist[i]
    if d*d>x: break
    while 0==x%d:
      f.append(d)
      x//=d # ?       
  if x>1: f.append(x)
  return f


for num in nums:
  print(" ".join(list(map(str,pfact(num)))))  
  
    
  