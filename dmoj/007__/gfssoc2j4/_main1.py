from sys import stdin
input=stdin.readline
N,Q=map(int,input().split())
arr=list(map(int,input().split()))
s=0
for i in range(N):
  s+=arr[i]
  arr[i]=s
for i in range(Q):
  a,b=map(int,input().split())
  if 1==a:
    print(s-arr[b-1])
  else:
    print(s-arr[b-1]+arr[a-2])