from sys import stdin
input=stdin.readline
N,Q=map(int,input().split())
arr=[0]+[int(x) for x in input().split()]
sarr=0
for i in range(1,N+1):
  sarr+=arr[i]
  arr[i]=sarr
for _ in range(Q):
  a,b=map(int,input().split())
  print(sarr-arr[b]+arr[a-1])