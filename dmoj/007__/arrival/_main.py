# s=240 vmilyen egysegben
# s=120*(v=2,1 egyseg/perc)
def solve(m,r,R):
  s=0
  while m<r and s<240:
    m+=1; s+=2
  while m<R and s<240:
    m+=1; s+=1
  while s<240:
    m+=1; s+=2
  return ((m//60)%24,m%60)
  
hh,mm=map(int,input().split(':'))
m=hh*60+mm

if m<10*60:
  hh,mm=solve(m,7*60,10*60)
elif m<19*60:
  hh,mm=solve(m,15*60,19*60)
else:
  hh,mm=solve(m,-1,-1)

print("{0:02d}:{1:02d}".format(hh,mm))
