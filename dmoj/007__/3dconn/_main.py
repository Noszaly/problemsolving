from sys import stdin; input=stdin.readline

rr=0
cc=[0]*6
rc=[[0]*6 for _ in range(6)]
ans=0

def ins(i,j,c):
  global rr,ans
  if c=='Y':
    ans+=max(rr-3,0)
    rr=0
    ans+=max(cc[j]-3,0)
    cc[j]=0
    ans+=max(rc[i][j]-3,0)
    rc[i][j]=0
  else:
    rr+=1; cc[j]+=1; rc[i][j]+=1

def egysor():
  while True:
    w=input().strip()
    if len(w)>0: return w
  return None
  
for k in range(6):
  for i in range(6):
    w=egysor()
    for j in range(6):
      ins(i,j,w[j])
    ans+=max(rr-3,0)
    rr=0
  for j in range(6):
    ans+=max(cc[j]-3,0)
    cc[j]=0
  #print(rc)
for i in range(6):
  for j in range(6):
    ans+=max(rc[i][j]-3,0)
    rc[i][j]=0

if ans>0:print(ans) 
else: print('lost')
