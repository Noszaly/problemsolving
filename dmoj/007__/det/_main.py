import sys; input=sys.stdin.readline

from math import gcd
def egcd(a,b):
  xa,xb=(1,0),(0,1)
  while True:
    q,c=divmod(a,b)
    if 0==c: break
    xa,xb=xb,(xa[0]-q*xb[0],xa[1]-q*xb[1])
    a,b=b,c
  return (b,xb)

# while True:
#   a,b=[int(v) for v in input().split()]
#   if a==0: break
#   _,x=egcd(a,b)
#   print(x[0])
# exit(0)

M=10**9+7
n=int(input())
A=[[int(v)%M for v in input().split()] for _ in range(n)]

#print(n,A)
sw=1
ret=-1
mul=1
for i in range(n):
  #print(i,A)  
  nz=0;inz=-1
  for j in range(i,n):
    if A[j][i]!=0:
      nz=1
      inz=j
      break
  if nz==0:
    ret=0
    break
  if inz>i:
    #print('csere')
    A[i],A[inz]=A[inz],A[i]
    sw=-sw
  Aii=A[i][i]=(A[i][i]+M)%M
  #print(i+1," ",n)

  for j in range(i+1,n):
    Aji=(A[j][i]+M)%M
    if Aji==0: continue
    g=gcd(Aii,Aji)
    fi=(Aji//g)
    fj=(Aii//g)
    mul=(mul*fj)%M
    for k in range(i+1,n):
      A[j][k]=(fj*A[j][k]-fi*A[i][k])%M
  
if ret<0:
  ret=sw
  if mul>1:    
    (g,x)=egcd(mul,M)
    ret=(ret*x[0])%M
    if ret<0: ret+=M
  for i in range(n):
    ret=(ret*A[i][i])%M
#print(type(ret))
print(ret)
