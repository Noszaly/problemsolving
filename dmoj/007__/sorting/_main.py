N=int(input())
arr=map(int,input().split())
lo,hi,prev=1,1000000,1 # assumptions
nz=0
res='YES'
for v in arr:
  if v>0:
    if v<prev:
      res='NO'
      break
    prev=v
    if nz>0: hi=min(hi,v)
  else:
    nz+=1
    lo=max(lo,prev)
  if lo>hi:
    res='NO'
    break

print(res)
