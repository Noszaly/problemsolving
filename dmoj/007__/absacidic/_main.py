from sys import stdin; input=stdin.readline

# absolutely acidic @ dmoj
N=int(input())
fr=[0]*(1001)
r=0
for _ in range(N):
  t=int(input())
  fr[t]+=1
  r=max(r,t)

  
mx1=[fr[1],1,1]
mx2=[-1,0,0]
for i in range(2,r+1):
  t=fr[i]
  if t>mx1[0]:
    mx2[:]=mx1
    mx1=[t,i,i]
  elif t==mx1[0]:
    mx1[2]=i
  elif t>mx2[0]:
    mx2=[t,i,i]
  elif t==mx2[0]:
    mx2[2]=i


if mx1[1]<mx1[2]:
  print(mx1[2]-mx1[1])
else:
  print(max(abs(mx1[1]-mx2[1]),abs(mx1[1]-mx2[2])))
