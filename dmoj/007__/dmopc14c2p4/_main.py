from sys import stdin; input=stdin.readline
N=int(input())
arr=[0]*N
s=0
for i in range(N):
  s=s+int(input())
  arr[i]=s
Q=int(input())
for _ in range(Q): 
  a,b=map(int,input().split())
  if 0==a: print(arr[b])
  else: print(arr[b]-arr[a-1])
