# roman numerals - small
ones=['','I','II','III','IV','V','VI','VII','VIII','IX']
tens=['','X','XX','XXX','XL','L','LX','LXX','LXXX','XC']
first={}
least={}
for i in range(1,100):
  x=tens[i//10]+ones[i%10]
  sx=''.join(sorted(list(x)))
  if first.get(sx)==None:
    first[sx]=x
  least[x]=first[sx]

print(least[input()])
