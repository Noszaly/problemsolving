from collections import deque

n=int(input())
gg=[[]]
for i in range(n):
  gg.append([int(v) for v in input().split()])

INF=1000000000
mn=INF
dd=[INF]*(n+1)
dd[0]=0; dd[1]=1
qq=deque([1])
while len(qq)>0:
  a=qq.popleft()
  d=dd[a]
  if len(gg[a])==1:
    mn=min(mn,d) # only one update
    continue
  d+=1
  for b in gg[a][1:]:
    if d<dd[b]:
      dd[b]=d
      qq.append(b)
if dd.count(INF)>0:
  print('N')
else:
  print('Y')
print(min(dd[2:]))
