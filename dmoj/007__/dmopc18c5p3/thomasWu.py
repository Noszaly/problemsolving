from sys import stdin

def two_pointer_approach(li, max_sum):
    l_pointer = 0
    max_count = 0
    current_sum = 0
    current_count = 0

    for r_pointer in range(len(li)):
        current_sum += li[r_pointer]
        current_count += 1

        if current_sum >= max_sum:

            if current_count - 1 > max_count:
                max_count = current_count - 1

            while current_sum >= max_sum:
                current_sum -= li[l_pointer]
                l_pointer += 1
                current_count -= 1

    if current_count > max_count:
        max_count = current_count

    return max_count


a, limit = list(map(int, stdin.readline().split()))
li = list(map(int, stdin.readline().split()))
print(two_pointer_approach(li, limit))
