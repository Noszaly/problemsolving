akt=[0]*3
price=[0]*3
ossz=0
def wr(vege=False):
  global ossz
  if vege==True:
    print('Number of ways to catch fish: {0:d}'.format(ossz))
    return
  if sum(akt)>0:
    print('{0:d} Brown Trout, {1:d} Northern Pike, {2:d} Yellow Pickerel'.format(*akt))
    ossz+=1
  
def gen(lev,left):
  if lev==3:
    wr()
    return
  pl=price[lev]
  for i in range(1+left//pl):
    akt[lev]=i
    gen(lev+1,left-i*pl)
  
for i in range(3):
  price[i]=int(input())
lim=int(input())

# ~ print(price)
# ~ print(lim)

gen(0,lim)
wr(True)
