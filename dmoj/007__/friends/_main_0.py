# https://dmoj.ca/problem/ccc07s3
# azokat melyeknek nincs befoka,
# a feltetelek miatt diszjunkt korok maradnak


dd={}
indeg=[]
gg=[]
n=0
def ins(a,b):
  if dd.get(a)==None:
    dd[a]=n; n+=1; 
    indeg.append(0)
    gg.append([])
  if dd.get(b)==None:
    dd[b]=n; n+=1; 
    indeg.append(0)
    gg.append([])
  a=dd[a]
  b=dd[b]
  indeg[b]+=1
  gg[a].append(b)  


ne=int(input())
for _ in range(ne):
  a,b=map(int,input().split())
  ins(a,b)
nv=len(indeg)
