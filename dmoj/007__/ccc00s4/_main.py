# a club can be used more than once...
from sys import stdin; input=stdin.readline
T=int(input())
n=int(input())
cl=[]
s=0
for _ in range(n):
  t=int(input()); 
  if t>T: continue
  s+=t
  cl.append(t)
INF=1000000000
cl.sort(reverse=True)
#  print(cl)
rr=[INF]*(T+1); rr[0]=0
for h in cl:
  if rr[T]<INF: break
  j=0
  while j+h<=T:
    rr[j+h]=min(rr[j+h],rr[j]+1)
    j+=1
#  print(rr)
if rr[T]<INF:
  print("Roberta wins in "+str(rr[T])+" strokes.")
else:
  print("Roberta acknowledges defeat.")
