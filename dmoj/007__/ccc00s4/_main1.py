from sys import stdin; input=stdin.readline
T=int(input())
n=int(input())
cl=[]
s=0
for _ in range(n):
  t=int(input()); 
  if t>T: continue
  s+=t
  cl.append(t)
INF=1000000000
ans=INF
if s>=T:
  cl.sort(reverse=True)
#  print(cl)
  rr=[INF]*(T+1); rr[0]=0
  for h in cl:
#    if rr[T]<INF: break
    j=T-h
    while j>=0:
      rr[j+h]=min(rr[j+h],rr[j]+1)
      j-=1
  ans=rr[T]
#  print(rr)
if ans<INF:
  print("Roberta wins in "+str(ans)+" strokes.")
else:
  print("Roberta acknowledges defeat.")
