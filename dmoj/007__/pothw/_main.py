# atrendezesi egyenlotlenseg, min/max->ellenkezo/egyforma rendezes
MOD=10007
n=int(input())
arr=[0]*n
for i in range(n):
  arr[i]=int(input())
arr.sort()
ans=0
i,j=0,n-1
while i<j:
  ans=(ans+2*arr[i]*arr[j])%MOD
  i+=1; j-=1
if i==j: ans=(ans+arr[i]*arr[i])%MOD
print(ans)  
