from sys import stdin
input = stdin.readline
T=int(input())
S1=[0]*100001
S2=[0]*100001
while T>0:
  n=int(input())
  for i in range(n):
    S1[i]=int(input())
  n1=n-1
  n2=-1
  a=0
  while True:
    a+=1
    if a>n: break
    if n2>=0 and S2[n2]==a:
      n2-=1
      continue
    while n1>=0 and S1[n1]!=a:
      n2+=1; S2[n2]=S1[n1]; n1-=1
    if n1>=0 and S1[n1]==a:
      n1-=1
    else:
      break
  if a>n:
    print("Y")
  else:
    print("N")

  T-=1