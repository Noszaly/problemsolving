input = __import__("sys").stdin.readline
for _ in range(int(input())):
    b = int(input())
    branch = []
    bad = False
    top = [int(input()) for _ in range(b)]
    for h in range(1, b + 1):
        if h in top:
            while top[-1] != h:
                branch.append(top.pop())
            top.pop()
        else:
            if branch[-1] != h:
                bad = True
                break
            branch.pop()
    print("N" if bad else "Y")