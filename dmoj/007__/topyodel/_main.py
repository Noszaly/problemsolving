n,r=map(int,input().split())
worst=[0]*n
akt=[0]*n
cum=[0]*n
for _ in range(r):
  res=list(map(int,input().split()))
  for i in range(n):
    cum[i]+=res[i]
  tmp=sorted([(cum[i],i) for i in range(n)],reverse=True)
  p=tmp[0][0]; h=1
  for i in range(n):
    pont,idx=tmp[i]
    if pont<p:
      p=pont; h+=1
    akt[idx]=h
    worst[idx]=max(worst[idx],h)
for i in range(n):
  if akt[i]==1:
    print("Yodeller {0:d} is the TopYodeller: score {1:d}, worst rank {2:d}".format(i+1,cum[i],worst[i]))
