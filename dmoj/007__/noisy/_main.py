# dag or not

from collections import deque
nv=int(input())
ne=int(input())
indeg=[0]*(nv+1)
gg=[[] for _ in range(nv+1)]
for _ in range(ne):
  a,b=map(int,input().split())
  indeg[b]+=1
  gg[a].append(b)
qq=deque()
ok=0
for a in range(1,nv+1):
  if indeg[a]==0: 
    qq.append(a)
    ok+=1
while len(qq)>0:
  a=qq.popleft()
  for b in gg[a]:
    indeg[b]-=1
    if indeg[b]==0: 
      qq.append(b)
      ok+=1
if ok==nv:
  print('Y')
else:
  print('N')
