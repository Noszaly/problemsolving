H=int(input())
cs=''.join(['*']*(2*H))
h=''.join([' ']*(2*H))
out=[]
for i in range(1,H+1,2):
	out.append(cs[:i]+h[:2*H-2*i]+cs[:i])

for i in range(H-2,0,-2):
	out.append(cs[:i]+h[:2*H-2*i]+cs[:i])
print('\n'.join(out))  
