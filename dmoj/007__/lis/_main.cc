// lis nlog(n)

#include <bits/stdc++.h>
using namespace std;
#define __getchar getchar_unlocked
int getnum(){
  int c;
  while((c=__getchar())<'0' || c>'9');
  int x=c-'0';
  while((c=__getchar())>='0' && c<='9'){
    x=10*x+c-'0';
  }
  return x;
}
int main(){
  int N=getnum();
  auto arr=vector<int>();
  int prev=-1;
  for(int j=0;j<N;j++){
    int v=getnum();
    if(v==prev){continue;}
    prev=v;
    auto i=lower_bound(arr.begin(),arr.end(),v);
    if(i==arr.end()){
      arr.push_back(v);
    }else{
      *i=v;
    }
  }
  printf("%d\n",int(arr.size()));

  return 0;
}
