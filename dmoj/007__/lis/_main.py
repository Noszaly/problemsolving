# mle az utolso eseteknel
# pypy az elsonel mle

from bisect import bisect_left


N=int(input())
line=input()+' '

def getnum(i):
  while not line[i].isdigit(): i+=1
  x=ord(line[i])-48
  i+=1
  while line[i].isdigit(): 
    x=10*x+ord(line[i])-48
    i+=1
  return i,x
  
arr=[-1]
prev=-1
j=0
for _ in range(N):
  j,v=getnum(j)
  if v==prev: continue
  prev=v
  i=bisect_left(arr,v)
  if i==len(arr):
    arr.append(v)
  else:
    arr[i]=v
print(len(arr)-1)
