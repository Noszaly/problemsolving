# mle az utolso eseteknel
# pypy az elsonel mle

from bisect import bisect_left
input()
arr=[-1]
prev=-1
for v in map(int,input().split()):
  if v==prev: continue
  prev=v
  i=bisect_left(arr,v)
  if i==len(arr):
    arr.append(v)
  else:
    arr[i]=v
print(len(arr)-1)
