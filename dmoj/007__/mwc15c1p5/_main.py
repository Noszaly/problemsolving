# tle
# atirva, wired in remainders
m2=[6,2,4,8]
m3=[1,3,9,7]
m4=[6,4,6,4]
m5=[5,5,5,5]
m6=[6,6,6,6]
m7=[1,7,9,3]
m8=[6,8,4,2]
m9=[1,9,1,9]
R=[[],[],m2,m3,m4,m5,m6,m7,m8,m9]

c2num={}; pa=ord('a')-1; pA=ord('A')-1;
for i in range(1,27):
  c2num[chr(pa+i)]=i%10
  c2num[chr(pA+i)]=i%10
  
def sol(s):
  ls=len(s)
  ans=0
  for i in range(ls):
    x=c2num[s[i]]
    if 0==x: continue
    if x>1:
      ans+=R[x][(i+1)%4]
    else:
      ans+=1
  ans=ans%10
  if 0==ans: ans=10
  return ans


def sol2(s): # lassabb (not faster)
  ans=0
  i=0
  for c in s:
    i+=1
    if 4==i: i=0
    x=c2num[c]
    if 0==x: continue
    if x>1:
      ans+=R[x][i]
    else:
      ans+=1
  ans=ans%10
  if 0==ans: ans=10
  return ans
    
print(sol2(input().strip())+sol2(input().strip()))

 