def mkPow(MOD):
  def pw(x,n):# x<MOD
    if 0==n: return 1
    if 1==n: return x
    ans=pw(x,n//2)
    ans=ans*ans
    if n%2>0: ans*=x
    return ans%MOD # 
  return pw

c2num={}; pa=ord('a')-1; pA=ord('A')-1;
for i in range(1,27):
  c2num[chr(pa+i)]=i
  c2num[chr(pA+i)]=i
  
#print(d)  
pw=mkPow(10)
def sol(s,MOD):
  ls=len(s)
  ans=0
  for i in range(ls):
    x=c2num[s[i]]
    if x>1:
      ans+=pw(x,i+1)
    else:
      ans+=1
  ans=ans%MOD
  if 0==ans: ans=MOD
  return ans
    
print(sol(input().strip(),10)+sol(input().strip(),10))

 