from heapq import heapify, heappop, heappush
nv,ne=map(int,input().split())
gg=[[] for _ in range(nv+1)]
for _ in range(ne):
  a,b,w=map(int,input().split())
  gg[a].append((w,b))
  gg[b].append((w,a))

dd=[-1]*(nv+1)
dd[1]=0
hh=[(0,1)]
while len(hh)>0:
  d,a=heappop(hh)
  for w,b in gg[a]:
    t=d+w
    if dd[b]<0 or t<dd[b]:
      dd[b]=t
      heappush(hh,(t,b))
for i in range(1,nv+1):
  print(dd[i])     
