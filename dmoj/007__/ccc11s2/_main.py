n=int(input())
d={"A":1,"B":2,"C":3,"D":4,"E":5}
X=[0]*n
for i in range(n):
  X[i]=d[input().strip()]
Y=[0]*n
for i in range(n):
  Y[i]=d[input().strip()]
print( [X[i]==Y[i] for i in range(n)].count(True) )