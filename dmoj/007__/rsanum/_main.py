def rsa(x):
  ret=0;d=2
  while ret<3 and d<x:
    if x%d==0: ret+=1
    d+=1
  return ret==2
  
a=int(input())
b=int(input())
ans=0
for x in range(a,b+1):
  ans+=rsa(x)
print("The number of RSA numbers between {0:d} and {1:d} is {2:d}".format(a,b,ans))
