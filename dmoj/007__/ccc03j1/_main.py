t=int(input())
s=int(input())
h=int(input())

line="*"+" "*s+"*"+" "*s+"*"
ans="\n".join([line]*t)
line="*"*(2*s+3)
ans=ans+"\n"+"\n".join([line])
line=" "*(s+1)+"*"
ans=ans+"\n"+"\n".join([line]*h)
print(ans)
