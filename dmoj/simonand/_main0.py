import sys; input=sys.stdin.readline
# dp, knapsack

c,maxW=map(int,input().split())

V=[0];W=[0] # val,weight
for _ in range(c):
  v,w=map(int,input().split())
  if w>maxW:continue
  V.append(v); W.append(w)

ans=sum(V)
nV=len(V)-1
if nV>0 and sum(W)>maxW:
  dp=[[0]*(nV+1) for _ in range(maxW+1)]
  for i in range(1,maxW+1):
    for j in range(1,nV+1):
      v,w=V[j],W[j]
      opt=dp[i][j-1] # w/o
      if w<=i: opt=max(opt,v+dp[i-w][j-1]) # w
      dp[i][j]=opt
  ans=max(dp[i][nV] for i in range(maxW+1))

print(ans)
