# https://dmoj.ca/problem/dwite12c5p5
# pattern lock @ dmoj

dirs=[[1,0],[1,-1],[0,-1],[-1,-1],[-1,0],[-1,1],[0,1],[1,1]]
dirs2=[[1,2],[1,-2],[2,1],[2,-1],[-1,2],[-1,-2],[-2,1],[-2,-1]]

vis=[[0,0,0],[0,0,0],[0,0,0]]
si,sj=0,0
mx=0
num=0
def gen(i,j,lev):
  global mx,num,si,sj
  if lev==0: 
    num+=1; return
  vis[i][j]=1

  for d in dirs:
    ti,tj=i+d[0],j+d[1]
    if ti<0 or ti>2 or tj<0 or tj>2: continue
    if 0==vis[ti][tj]:
      gen(ti,tj,lev-1)
    else:
      if 0==lev-1 and ti==si and tj==sj: num+=1
      ti,tj=ti+d[0],tj+d[1]
      if ti<0 or ti>2 or tj<0 or tj>2: continue
      if 0==vis[ti][tj]:
        gen(ti,tj,lev-1)
      elif 0==lev-1 and ti==si and tj==sj: num+=1

  for d in dirs2:
    ti,tj=i+d[0],j+d[1]
    if ti<0 or ti>2 or tj<0 or tj>2: continue
    if 0==vis[ti][tj]:
      gen(ti,tj,lev-1)
    elif 0==lev-1 and ti==si and tj==sj: num+=1
  vis[i][j]=0
        

mx=int(input())
tot=0
num=0;si,sj=0,0;gen(0,0,mx);tot+=4*num #; print(num)
num=0;si,sj=0,1;gen(0,1,mx);tot+=4*num # ; print(num)
num=0;si,sj=1,1;gen(1,1,mx);tot+=num; # print(num)

print(tot)
 
