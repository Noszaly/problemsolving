import sys; input=sys.stdin.readline

def mk(N):
  H=[i for i in range(N+1)]
  def find(x):
    if H[x]!=x:
      H[x]=find(H[x])
    return H[x] 

  def unio(x,y):
    x=find(x)
    y=find(y)
    if x!=y:
      H[x]=y
      return 1
    return 0
  return find,unio

v,e=map(int,input().split())
_,uni=mk(v)


vv=v
edges=[]
for i in range(1,e+1):
  a,b=map(int,input().split())
  r=uni(a,b)
  if r>0:
    edges.append(i)
    vv-=1
    if vv<2: break
if vv>1:
  print('Disconnected Graph')
else:
  for v in edges:
    print(v)
  
