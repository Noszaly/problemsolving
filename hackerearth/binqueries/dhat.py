from sys import stdin, stdout
length, query = [int(i) for i in stdin.readline().split()]
number = [int(i) for i in stdin.readline().split()]
for i in range(query):
  string = stdin.readline().split()
  if(len(string)==2):
    number[int(string[1])-1] = (number[int(string[1])-1]+1)%2
  else:
    if number[int(string[2])-1] == 0:
      stdout.write("EVEN"+"\n")
    else:
      stdout.write("ODD"+"\n")
        