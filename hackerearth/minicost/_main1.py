N,K=map(int,input().split())
arr=list(map(int,input().split()))
s=0
for v in arr: s+=abs(v)
pi,ni=0,0
while True:
  if pi>=N or ni>=N: break
  if (arr[pi]<=0 or ni-pi>K): pi+=1; continue
  if (arr[ni]>=0 or pi-ni>K): ni+=1; continue
  t=min(arr[pi],-arr[ni])
  s-=2*t
  arr[pi]-=t; arr[ni]+=t
print(s)
