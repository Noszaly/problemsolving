T=int(input())
for _ in range(T):
  dd={}
  for c in input():
    if None==dd.get(c): dd[c]=0
    dd[c]+=1
  for c in input():
    if None==dd.get(c): dd[c]=0
    dd[c]-=1
  ans=0
  for v in dd.values():
    ans+=abs(v)
  print(ans)