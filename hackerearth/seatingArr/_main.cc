#include <bits/stdc++.h>
using namespace std;

int main(){
  char st[12][3]=
  {"WS","MS","AS","AS","MS","WS","WS","MS","AS","AS","MS","WS"};
  int T;scanf("%d",&T);
  while(T--){
    int n;scanf("%d",&n);
    n-=1;
    int r=11-(n%12);
    int q=n/12;
    printf("%d %s\n",q*12+r+1,st[r]);
  }
	return 0;
}