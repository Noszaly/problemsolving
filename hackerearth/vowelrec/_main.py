# vowelRecognition@hackerEarth
vow=set("aeiouAEIOU")
T=int(input())
for _ in range(T):
  S=input()
  nS=len(S); ans=0
  for i in range(nS): 
    if S[i] in vow: ans+=(i+1)*(nS-i)    
  print(ans)
