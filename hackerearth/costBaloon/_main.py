T=int(input())
for _ in range(T):
  g,p=map(int,input().split())
  gp=pg=0
  for _ in range(int(input())):
    f1,f2=map(int,input().split())
    gp+=f1*g+f2*p
    pg+=f1*p+f2*g
  print(min(gp,pg))
