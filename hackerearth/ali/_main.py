ok=lambda x,y: x.isdigit() and y.isdigit() and (0==(int(x)+int(y))%2)
tag=input().strip()
ans="valid"
while True: # a maganhangzo ell. kimaradt
    if len(tag)!=9: ans="invalid"; break
    if (ok(tag[0],tag[1]) and tag[2].isupper() and ok(tag[3],tag[4]) and 
        ok(tag[4],tag[5]) and tag[6]=='-' and ok(tag[7],tag[8])): break
    ans="invalid"
    break
print(ans)