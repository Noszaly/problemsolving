def tog(x):
    if x.upper(): return x.lower()
    return x.upper()
print(''.join([tog(c) for c in input()]))    