let
  n,l,t,r=parse.(Int,split(readline()))
  res=-1
  while t-l+1+t-r+1==n
    a=parse.(Int,split(readline()))
    tmp=[l:t;].-a[1:t-l+1]
    any(tmp.<0)&&break
    s=sum(tmp)
    tmp=[t:-1:r;].-a[t-l+2:end]
    any(tmp.<0)&&break
    res=s+sum(tmp)
    break
  end
  println(res)
end