let
  I()=parse.(Int,split(readline()))
  na,nq=I()
  INF=10^10
  a=[(-INF,-INF);sort([(v,i) for (v,i) in zip(I(),1:na)]);(INF,INF)]
  q=sort([(v,i) for (v,i) in zip(I(),1:nq)])
  res=fill(0,nq)
  ia,iq=2,1 # inv: a[ia-1]<q[iq]<=a[ia]
  while iq<=nq
    (q[iq]>a[ia])&&(ia+=1;continue)
    res[q[iq][2]]= (q[iq][1]-a[ia-1][1]<a[ia][1]-q[iq][1]) ? a[ia-1][2] : a[ia][2]
    iq+=1
  end
  println(join(string.(res),"\n"))
  # println(res)
  # println(a)
  # println(q)
end