n,s=parse.(Int,split(readline()))
a=fill(0,n,n)
a[1,1]=1
for c=2:n
  s=(c-1)^2+1
  a[1:c,c]=s:s+c-1
  a[c,1:c]=s+2c-2:-1:s+c-1
end
o=""
sa=string.(a.+(s-1))
println(join([join(sa[i,:]," ") for i=1:n],"\n"))
