def is_vowel(c):
    return c in ['a', 'e', 'i', 'o', 'u']

def score_words(words):
    score = 0
    for word in words:
        print(word)
        num_vowels = 0
        for letter in word:
            if is_vowel(letter):
                num_vowels += 1
        if num_vowels % 2 == 0:
            score += 2
        else:
            score+=2 # ++nem jo
    return score


n = int(input())
words = input().split()
print(score_words(words))
