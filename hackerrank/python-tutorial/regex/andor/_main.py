import re

for _ in range(int(input())):
  lin=input()
  if not re.search('$\s#',lin):
    lin=re.sub(r' \|\|(?= )',r' or',re.sub(r' \&\&(?= )',r' and',lin))
  print(lin)
