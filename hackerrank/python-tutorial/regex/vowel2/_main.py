import re

con='bcdfghjklmnpqrstvwxyz'
con=re.compile('['+con+con.upper()+']+')

vow=r'[aeiouAEIOU]+$'

# ~ while True:
  # ~ s=input().strip()
  # ~ print(re.match(vow,s))

  
# ~ print(re.split(con,input().strip()))
# ~ exit()

sol=re.split(con,input().strip())
sol=[ v for v in sol[1:-1] if len(v)>1 and re.match(vow,v)!=None ]
if len(sol)>0:
  for v in sol: print(v)
else:
  print('-1')


