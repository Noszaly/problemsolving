import re
r=r'[456]\d{3}(-*\d{4}){3}$'
rr=r'(\d){3,}'


ll=[
'4253625879615786',
'4424424424442444',
'5122-2368-7954-3214',
'42536258796157867',       #17 digits in card number → Invalid 
'4424444424442444',        #Consecutive digits are repeating 4 or more times → Invalid
'5122-2368-7954 - 3214',   #Separators other than '-' are used → Invalid
'44244x4424442444',        #Contains non digit characters → Invalid
'0525362587961578']


ll2=[
'4123456789123456',
'5123-4567-8912-3456',
'61234-567-8912-3456',
'4123356789123456',
'5133-3367-8912-3456',
'5123 - 3567 - 8912 - 3456'
]
    
for v in ll2:
  out='Invalid'
  if re.match(r,v)!=None:
    out='Valid'
    p,f=-1,-1
    for c in v+'-':
      if not c.isdigit(): continue
      if not c==p:
        if f>3:
          out='Invalid'; break
        p=c; f=0
      f+=1
  print(out)

# ~ print('-----------')

# ~ for v in ll2:
  # ~ vv=re.sub(r'-',r'',v)
  # ~ print('Valid' if re.match(r,v)!=None and re.search(rr,vv)==None else 'Invalid' )
  
