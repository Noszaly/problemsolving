def solve2(s):
  def f(c):
    if c==c.upper(): return c.lower()
    return c.upper()
  return "".join([f(c) for c in s])


def solve(s):
  return " ".join([w[0].upper()+w[1:] for w in s.split()])
# capitalize + split(' ') a megoldas a wa-ra


print(solve(input()))
exit(0)

if __name__ == '__main__':
  fptr = open(os.environ['OUTPUT_PATH'], 'w')

  s = input()

  result = solve(s)

  fptr.write(result + '\n')

  fptr.close()