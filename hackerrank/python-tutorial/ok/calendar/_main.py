from datetime import date
mm,dd,yy=map(int,input().split())
print(date(yy,mm,dd).strftime("%A").upper())