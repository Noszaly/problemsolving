from fractions import Fraction
from functools import reduce

ll=[(1,2),(3,4),(10,6)]

x=map(lambda v:Fraction(v[0],v[1]),ll)
t=reduce(lambda s,t:s*t,x)
print(t)
