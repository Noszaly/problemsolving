from itertools import permutations
w,o=input().split()
w=sorted(w)
o=int(o)
for p in permutations(w,o):
  print(''.join(p))