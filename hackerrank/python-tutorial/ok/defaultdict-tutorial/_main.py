from collections import defaultdict
na,nb=map(int,input().split())
dd=defaultdict(list)
i=1
for _ in range(na):
  dd[input()].append(i)
  i+=1
for _ in range(nb):
  vv=dd[input()]
  ans="-1"
  if len(vv)>0:
    ans=" ".join([str(v) for v in vv])
  print(ans)
