from collections import Counter
N=int(input())
cnt=Counter(map(int,input().split()))
#print(cnt)
M=int(input())
dd={} # ez felesleges, mert nem kell rendezni
# sorba jonnek a vevok es a kiszolgalas igy tortenik...
for _ in range(M):
  s,m=map(int,input().split())
  if None==dd.get(s): dd[s]=[]
  dd[s].append(m)

s=0 # ennyit koltenek
for k in cnt.keys():
  if None==dd.get(k): continue
  s+=sum(dd[k][:cnt[k]])
print(s)
