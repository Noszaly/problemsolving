N=int(input())
M=4*N-3
K=2*N-1
abc="abcdefghijklmnopqrstuvwxyz"
ans=["" for _ in range(K)]
for i in range(N):
  alap=['-']*M
  alap[K-1]=abc[N-1-i]
  lo,hi,jj=K-3,K+1,N-i
  while jj<N:
    alap[lo]=alap[hi]=abc[jj]
    lo-=2; hi+=2; jj+=1
  ans[i]=ans[-1-i]="".join(alap)
print("\n".join(ans))