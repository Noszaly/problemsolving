def print_formatted(n):
  # your code goes here
  w=len("{0:b}".format(n))
  for i in range(1,n+1):
    f10="{0:d}".format(i).rjust(w)
    f8="{0:o}".format(i).rjust(w)
    f16="{0:x}".format(i).rjust(w)
    f2="{0:b}".format(i).rjust(w)
    print(f10+" "+f8+" "+f16+" "+f2)

print_formatted(10)