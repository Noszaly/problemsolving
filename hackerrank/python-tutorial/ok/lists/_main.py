n=int(input())
ll=[]
for _ in range(n):
  cmd,*par=input().split()
#  print(cmd,par)
  while True:
    if cmd=="insert": ll.insert(int(par[0]),int(par[1]));break
    if cmd=="print": print(ll); break
    if cmd=="remove": ll.remove(int(par[0])); break
    if cmd=="append": ll.append(int(par[0])); break
    if cmd=="sort": ll.sort(); break
    if cmd=="pop": ll.pop(); break
    if cmd=="reverse": ll.reverse(); break
    break
