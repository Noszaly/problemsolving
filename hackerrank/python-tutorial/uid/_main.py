for _ in range(int(input())):
  uid=input()
  ans='Invalid'
  while len(uid)==10:
    uid=list(filter(lambda x:x.isalnum(),uid))
    if len(uid)<10 or len(set(uid))<10: break
    if len(list(filter(lambda x:x.isupper(),uid)))<2: break
    if len(list(filter(lambda x:x.isdigit(),uid)))<3: break
    ans='Valid'
    break
  print(ans)
