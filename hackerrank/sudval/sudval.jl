let
  r=[fill(0,9) for i=1:9]
  c=[fill(0,9) for i=1:9]
  s=reshape([fill(0,9) for i=1:9],3,3)

  res=1
  n=parse(Int,readline())
  for i=1:n
    i,j,v=parse.(Int,split(readline()))
    x,y=1+(i-1)÷3,1+(j-1)÷3
    (r[i][v]+c[j][v]+s[x,y][v]>0)&&(res=2;break)
    r[i][v]=c[j][v]=s[x,y][v]=1
  end
  ["OK","WRONG INPUT"][res]|>println
end