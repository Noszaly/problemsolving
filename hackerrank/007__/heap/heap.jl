# sima heap
function makeHeap(N,T,lt) #lt=less than
  h=Array{T,1}(undef,N)
	n=0
	function push!(v)
    n+=1
    h[n]=v
    down(up(n))
	end
	function up(k)
	  while k>1
	    kk=div(k,2)
	    !lt(h[k],h[kk]) && break
	    h[k],h[kk]=h[kk],h[k] 

	    k=kk
	  end
	  k
	end
	function down(k)
	  while k<n
	    kk=2k; 
	    kk>n && break
	    kk+1<=n && lt(h[kk+1],h[kk]) && (kk+=1)
	    !lt(h[kk],h[k]) && break
      h[k],h[kk]=h[kk],h[k] 
	    k=kk
	  end
	  k
	end
	function peek()
	  if n>0 h[1] else nothing end
	end
	function pop!()
    n-=1
    0==n && return
    h[1]=h[n+1]
    down(1)	  
	end
  push!,peek,pop!
end

