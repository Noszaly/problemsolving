let 
	T=parse(Int,readline())
	for _ in 1:T
		m,n=parse.(Int,split(readline()))
		tab=Matrix{Char}(undef,m,n)
		for i in 1:m
			tab[i,:]=collect(readline())
		end #		println(tab)		 
		g=parse(Int,readline()) 
		start=Tuple(findnext((x)->x=='M', tab, CartesianIndex(1,1)))		
		goal=Tuple(findnext((x)->x=='*', tab, CartesianIndex(1,1)))		
		found=false
		ag=0
		dirs=((-1,0),(1,0),(0,-1),(0,1))
		function dfs(par,akt)
			if(akt==goal) found=true; return end
			for d in dirs
				nxt=akt .+ d
				if all((1,1).<=nxt.<=(m,n))&&(nxt!=par)&&
						(tab[CartesianIndex(nxt)]!='X')
					dfs(akt,nxt)
					found&&break
				end
			end
			if true==found
				nd=0
				for d in dirs
					nxt=akt .+ d
					all((1,1).<=nxt.<=(m,n))&&(nxt!=par)&&
						(tab[CartesianIndex(nxt)]!='X')&&(nd+=1)
				end
				(nd>1)&&(ag+=1)
			end				
		end
		dfs((0,0),start)
		println(if ag==g "Impressed" else "Oops!" end)
	end
end
