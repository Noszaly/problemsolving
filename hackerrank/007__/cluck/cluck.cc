#include <bits/stdc++.h>
using namespace std;

typedef pair<int,int> PII;
bool found;
vector<PII> dirs({PII(-1,0),PII(1,0),PII(0,-1),PII(0,1)});
PII start, goal;
int m,n,g,ag;
char tab[108][108];

void dfs(PII par, PII akt){
	if(akt==goal){
		found=true;
		return;
	}
	PII nxt;
	for(auto d:dirs){
		nxt.first=akt.first+d.first, 
		nxt.second=akt.second+d.second;
		if(nxt.first<1||nxt.first>m||nxt.second<1||nxt.second>n||nxt==par||
			tab[nxt.first][nxt.second]=='X'){continue;}
		dfs(akt,nxt);
		if(found){break;}
	}
	if(true==found){
		int nd=0;
		for(auto d:dirs){
			nxt.first=akt.first+d.first, 
			nxt.second=akt.second+d.second;
			if(nxt.first<1||nxt.first>m||nxt.second<1||nxt.second>n||nxt==par||
				tab[nxt.first][nxt.second]=='X'){continue;}
			nd+=1;
		}
		if(nd>1){ag+=1;}
	}
}

int main(){
	int T;scanf("%d",&T);
	while(T--){
		scanf("%d%d",&m,&n);
		for(int i=1;i<=m;i++){
			scanf("%s",&tab[i][1]);
//			printf("%s\n",&tab[i][1]);
		}
		scanf("%d",&g);
		for(int i=1;i<=m;i++){
			for(int j=1;j<=n;j++){
				if('M'==tab[i][j]){
					start=PII(i,j);
				}
				if('*'==tab[i][j]){
					goal=PII(i,j);
				}
			}
		}
//		printf("%d %d\n",start.first,start.second);
		ag=0; found=false;
		dfs(PII(0,0),start);
		if(g==ag){
			printf("Impressed\n");
		}else{
			printf("Oops!\n");
		}
	}//T
	return 0;
}
