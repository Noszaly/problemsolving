let
  m,n=parse.(Int,split(readline()))
#   table=Array{BitVector,1}(undef,m)
#   for i in 1:m
#     table[i]=BitVector(parse.(Int,collect(readline())))
#   end # try compreh.
  table=[ BitVector(parse.(Int,collect(readline()))) for _ in 1:m ]
  mx,nmx=-1,0
#   for i in 1:m-1, j in i+1:m
#     tmp=sum( table[i] .| table[j] )
#     tmp<mx && continue
#     (tmp==mx) && (nmx+=1;continue)
#     mx=tmp; nmx=1
#   end
  for i in 1:m-1
    ti=table[i]
    for j in i+1:m
      tmp=sum( ti .| table[j] )
      tmp<mx && continue
      (tmp==mx) && (nmx+=1;continue)
      mx=tmp; nmx=1
    end
  end
  println(mx,"\n",nmx)
end
