T=int(input())
for _ in range(T):
  nlad=int(input())
  lad=[0]*101
  for _ in range(nlad):
    a,b=map(int,input().split())
    lad[a]=b
  nsnak=int(input())
  snak=[0]*101
  for _ in range(nsnak):
    a,b=map(int,input().split())
    snak[a]=b
  dist=[-1]*101
  qq=[1];dist[1]=0
  while len(qq)>0 and dist[100]<0:
    a=qq.pop(0)
    d=dist[a]+1
    for t in range(a+1,min(a+7,101)):
      if dist[t]>=0: continue
      dist[t]=d
      if 0==snak[t] and 0==lad[t]:
        qq.append(t)
        continue
      tt=t
      if lad[tt]>0: tt=lad[tt]
      else: tt=snak[tt]
      if dist[tt]>=0: continue
      dist[tt]=d
      qq.append(tt)
  print(dist[100])
