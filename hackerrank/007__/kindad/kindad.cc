#include <bits/stdc++.h>
using namespace std;

int main(){
  int n;scanf("%d",&n);
  vector<int> ps(2*n+1,0);//0...n-1 0...n-1 (+1)
  for(int i=0;i<n;i++){
    int ti;scanf("%d",&ti);
    if(ti+1>n){continue;}
    int b=(i-ti+n)%n;
    int a=i+1;
    ps[a]+=1;
    if(a>b){
      b+=n;
    }
    ps[b+1]-=1;
  }
  for(int i=1;i<2*n;i++){
    ps[i]+=ps[i-1];
  }
  int mx=-1,mxL=-1;
  for(int i=0;i<n;i++){
    if(ps[i]+ps[i+n]>mx){
      mx=ps[i]+ps[i+n];
      mxL=i;
    }
  }
  printf("%d\n",mxL+1);

  return 0;
}
