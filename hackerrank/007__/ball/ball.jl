let
  Q=parse(Int,readline())
  for _ in 1:Q
    N=parse(Int,readline())
    n,c=zeros(Int,N),zeros(Int,N)
    for i in 1:N
      arr=parse.(Int,split(readline()))
      n[i]=sum(arr)
      c .+= arr
    end
    sort!(n)
    sort!(c)
    if n == c
      println("Possible")
    else
      println("Impossible")
    end
  end
end  
