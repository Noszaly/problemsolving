let
  p,d,m,s=parse.(Int,split(readline()))
  db=0
  while true
    s<p && break
    s-=p
    db+=1
    if p>m 
      p=max(p-d,m)
    end
  end
  println(db)
end  
