let 
	Q=parse(Int,readline())
	for _ in 1:Q
		n,m=parse.(Int,split(readline()))
		arr=parse.(Int,split(readline()))
		arr=sort(arr[rem.(n,arr).==0])
		arr=arr[arr.>1]
		volt=Dict{Int,Int}()
		function gen(y)
		#	println(y)
			haskey(volt,y)&& return 
			ans=0
			for x in arr
				x>=y&&break
				q,r=divrem(y,x)
				r>0&&continue
				gen(x)
				ans=max(ans,1+q*volt[x])
			end
			volt[y]=ans
		end
		gen(n)
		println(volt[n])
	end
end