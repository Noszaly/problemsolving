# 3,4,5 
pr=[2,3,5,7,11,13,17,19,23]
ispr=[0]*28
for v in pr: ispr[v]=1
def gen(x):
  tmp=[0]*3
  tmp[2]=x%10;x//=10;tmp[1]=x%10;x//=10;tmp[0]=x%10
  if 1==ispr[sum(tmp)]:
    return "".join(str(d) for d in tmp)
  return ""
ll=[]
for x in range(1000):
  v=gen(x)
  if v!="": ll.append(v)

print(ll)
print(len(ll))
