let
	n=parse(Int,readline())
	arr=mod.(parse.(Int,split(readline())),2)
	if mod(sum(arr),2)>0
		println("NO")
	else
		ans=0
		state=-1
		for i in 1:length(arr)
			if 1==arr[i]
				if state<0
					state=i
				else
					ans+=(i-state)
					state=-1
				end
			end
		end
		println(2ans)
	end
end