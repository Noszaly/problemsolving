let
  n,nob=parse.(Int,split(readline()))
  qx,qy=parse.(Int,split(readline()))
  ob=Dict{Tuple{Int,Int},Bool}()
  for _ in 1:nob
    x,y=parse.(Int,split(readline()))
    ob[(x,y)]=true
  end
  function steps(s,d)
    ret=0
    while true
      s=s.+d
      (s[1]<1||s[1]>n||s[2]<1||s[2]>n||haskey(ob,s))&&break
      ret+=1
    end
    ret
  end
  dir=[(0,1),(1,1),(1,0),(1,-1),(0,-1),(-1,-1),(-1,0),(-1,1)]
  ans=0
  for d in dir
    ans+=steps((qx,qy),d)
  end
  println(ans)
end
