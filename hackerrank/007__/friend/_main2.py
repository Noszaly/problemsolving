# friend circle
def mk():
  d={-1:0}
  H=[]
  sH=[]
  def ins(x):
    if d.get(x)==None:
      n=d[-1]; d[-1]+=1
      d[x]=n
      H.append(n)
      sH.append(1)
    return d[x]

  def find(x):
    if H[x]!=x:
      H[x]=find(H[x])
    return H[x] 

  def unio(x,y):
    if x!=y:
      x=find(x)
      y=find(y)
      H[x]=y
      sH[y]+=sH[x]
    return sH[y]
  return ins,find,unio


ii,ff,uu=mk()
mx=0
nr=int(input())
for _ in range(nr):
  a,b=map(int,input().split())
  a=ff(ii(a))
  b=ff(ii(b))
  mx=max(mx,uu(a,b))
  print(mx)

