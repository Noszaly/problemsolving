#include <bits/stdc++.h>
using namespace std;
int main(){
  int K,nA,nB; scanf("%d%d%d",&K,&nA,&nB);
  vector<int> A(nA), B(nB);
  for(int i=0;i<nA;i++){
    scanf("%d",&A[i]);
  }
  for(int i=0;i<nB;i++){
    scanf("%d",&B[i]);
  }
  sort(A.begin(),A.end());
  sort(B.begin(),B.end());
  int opt=-1;
  int iB=nB-1;
  for(int iA=0;iA<nA&&A[iA]+B[0]<=K;iA++){
    int tK=K-A[iA];
    while(iB>=0&&B[iB]>tK){iB--;}
    if(iB<0){break;}
    if(A[iA]+B[iB]>opt){opt=A[iA]+B[iB];}
  }
  
  printf("%d\n",opt);
  
  return 0; 
}
