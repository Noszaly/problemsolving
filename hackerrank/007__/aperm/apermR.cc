#include <bits/stdc++.h>
using namespace std;

int main(){
	int Q;cin>>Q;
	while(Q--){
		int n,k;cin>>n>>k;
		while(true){
			if(0==k){
				for(int i=1;i<=n;i++){
					cout<<i<<" ";
				}
				cout<<"\n";
				break;
			}
			if((n%(2*k))>0){
				cout<<"-1\n";
				break;
			}
			int a=1;
			while(a<n){
				for(int i=a+k;i<a+2*k;i++){
					cout<<i<<" ";
				}
				for(int i=a;i<a+k;i++){
					cout<<i<<" ";
				}
				a+=(2*k);
			}
			cout<<"\n";
			break;
		}
	}//w(Q)
	return 0;
}