let
	Q=parse(Int,readline())
	for _ in 1:Q
		n,k=parse.(Int,split(readline()))
		sol=""
		while true
			if k==0 sol=join(collect(1:n),' ');break end
			if rem(n,2k)>0 sol="-1";break end
			p=zeros(Int,n)
			while a<n
				p[a:a+2k-1]=vcat(collect(a+k:a:2k-1),collect(a:a+k-1))
				a+=2k
			end
			break
		end
		println(sol)
	end
end