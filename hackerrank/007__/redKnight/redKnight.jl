let 
	n=parse(Int,readline())
	sx,sy,tx,ty=parse.(Int,split(readline())) .+ 1
	s,t=(sx,sy),(tx,ty)
#	println(sx)
	dirName=("UL","UR","R","LR","LL","L")
	dir=((-2,-1),(-2,1),(0,2),(2,1),(2,-1),(0,-2))
	dd=Dict{Tuple{Int,Int},Int}()
	qq=Array{Tuple{Int,Int}}(undef,n*n+11)
	head,tail=1,2
	qq[1]=s
	dd[s]=0
	while head!=tail
		a=qq[head]; head+=1
		a==t&&break
		for i in 1:6
			b=a .+ dir[i]
			(b[1]<1||b[1]>n||b[2]<1||b[2]>n||haskey(dd,b))&&continue
			qq[tail]=b; tail+=1
			dd[b]=i
		end
	end
	if haskey(dd,t)
		steps=[]
		while true
			t==s&&break
			i=dd[t]
			push!(steps,dirName[i])
			t=t .- dir[i]
		end
		println(length(steps),"\n",join(reverse!(steps)," ") )
	else
		println("Impossible")
	end
end