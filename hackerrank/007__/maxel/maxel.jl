let 
  N=parse(Int,readline())
  st=Array{Tuple{Int,Int},1}(undef,N)
  nst=0
  for _ in 1:N
    arr=split(readline())
    t=parse(Int,arr[1])
    if 1==t
      num=parse(Int,arr[2])
      nst+=1
      if 1==nst
        st[nst]=(num,num)
      else
        st[nst]=(num,max(num,st[nst-1][2]))
      end
      continue
    end

    if 2==t 
      nst-=1
      continue
    end
    println(st[nst][2])
  end
end
