#include <bits/stdc++.h>
using namespace std;

template<typename T> struct Fenwick{
	int aN;
	vector<T> tree;
	void init(int _aN){//aN<=N
		aN=_aN;
		if(aN+1>tree.size()){
			tree.resize(aN+1);
		}
		fill(tree.begin(),tree.begin()+aN+1,0);
	}
	T query(int x){
		T ans=0;
		while(x>0){
			ans+=tree[x];
			x-=(x&(-x));
		}
		return ans;
	}
	void update(int x, const T val=1){
		while(x<=aN){
			tree[x]+=val;
			x+=(x&(-x));
		}
	}
};

typedef long long int LLI;
LLI ans;
int n,k;
typedef vector<int> VI;
vector<VI> g;
Fenwick<LLI> bit;

void dfs(int s){
	int a=max(1,s-k), b=min(n,s+k);
	LLI pre=bit.query(b)-bit.query(a-1);
	for(auto t:g[s]){
		dfs(t);
	}
	ans+=(bit.query(b)-bit.query(a-1)-pre);
	bit.update(s,1);
}

int main(){
	cin>>n>>k;
	g.resize(n+1);
	VI ideg(n+1,0);

	for(int i=1;i<n;i++){
		int p,c;cin>>p>>c;
		g[p].push_back(c);
		++ideg[c];
	}
	
	ans=0;
	bit.init(n);
	for(int s=1;s<=n;s++){
		if(0==ideg[s]){
			dfs(s);
			break;
		}
	}
	cout<<ans<<"\n";

	return 0;
}