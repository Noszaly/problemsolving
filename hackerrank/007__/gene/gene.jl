let 
	n=parse(Int,readline())
	function f(x)
		x=='A'&&return 1
		x=='T'&&return 2
		x=='G'&&return 3
		return 4
	end
	gene=vcat(f.(collect(strip(readline()))),5)
#	println(gene)
	frek=zeros(Int,5)
	for c in gene
		frek[c]+=1	
	end
	n4=div(n,4)
	tobblet=zeros(Int,5)
	csere=falses(5)
	mind=0
	for i in 1:4
		(frek[i]>n4)&&(csere[i]=true;tobblet[i]=frek[i]-n4;mind+=tobblet[i])
	end
	tobblet[5]=n; csere[5]=true
# println(tobblet)	
# println("all=",mind)	
	ans=0
	if mind>0
		benn=zeros(Int,5)
		L=findnext((x)->true==csere[x],gene,1)
		R=L-1; akt=0
		while akt<mind
			R+=1
			c=gene[R]
			(benn[c]<tobblet[c])&&(akt+=1)
			benn[c]+=1
		end
# println(L," ",R," ",gene[L:R])		
		ans=R-L+1
		while ans>mind
			c=gene[L]
			benn[c]-=1
			sok=benn[c]>=tobblet[c]
			L=findnext((x)->true==csere[x],gene,L+1)
			(L>n)&&break 
			if false==sok
				while true
					R=findnext((x)->true==csere[x],gene,R+1)
					(R>n)&&break
					benn[gene[R]]+=1
					c==gene[R]&&break
				end
				(R>n)&&break 
			end
#println(L," ",R," ",gene[L:R])		
			ans=min(ans,R-L+1)
		end
	end
	println(ans)
end