let
	mlo=join(collect('a':'z'))^2
	mup=join(collect('A':'Z'))^2
	n=parse(Int,readline())
	s=strip(readline())
	k=rem(parse(Int,readline()),26)
	d=Dict{Char,Char}()
	for i in 1:26
		d[mlo[i]]=mlo[i+k]
		d[mup[i]]=mup[i+k]
	end

	sol=fill('?',n)
	for i in 1:n
		c=s[i]
		if haskey(d,c) sol[i]=d[c]; continue end
		sol[i]=c
	end
	println(join(sol))
end