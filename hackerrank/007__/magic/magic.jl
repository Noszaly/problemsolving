let
  A=[2 7 6; 9 5 1; 4 3 8]
  B=Array{Int,2}(undef,3,3)
  for i in 1:3
    B[i,:]=parse.(Int,split(readline()))
  end
  opt=10000
  for _ in 1:2
    for _ in 1:4
      B=rotl90(B)
      opt=min(opt, sum(abs.(B .- A)))
    end
    B=B[:,[3,2,1]]
  end
  println(opt)
end
