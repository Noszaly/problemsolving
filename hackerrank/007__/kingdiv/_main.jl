let
	n=parse(Int,readline())
	gg=[[] for _ in 1:n]
	for _ in 1:n-1
		a,b=parse.(Int,split(readline()))
		push!(gg[a],b); push!(gg[b],a)
	end
	kek,piros=fill((0,0),n),fill((0,0),n)
	MOD=7+10^9
	function dfs(s,par)
		ch=false
		for t in gg[s]
			t==par&&continue
			ch=true # van dfs gyerek
			dfs(t,s)
		end
		(false==ch)&&((kek[s],piros[s])=((1,0),(1,0));return)
		k1,k2,p1,p2=1,1,1,1
		for t in gg[s]
			t==par&&continue
			p1=rem(p1*kek[t][2],MOD)
			k1=rem(k1*piros[t][2],MOD)
			p2=rem(p2*(sum(piros[t])+kek[t][2]),MOD)
			k2=rem(k2*(sum(kek[t])+piros[t][2]),MOD)
		end
		kek[s],piros[s]=(k1,rem(k2+MOD-k1,MOD)),(p1,rem(p2+MOD-p1,MOD))
	end
	dfs(1,-1)
	println(rem(kek[1][2]+piros[1][2],MOD))
end
