let
	n=parse(Int,readline())
	gg=[[] for _ in 1:n]
	for _ in 1:n-1
		a,b=parse.(Int,split(readline()))
		push!(gg[a],b); push!(gg[b],a)
	end
	kek,piros=fill(0,n,2),fill(0,n,2)
	#println(kek)
	MOD=7+10^9
	function dfs(s,par)
		ch=false
		for t in gg[s]
			t==par&&continue
			ch=true # van dfs gyerek
			dfs(t,s)
		end
		(false==ch)&&(kek[s,:]=[1,0];piros[s,:]=[1,0];return)
		kek[s,:]=[1,1];piros[s,:]=[1,1]
		for t in gg[s]
			t==par&&continue
			piros[s,1]=rem(piros[s,1]*kek[t,2],MOD)
			kek[s,1]=rem(kek[s,1]*piros[t,2],MOD)
			piros[s,2]=rem(piros[s,2]*(piros[t,1]+piros[t,2]+kek[t,2]),MOD)
			kek[s,2]=rem(kek[s,2]*(kek[t,1]+kek[t,2]+piros[t,2]),MOD)
		end
		kek[s,2]=rem(kek[s,2]+MOD-kek[s,1],MOD)
		piros[s,2]=rem(piros[s,2]+MOD-piros[s,1],MOD)
	end
	dfs(1,-1)
	#println(kek)
	println(rem(kek[1,2]+piros[1,2],MOD)) #wrong for n==1
	end
