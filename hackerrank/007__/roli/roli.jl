let 
	Q=parse(Int,readline())
	for _ in 1:Q
		n,m,L,R=parse.(Int,split(readline()))
		gg=[[] for _ in 1:n]
		for _ in 1:m
			a,b=parse.(Int,split(readline()))
			push!(gg[a],b)
			push!(gg[b],a)
		end #		println(gg)
		comp=trues(n)
		function dfs(s)
			for v in gg[s]
				if true==comp[v]
					comp[v]=false
					dfs(v)
				end
			end
		end
		K=0
		for s in 1:n
			if true==comp[s]
				K+=1
				comp[s]=false
				dfs(s)
			end
		end
#		println(n-K," ",m)
		if L<=R
			println(n*L)
		else
			println(K*L+(n-K)*R)
		end

	end # Q
end