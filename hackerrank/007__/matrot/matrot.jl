let 
	m,n,r=parse.(Int,split(readline()))
	A=Matrix{Int}(undef,m,n)
	for i in 1:m
		A[i,:]=parse.(Int,split(readline()))
	end
#	println(A)
	function layer(i)
		dir=((1,0),(0,1),(-1,0),(0,-1))
		sm,sn=m-2(i-1)-1,n-2(i-1)-1
		s=(sm,sn,sm,sn-1)
		x,y=i,i
		ret=[(i,i)]
		for i in 1:4
			for _ in 1:s[i]
				(x,y)=(x,y) .+ dir[i]
				push!(ret,(x,y))
			end
		end
		vcat(ret,ret)
	end
	B=copy(A)

	h=2(m+n)-4
	for i in 1:div(min(m,n),2)
		Li=layer(i)
		r1=h+1-mod(r,h)
		for d in 0:h-1
			x,y=Li[1+d]
			rx,ry=Li[r1+d]
			B[x,y]=A[rx,ry]
		end
		h-=8		
	end
	out=""
	for i in 1:m
		out=out*join(B[i,:]," ")*"\n"
	end
	print(out)
end