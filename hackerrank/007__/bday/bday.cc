#include <bits/stdc++.h>
using namespace std;

int main(){
  typedef long long int LLI;
  int Q;cin>>Q;
  while(Q--){
    LLI b,w,bc,wc,z;cin>>b>>w>>bc>>wc>>z;
    if(bc>wc){//make b the cheaper one
      swap(b,w);swap(bc,wc);
    }
    if(bc+z<wc){
      wc=bc+z;
    }
    cout<<b*bc+w*wc<<"\n";
  }
  return 0;
}

