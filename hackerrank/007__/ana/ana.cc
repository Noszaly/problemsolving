#include <bits/stdc++.h>
using namespace std;

int main(){
	char eq[128]={0};
	int Q; cin>>Q;
  while(Q--){
		string s; 
  	cin>>s;
		int n=s.length(), ans=-1;
		if(0==n%2){
			int m=n/2;
			for(int i=0;i<m;i++){
				++eq[s[i]];
			}
			for(int i=m;i<n;i++){
				--eq[s[i]];
			}
			ans=0;
			for(int i='a';i<='z';i++){
				if(eq[i]>0){ans+=eq[i];}
				eq[i]=0;
			}
		}
		cout<<ans<<"\n";
  }
  return 0; 
}
