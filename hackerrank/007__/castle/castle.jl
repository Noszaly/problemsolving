let
  dir=[(0,1),(0,-1),(1,0),(-1,0)]
  INF=1000000000
  n=parse(Int,readline())
  grid=Array{Array{Int,1},1}(undef,n+2)
  grid[1]=zeros(Int,n+2)
  grid[n+2]=grid[1]
  tr(x)=if x=='X' 0 else 1 end
  for i in 2:n+1
    grid[i]=vcat(0,tr.(collect(strip(readline()))),0)
  end
println("grid=",grid)
  dist=[ fill(INF,n+2) for _ in 1:n+2 ]
println("dist=",dist)  
  sx,sy,tx,ty=parse.(Int,split(readline()))
  sx+=2;sy+=2;tx+=2;ty+=2
println((sx,sy))    
println((tx,ty))    

  dist[sx][sy]=0
  Q=Array{Tuple{Int,Int},1}(undef,n*n)
  Q[1]=(sx,sy) # itt egyformat raktam bele veletelenul
  head,tail=1,2
  while head!=tail
    sx,sy=Q[head]
    head+=1
println("* ",(sx,sy))    

    ((sx,sy)==(tx,ty)) && break

    d=dist[sx][sy]+1
println(d, "=dist")    
    for (dx,dy) in dir
      x,y=sx,sy 
      while true
        x+=dx; y+=dy
println(" ",x," ",y," *")    

        if 0==grid[x][y] break end
        if d<dist[x][y]
          dist[x][y]=d
          Q[tail]=(x,y)
          tail+=1
        end
      end
    end # dir
  end # bfs
    
  println(dist[tx][ty]) # always reachable?

end  
