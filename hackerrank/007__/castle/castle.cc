#include <bits/stdc++.h>
using namespace std;

int dir[4][2]={{0,1},{0,-1},{1,0},{-1,0}};
int const N=111;
int const INF=N*N;
char grid[N][N]={0};
int dist[N][N];
int Q[N*N][2];
int main(){
  int n;scanf("%d",&n);
  for(int i=1;i<=n;i+=1){
    scanf("%s",&grid[i][1]);
    for(int j=1;j<=n;j+=1){
      dist[i][j]=INF;
      grid[i][j]=(grid[i][j]=='.'?1:0);
    }
  }
  for(int i=0;i<n;i+=1){
    grid[0][i]=grid[n+1][i]=grid[i][0]=grid[i][n+1]=0;
  }
  int sx,sy,tx,ty;
  scanf("%d%d%d%d",&sx,&sy,&tx,&ty);
  sx+=1;sy+=1;tx+=1;ty+=1;
  dist[sx][sy]=0;
  Q[0][0]=sx;
  Q[0][1]=sy;
  int head=0,tail=1;
  while(head!=tail){
    sx=Q[head][0];
    sy=Q[head][1];
    head+=1;
    if(sx==tx&&sy==ty){break;}
    int d=dist[sx][sy]+1;
    for(int i=0;i<4;i+=1){
      int dx=dir[i][0],dy=dir[i][1];
      int x=sx, y=sy;
      while(true){
        x+=dx; y+=dy;
        if(0==grid[x][y]){break;}
        if(d<dist[x][y]){
          dist[x][y]=d;
          Q[tail][0]=x;Q[tail][1]=y;
          tail+=1;
        }
      }
    }//bfs
  }
  
  printf("%d\n",dist[tx][ty]);//always reachable?

  return 0;
}
