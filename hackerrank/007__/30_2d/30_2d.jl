let 
	A=Matrix{Int}(undef,6,6)
	for i in 1:6
		A[i,:]=parse.(Int,split(readline()))
	end
	mx=-1000000000
	for i in 1:4, j in 1:4
		mx=max(mx,sum(A[i:i+2,j:j+2])-A[i+1,j]-A[i+1,j+2])
	end
	println(mx)
end