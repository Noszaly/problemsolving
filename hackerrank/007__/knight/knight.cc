#include <bits/stdc++.h>
using namespace std;
struct{
	typedef vector<int> VI;
	typedef pair<int,int> PII;
	int const INF=1000000000;
	int n;
	vector<VI> dist;
	vector<VI> out;
	vector<PII> qq;
	
	void init(){
		cin>>n;
		dist.resize(n+1);
		for(auto& di:dist){di.resize(n+1);}
		out.resize(n+1);
		for(auto& oi:out){oi.resize(n+1);}
		qq.resize(n*(n+1));
	}
	void initDist(){
		for(int i=1;i<=n;i++){
			for(int j=1;j<=n;j++){
				dist[i][j]=INF;
			}
		}
	}
	void move(int ii,int jj,int d,int& tail){
		if(ii<1||ii>n||jj<1||jj>n||d>=dist[ii][jj]){return;}
		dist[ii][jj]=d; 
		qq[tail++]=PII(ii,jj);
	};
	void bfs(int x,int y){
		int head=0,tail=1;
		dist[1][1]=0;
		qq[0]=PII(1,1);
		while(head!=tail){
			auto s=qq[head++];
			int i=s.first, j=s.second;
			int d=1+dist[i][j];
			move(i-x,j+y,d,tail);
			move(i+x,j+y,d,tail);
			move(i-x,j-y,d,tail);
			move(i+x,j-y,d,tail);
			if(x!=y){
			move(i-y,j+x,d,tail);
			move(i+y,j+x,d,tail);
			move(i-y,j-x,d,tail);
			move(i+y,j-x,d,tail);
			}
		}
	}
	void solve(){
		for(int x=1;x<n;x++){
			for(int y=x;y<n;y++){
				initDist();
				bfs(x,y);
				int ans=dist[n][n];
				if(INF==ans){
					ans=-1;
				}
				out[x][y]=out[y][x]=ans;
			}
		}
	}
	void write(){
		for(int x=1;x<n;x++){
			for(int y=1;y<n;y++){
				cout<<out[x][y]<<" ";
			}
			cout<<"\n";
		}
	}
}problem;

int main(){
	problem.init();
	problem.solve();
	problem.write();

	return 0;
}