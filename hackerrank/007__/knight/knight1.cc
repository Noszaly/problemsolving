#include <bits/stdc++.h>
using namespace std;
typedef vector<int> VI;
typedef pair<int,int> PII;

int main(){
	int const INF=1000000000;
	int n;cin>>n;
	vector<VI> dist(n+1);
	for(auto& di:dist){di.resize(n+1);}
	auto init=[&dist,n](){
		for(int i=1;i<=n;i++){
			for(int j=1;j<=n;j++){
				dist[i][j]=INF;
			}
		}
	};
	vector<VI> out(n+1);
	for(auto& oi:out){oi.resize(n+1);}
	vector<PII> qq(n*(n+1));
	auto moves=[&dist,&qq,n](int i,int di,int j,int dj,int d,int& tail){
		for(int ii=i-di;ii<=i+di;ii+=2*di){
			if(ii<1||ii>n){continue;}
			for(int jj=j-dj;jj<=j+dj;jj+=2*dj){
				if(jj<1||jj>n||d>=dist[ii][jj]){continue;}
				dist[ii][jj]=d; 
				qq[tail++]=PII(ii,jj);
			}
		}
	};
	auto bfs=[&dist,&qq,moves](int x,int y){
		int head=0,tail=1;
		dist[1][1]=0;
		qq[0]=PII(1,1);
		while(head!=tail){
			auto s=qq[head++];
			int i=s.first, j=s.second;
			int d=1+dist[i][j];
			moves(i,x,j,y,d,tail);
			if(x!=y){
				moves(i,y,j,x,d,tail);
			}
		}
	};

	for(int x=1;x<n;x++){
		for(int y=x;y<n;y++){
			init();
			bfs(x,y);
			int ans=dist[n][n];
			if(INF==ans){
				ans=-1;
			}
			out[x][y]=out[y][x]=ans;
		}
	}

	for(int x=1;x<n;x++){
		for(int y=1;y<n;y++){
			cout<<out[x][y]<<" ";
		}
		cout<<"\n";
	}
	


	return 0;
}