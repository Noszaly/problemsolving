//dfs, (mraron: all good edge can be removed
#include <bits/stdc++.h>
using namespace std;
typedef vector<int> VI;
vector<VI> gg(108);
int ans,n;
int dfs(int s,int par){
	int csum=1;
	for(auto t:gg[s]){
		if(t==par){continue;}
		int tsum=dfs(t,s);
		if(0==tsum%2){++ans;}
		csum+=tsum;
	}
	return csum;
}
int main(){
	int m;cin>>n>>m;//m=n-1
	for(int i=1;i<=n-1;i++){
		int a,b;cin>>a>>b;
		gg[a].push_back(b);
		gg[b].push_back(a);		
	}
	ans=0;
	dfs(1,-1);
	cout<<ans<<"\n";

	return 0;
}