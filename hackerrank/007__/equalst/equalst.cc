#include <bits/stdc++.h>
using namespace std;

typedef vector<int> VI;

void w(VI& x){
  for_each(x.begin(),x.end(),[](int xi){printf("%d ",xi);});
  printf("\n");
}
int main(){
  VI n(3); scanf("%d%d%d",&n[0],&n[1],&n[2]);
  vector<VI> st(3); //hogy lehet n0,n1,n2 elemuekkel inicializalni
  for(int i=0;i<3;i++){
    st[i]=VI(n[i]+1,0);
    auto& sti(st[i]);
    for(int j=n[i];j>0;j--){
      scanf("%d",&sti[j]);
      sti[j]+=sti[j-1];
    }
    partial_sum(st[i].begin(),st[i].end(),st[i].begin());
//w(st[i]);
  }
  while(true){
    int mx=st[0][n[0]], mxL=0, mn=st[0][n[0]];
    if(st[1][n[1]]>mx){mx=st[1][n[1]];mxL=1;}if(st[1][n[1]]<mn){mn=st[1][n[1]];}
    if(st[2][n[2]]>mx){mx=st[2][n[2]];mxL=2;}if(st[2][n[2]]<mn){mn=st[2][n[2]];}
    if(0==mn){//early
      n[0]=0;
      break;
    }
    if(mn==mx){ 
      break;
    }else{
      --n[mxL];
    }
  }
  printf("%d\n",st[0][n[0]]);
  return 0;
}
