let 
	nArr=parse(Int,readline())
	arr=parse.(Int,split(readline()))
	nX=parse(Int,readline())
	X=parse.(Int,split(readline()))
	sort!(arr) #	println(arr)
	function mkCum(x)
		cx=cumsum(x)
		lx=length(x)
		function cum(i)
			(i<1)&&(return 0)
			(i>lx)&&(return cx[lx])
			cx[i]
		end
	end
	function mkSearch(x)
		function search(a,b,k)
			(k>=x[b])&&(return b+1)
			(k<=x[a])&&(return a)
			while a<b
				m=div(a+b,2)
				a,b=if k<=x[m] a,m else m+1,b end
			end
			b
		end
	end
	cum=mkCum(arr)
	search=mkSearch(arr)
	sx=0
	for i in 1:nX
		sx+=X[i]
		i=search(1,nArr,-sx)
		println(-cum(i-1)-sx*(i-1)+(cum(nArr)-cum(i-1))+sx*(nArr-i+1) )			
	end
end