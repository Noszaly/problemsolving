def recurse(src,tar):
    if len(src) < len(tar):
        return False
    if len(tar) == 0:
        return src.islower()
    if src.upper() == tar:
        return True
    if src[0].isupper():
        if src[0] == tar[0]:
            return recurse(src[1:],tar[1:])
        else:
            return False
    else:
        if src[0].upper() != tar[0]:
            return recurse(src[1:],tar)
        else:
            return (recurse(src[1:],tar[1:]) or recurse(src[1:],tar))

def solveProblem():
    a = input()
    b = input()
    x = recurse(a,b)
    if x:
        print('YES')
    else:
        print('NO')

T = int(input())
for t in range(T):
    solveProblem()
