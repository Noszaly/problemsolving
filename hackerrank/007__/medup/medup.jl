# egy elem tobbszor is lehet!!!

let
  # heap with delete functionality
  function makeHeapLoc(N,T,lt) #lt=less than
    h=Array{T,1}(undef,N)
    idx=Dict{T,Int}()
	  n=0
	  function _push(v)
      haskey(idx,v) && idx[v]!=-1 && return false
      n+=1
      h[n]=v; idx[v]=n
      _down(_up(n))
      true
	  end
	  function _up(k)
	    while k>1
	      kk=div(k,2)
	      !lt(h[k],h[kk]) && break
	      h[k],h[kk]=h[kk],h[k] 
        idx[h[k]],idx[h[kk]]=idx[h[kk]],idx[h[k]]
	      k=kk
	    end
	    k
	  end
	  function _down(k)
	    while k<n
	      kk=2k; 
	      kk>n && break
	      kk+1<=n && lt(h[kk+1],h[kk]) && (kk+=1)
	      !lt(h[kk],h[k]) && break
        h[k],h[kk]=h[kk],h[k] 
        idx[h[k]],idx[h[kk]]=idx[h[kk]],idx[h[k]]
	      k=kk
	    end
	    k
	  end
	  function _top()
	    if n>0 h[1] else nothing end
	  end
	  function _pop()
      0==n && return 
      idx[h[1]]=-1
      n-=1
      0==n && return
      idx[h[n+1]]=1
      h[1]=h[n+1]
      _down(1)  
	  end
	  function _del(v)
      nothing==v && return false
      (!haskey(idx,v) || (-1==idx[v])) && return false
	    iv=idx[v]
	    idx[v]=-1
	    n-=1
	    iv==n+1 && return true
      h[iv]=h[n+1]
      idx[h[n+1]]=iv #forget about setting it
      _down(iv)
      true
	  end
	  function _size()
      n
	  end
	  function _println()
	    println(h[1:n])
	  end
    _push,_top,_pop,_del,_size,_println
  end

    
# QHEAP1@hackerrank
  Q=parse(Int,readline())
  pushL,topL,popL,delL,sizeL,printL=makeHeapLoc(Q,Int,<)
  pushR,topR,popR,delR,sizeR,printR=makeHeapLoc(Q,Int,>)

  function adjust()
    tL,tR=topL(),topR()
    sizeL()<sizeR() && (popR();pushL(tR);return)
    sizeL()==sizeR()+2 && (popL();pushR(tL);return)
    0==sizeR() && return
    tL>tR && (popL(); popR();pushL(tR); pushR(tL);return)
  end
  
  for _ in 1:Q
    arr=split(readline())
    c,num=parse(Char,arr[1]),parse(Int,arr[2])
    if 'a'==c
      pushL(num)
    else # 'r'
      delL(num);delR(num)
    end
    adjust()
    if sizeL()>0
      if sizeL()>sizeR()
        printf("%.1lf\n",double(L.top()));
      }else{
        printf("%.1lf\n",0.5*(double(L.top())+R.top()));
      }
      
    else
      println("Wrong!")
    end
  end


end

