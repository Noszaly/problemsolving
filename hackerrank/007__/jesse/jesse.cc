#include <bits/stdc++.h>
using namespace std;

int main(){
  typedef long long int LLI;
  priority_queue<LLI,vector<LLI>,greater<LLI>> H; // minHeap
  int n,k; scanf("%d%d",&n,&k);
  for(int i=0;i<n;i++){
    LLI t;scanf("%lld",&t);
    H.push(t);
  }
  int op=0;
  while(1){
    LLI t=H.top(); H.pop();
    if(t>=k){break;}
    if(H.size()==0){op=-1;break;}
    LLI tt=H.top(); H.pop();
    t=2*tt+t;
    H.push(t);
    ++op;
  }
  printf("%d\n",op);
  return 0; 
}
