let
  p=parse(Int,readline())
  q=parse(Int,readline())
  function isKap(x)
    if 1==x return true end
    sx=string(x^2)
    L=length(sx)
    d=div(1+L,2)
    d<length(sx) && (parse(Int,sx[1:L-d])+parse(Int,sx[L-d+1:end])==x)
  end
#  println(sum(isKap.(p:q)))
  volt=false  
  for v in p:q
    isKap(v)&&(volt=true;print(v," "))
  end
  if false==volt print("INVALID RANGE") end
  println()
end
