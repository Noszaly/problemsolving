let
  m,g=parse.(Int,split(readline()))
  mach=parse.(Int,split(readline()))
  mini,maxi=extrema(mach)
  t=div(g,m)+if (g%m)>0 1 else 0 end
  L,U=t*mini,t*maxi
  while L<U
    M=div(L+U,2)
    fM=sum(div.(M,mach))
    if(fM<g)
      L=M+1
    else
      U=M
    end
  end
  println(U)
end
