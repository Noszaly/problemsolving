#include <bits/stdc++.h>
using namespace std;
typedef long long int LLI;

int main(){
  LLI m,g; cin>>m>>g; //num of machs, goal
  vector<int> mach(m);
  for(int s=0;s<m;s++){
    cin>>mach[s];
  }
  sort(mach.begin(),mach.end());// min-max
  
  auto fun=[mach](LLI M){
    LLI ans=0;
    for(auto v:mach){
      ans+=M/v;
    }
    return ans;
  };
  
  LLI t=(g/m)+(g%m>0?1:0);
  LLI L=t*LLI(mach[0]);
  LLI U=t*LLI(mach[m-1]);
  while(L<U){
    LLI M=(L+U)/2;
    LLI fM=fun(M);
    if(fM<g){
      L=M+1;
    }else{
      U=M;
    }
  }
  cout<<U<<"\n";


  return 0; 
}
