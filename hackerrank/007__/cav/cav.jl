let
	n=parse(Int,readline())
	M=[ [] for _ in 1:n ]
	M2=[ [] for _ in 1:n ]

	for i in 1:n
		M2[i]=collect(strip(readline()))
		M[i]=parse.(Int,M2[i])
	end

	for i in 2:n-1, j in 2:n-1
		if M[i][j]>M[i-1][j]&&M[i][j]>M[i+1][j]&&M[i][j]>M[i][j-1]&&M[i][j]>M[i][j+1]
			M2[i][j]='X'
		end
	end
	for i in 1:n
		println(join(M2[i]))
	end
end