## [hackerRank](https://www.hackerrank.com)

### [submissions](https://www.hackerrank.com/submissions/all)


### [30_2d Array](https://www.hackerrank.com/challenges/30-2d-arrays/problem)
- [sol](30_2d) `Julia`  `tutorial`

### [Even Tree](https://www.hackerrank.com/challenges/even-tree/problem)
-  [sol](evtree) `dfs`


### [Highest Value Palindrome](https://www.hackerrank.com/challenges/richie-rich/problem)
-  [sol](hipal) `implementation`


### [Correctness and the Loop Invariant](https://www.hackerrank.com/challenges/correctness-invariant/problem)
-  `bug`


### [Big Sorting](https://www.hackerrank.com/challenges/big-sorting/problem)
- [sol](bsort) `easy`


### [Stone Division, Revisited](https://www.hackerrank.com/challenges/stone-division-2/problem)
- [sol](stone2) `Julia` `memoization`

### [Bear and Steady Gene](https://www.hackerrank.com/challenges/bear-and-steady-gene/problem)
-[sol](gene)
`Julia` `two-pointer` `sliding-window` 


### [Roads and Libraries](https://www.hackerrank.com/challenges/torque-and-development/problem)
-[sol](roli)
`Julia` `dfs`


### [Insertion Sort Advanced Analysis](https://www.hackerrank.com/challenges/insertion-sort/problem)
-[sol](invan)
`Julia` `merge-sort`


### [Count Luck](https://www.hackerrank.com/challenges/count-luck/problem)
[sol](cluck)
`DFS`

### [Red Knight's Shortest Path](https://www.hackerrank.com/challenges/red-knights-shortest-path/problem)
[sol](redKnight)
`Julia` `BFS`

### [KnightL on a Chessboard](https://www.hackerrank.com/challenges/knightl-on-chessboard/problem)
[sol](knight)
`BFS`

### [Similar Pair](https://www.hackerrank.com/challenges/similarpair/problem)
[sol](simpa) 
`DFS` `Fenwick-tree`



