let
  function mkpow(A::Array{Int,2},M::Int)
    function pow(n::Int)
      if n>1
        B=(pow(n÷2)^2).%M
        (n%2>0)&&(B=(B*A).%M)
        B
      else
        #A[:,:]
        A
      end
    end
    pow
  end

  x=[1 2 4]'
  A=[0 1 0; 0 0 1; 1 1 1]
  M=10^9+7
  pow=mkpow(A,M)

  readline()
  s=0
  inp=parse.(Int,split(readline())) #total:4.6, csak inp:0.8
  for v=inp
    #continue
    s+= if v>3 (pow(v-1)*x)[1]%M else x[v] end
  end  
  println(s%M)

end