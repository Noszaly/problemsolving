#include <bits/stdc++.h>
using namespace std;

struct{
  vector<int> H;
  void init(int N){
    H.resize(N);
    for(int i=0;i<N;i++){H[i]=i;}
  }
  int find(int x){
    if(H[x]!=x){return H[x]=find(H[x]);}
    return H[x]; 
  }
  int unio(int x,int y){
    return H[x]=y;
  }
}UH;

struct I3{
  int a;
  int b;
  int w;
  bool operator<(const I3& m)const{return w<m.w;}
};

vector<I3> info;

typedef long long int LLI;

int main(){
  int T;scanf("%d",&T);
  while(T--){
    int V,E;scanf("%d%d",&V,&E);
    UH.init(V+1);//1 based data
    int nV=V;
    int pi=-1;//prev index of a milkman
    for(int i=1;i<=V;i++){
      int m; scanf("%d",&m);
      if(m==0){continue;}
      if(pi>0){
        UH.unio(i,pi);
        nV-=1;
      }
      pi=i;
    }
    int state=0; // -1: impossible, 0:no decision, 1:positive
    if(pi<0)
      {state=-1;}//no milkman, impossible
    else{
      if(nV==1){state=1;} //only milkmen
    }
    info.resize(E);
    for(int i=0;i<E;i++){
      I3& tmp=info[i];
      scanf("%d%d%d",&tmp.a,&tmp.b,&tmp.w);
    }
    LLI cost=0;
    if(state==0){
      sort(info.begin(),info.end());
      for(auto tmp:info){
        if(nV<2){break;}
        int a=UH.find(tmp.a);
        int b=UH.find(tmp.b);
        if(a!=b){
          nV-=1;
          UH.unio(a,b);
          cost+=tmp.w;
        }
      }
      if(nV==1){state=1;}
      else{state=-1;}//disconnected: impossible
    }
    
    if(state==1){
      printf("%Ld\n",cost);
    }else{
      printf("impossible\n");
    }
  
  }
  return 0;
}
