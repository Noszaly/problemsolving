import sys; input=sys.stdin.readline

# ~ def matmisc(n):
  # ~ def _mul(X,Y,M=None):
    # ~ Z=[0]*n**2
    # ~ for i in range(n):
      # ~ li,hi=i*n,(i+1)*n
      # ~ for j in range(n):
        # ~ z=0
        # ~ ix,iy=li,j
        # ~ while ix<hi:
          # ~ z+=X[ix]*Y[iy]
          # ~ ix+=1
          # ~ iy+=n
        # ~ Z[li+j]=z
    # ~ if M!=None: Z=_mod(Z,M)
    # ~ return Z
  # ~ def _mod(X,M):
    # ~ return [v%M for v in X]
  # ~ def _mkpow(X,M=None):
    # ~ def _pow(p):
      # ~ if p==1: return X[:]
      # ~ Y=_pow(p>>1)
      # ~ Y=_mul(Y,Y,M)
      # ~ if p&1: Y=_mul(X,Y,M)
      # ~ return Y
    # ~ return _pow
  # ~ return _mul,_mod,_mkpow

# ~ def matmisc(n):
  # ~ _tmp=[0]*n**2
  # ~ def _mul(X,Y,M=None,res=_tmp):
    # ~ for i in range(n):
      # ~ li,hi=i*n,(i+1)*n
      # ~ for j in range(n):
        # ~ z=0
        # ~ ix,iy=li,j
        # ~ while ix<hi:
          # ~ z+=X[ix]*Y[iy]
          # ~ ix+=1
          # ~ iy+=n
        # ~ res[li+j]=z
    # ~ if M!=None: _mod(res,M)
    # ~ return res
  # ~ def _mod(X,M):
    # ~ for i in range(n*n):
      # ~ X[i]=X[i]%M
  # ~ def _mkpow(X,M=None):
    # ~ def _pow(p):
      # ~ if p==1: return X[:]
      # ~ Y=_pow(p>>1)
      # ~ Y=_mul(Y,Y,M)[:]
      # ~ if p&1: Y=_mul(X,Y,M)[:]
      # ~ return Y
    # ~ return _pow
  # ~ return _mul,_mod,_mkpow

def matmisc(n,res):
  def _mul(X,Y,M=None):
    for i in range(n):
      li,hi=i*n,(i+1)*n
      for j in range(n):
        z=0
        ix,iy=li,j
        while ix<hi:
          z+=X[ix]*Y[iy]
          ix+=1
          iy+=n
        if M!=None: z=z%M
        res[li+j]=z
    return res
  def _mkpow(X,M=None):
    def _pow(p):
      if p==1: return X[:]
      Y=_pow(p>>1)
      _mul(Y,Y,M)
      Y,res=res,Y
      if p&1: 
        _mul(X,Y,M)
        Y,res=res,Y
      return Y
    return _pow
  return _mul,_mkpow


T=int(input())
while T>0:
  T-=1
  n,p=[int(v) for v in input().split()]
  A=[0]*n**2
  for i in range(n):
    A[i*n:(i+1)*n]=[int(v) for v in input().split()]

  mul,mkpow=matmisc(n,[0]*n**2)
  A=mkpow(A,10**9+7)(p)
  
  for i in range(n):
    print(' '.join(str(v) for v in A[i*n:(i+1)*n]))
