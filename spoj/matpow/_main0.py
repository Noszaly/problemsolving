import numpy as np
import sys; input=sys.stdin.readline

# ~ def mpow(X,M):
  # ~ def _mpow(p):
    # ~ if p==0: return np.eye(np.size(X,0),dtype=object)
    # ~ if p==1: return np.copy(X)
    # ~ Y=_mpow(p>>1)
    # ~ Y=np.mod(np.matmul(Y,Y),M)
    # ~ if p&1>0: Y=np.mod(np.matmul(X,Y),M)
    # ~ return Y
  # ~ return _mpow

T=int(input())
while T>0:
  T-=1
  n,p=[int(v) for v in input().split()]
  A=np.zeros([n,n],dtype=object)
  for i in range(n):
    A[i,:]=[int(v) for v in input().split()]

  # ~ s=1<<int(np.floor(np.log2(p)))
  # ~ s=s>>1
  # ~ B=np.copy(A)
  # ~ while s>0:
    # ~ B=np.mod(np.matmul(B,B),1000000007)
    # ~ if s&p>0:
      # ~ B=np.mod(np.matmul(B,A),1000000007);
    # ~ s=s>>1
  A=np.mod(np.linalg.matrix_power(A,p),1000000007)

  for i in range(n):
    print(' '.join(str(v) for v in A[i,:]))
