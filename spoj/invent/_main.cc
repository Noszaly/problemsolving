#include <bits/stdc++.h>
using namespace std;

struct{
  vector<int> H;
  vector<int> W;

  void init(int N){
    H.resize(N);
    W.resize(N);

    for(int i=0;i<N;i++){
      H[i]=i;
      W[i]=1;
    }
    
  }
  int find(int x){
    if(H[x]!=x){return H[x]=find(H[x]);}
    return H[x]; 
  }
  int unio(int x,int y){
    if(W[x]>W[y]){
      W[x]+=W[y];
      return H[y]=x;
    }
    W[y]+=W[x];
    return H[x]=y;
  }
  int size(int x){return W[x];}
}UH;

struct I3{
  int a;
  int b;
  int w;
  bool operator<(const I3& m)const{return w<m.w;}
};

vector<I3> info;

typedef long long int LLI;

int main(){
  int T;scanf("%d",&T);
  while(T--){
    int V;scanf("%d",&V);
    UH.init(V+1);//1 based data
    info.resize(V-1);
    LLI ans=0;
    for(int i=0;i<V-1;i++){
      I3& tmp=info[i];
      scanf("%d%d%d",&tmp.a,&tmp.b,&tmp.w);
      ans+=tmp.w;
      tmp.w+=1;//next larger 
    }
    sort(info.begin(),info.end());
    for(auto tmp:info){
      int a=UH.find(tmp.a);
      int b=UH.find(tmp.b);
      ans+=(LLI(UH.size(a))*UH.size(b)-1)*tmp.w;
      UH.unio(a,b);
    }
    printf("%Ld\n",ans);
  
  }
  return 0;
}
