import sys;input=sys.stdin.readline


# friend circle
def mk():
  d={-1:0}
  H=[]
  sH=[]
  def ins(x):
    if d.get(x)==None:
      n=d[-1]; d[-1]+=1
      d[x]=n
      H.append(n)
      sH.append(1)
    return d[x]

  def find(x):
    if H[x]!=x:
      H[x]=find(H[x])
    return H[x] 

  def unio(x,y):
    if x!=y:
      x=find(x)
      y=find(y)
      H[x]=y
      sH[y]+=sH[x]
    return sH[y]
  return ins,find,unio


t=int(input())
for _ in range(t):
  nr=int(input())
  
  ins,fnd,uni=mk()
  for _ in range(nr):
    a,b=input().split()
    a=fnd(ins(a)); b=fnd(ins(b))
    print(uni(a,b))

