import sys;input=sys.stdin.readline

K,N=[int(v) for v in input().split()]
obj=[]
mx=0
for i in range(N):
  v,w=[int(v) for v in input().split()]
  if w==0:
    mx+=v
    continue
  obj.append((v,w))

obj.sort(key=lambda x:x[0]/x[1])
N=len(obj)

def gred(lev,cap):
  ret=0
  while lev>=0:
    v,w=obj[lev]
    if cap>=w: 
      cap-=w
      ret+=v
    else:
      break
    lev-=1
  if cap>0 and lev>=0:
    v,w=obj[lev]
    cap*=v # cap*w volt
    ret+=(cap//w)+ 1 if (cap%w)>0 else 0
  return ret
  
def gen(lev,wakt,vakt):
  global mx
  if lev<0:
    mx=max(mx,vakt)
    return
  if vakt+gred(lev,wakt)<=mx: return

  v,w=obj[lev]
  if wakt>=w:
    gen(lev-1,wakt-w,vakt+v)
  gen(lev-1,wakt,vakt)

gen(N-1,K,mx)
print(mx)
