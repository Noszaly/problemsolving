K,N=[int(v) for v in input().split()]
obj=[]
allv=0
mx=0
for i in range(N):
  v,w=[int(v) for v in input().split()]
  if w==0:
    mx+=v
    continue
  allv+=v
  obj.append((v,w))

allv+=mx
obj.sort(key=lambda x:x[0]/x[1])
N=len(obj)

def gen(lev,wakt):
  global mx,allv
  if lev<0:
    mx=allv
    return
  v,w=obj[lev]
  if wakt>=w:
    gen(lev-1,wakt-w)
  if allv-v>mx:
    allv-=v
    gen(lev-1,wakt)
    allv+=v

gen(N-1,K)
print(mx)
