import sys;input=sys.stdin.readline

V,E=[int(x) for x in input().split()]
gg=[set()for _ in range(V)]
dd=[0]*V
edges=[]
for _ in range(E):
  a,b=[int(x) for x in input().split()]
  edges.append((a,b))
  gg[a].add(b);gg[b].add(a)
  dd[a]+=1;dd[b]+=1
nch=sum(v*(v-1)//2 for v in dd)
for a,b in edges:
  nch-=len(gg[a]&gg[b])
print(nch)
