# bf3@dmoj
from math import sqrt
def isp(x):
  if 1==x: return False
  if x<4: return True
  if 0==x%2 or 0==x%3: return False
  L=int(sqrt(x))
  d=5; delta=2
  while d<=L:
    if 0==x%d: return False
    d+=delta
    delta=6-delta
  return True

print(isp(10**9+7))
