import sys;input=sys.stdin.readline
m=[0]+list(map(int,input().split()))
nc=int(input())
c=[[] for _ in range(9)]
for _ in range(nc):
  a,b=map(int,input().split())
  c[a].append(b);c[b].append(a)
state=[1]*9
mx=0
def gen(lev,akt):
  global mx
  if lev==9:
    mx=max(mx,akt)
    return
  if state[lev]>0:
    for v in c[lev]:
      state[v]-=1
    gen(lev+1,akt+m[lev])
    for v in c[lev]:
      state[v]+=1
  gen(lev+1,akt)
    
gen(1,0)
print(mx)
