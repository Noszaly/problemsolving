import sys;input=sys.stdin.readline
n=int(input())
perm=[int(input())-1 for _ in range(n)]

ans=['_']*n
i=0
while i<n:
  if ans[i]=='_': 
  # cycle
    c='A'
    ii=i
    while True:
      ans[ii]=c
      c= 'B' if c=='A' else 'A'
      iii=perm[ii]
      if iii==i: 
        if ans[ii]==ans[i]:
          ans[ii]='C'
        break
      ii=iii
  i+=1
    
print(''.join(ans))
