# proof?
T=int(input())
while T:
  T-=1
  input()# unused
  ff,arr={},[]
  for v in input().split():
    x=int(v)
    if ff.get(x)==None: 
      ff[x]=0
      arr.append(x)
    ff[x]+=1
  arr.sort()
  M,ans=arr[-1],0 # M: early exit
  for v in arr:
    fv=ff[v]
    v2=2*v
    if v2>M: break
    m=0
    if ff.get(v2): 
      m=min(fv,ff[v2])
      ff[v2]-=m
    ans+=m
  print(ans)
