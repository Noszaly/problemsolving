T=int(input())
while T:
  T-=1
  ex=input().strip()
  lex=len(ex)
  dd={}
  for i in range(lex):
    if ex[i].isdigit(): dd[(i,i)]=(int(ex[i]),int(ex[i]))

  def gen(i,j):
    if dd.get((i,j))!=None: return
    opt=[9**60,0]
    k=i
    while k<j:
      gen(i,k); bal=dd[(i,k)]
      gen(k+2,j); jobb=dd[(k+2,j)]
      if ex[k+1]=='*':
        opt=(min(opt[0],bal[0]*jobb[0]),max(opt[1],bal[1]*jobb[1]))
      else:
        opt=(min(opt[0],bal[0]+jobb[0]),max(opt[1],bal[1]+jobb[1]))
      k+=2
    dd[(i,j)]=(opt[0],opt[1])
  
  gen(0,lex-1)
  print(dd[(0,lex-1)][1],dd[(0,lex-1)][0])
  
