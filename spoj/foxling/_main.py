# foxling@spoj
# bfs
import sys; input=sys.stdin.readline

V,E=[int(v) for v in input().split()]
gg=[[] for _ in range(V+1)] #1-based 
while E:
  E-=1
  a,b=[int(v) for v in input().split()]
  gg[a].append(b);gg[b].append(a)
ans=V
qq=[0]*(V+1)
vis=[False]*(V+1)
for a in range(1,V+1):
  if vis[a]: continue
  qq[0],head,tail=a,0,1
  vis[a]=True
  while head<tail:
    s=qq[head];head+=1
    for t in gg[s]:
      if vis[t]: continue
      qq[tail]=t; tail+=1
      vis[t]=True
  ans-=(tail-1)
print(ans)
