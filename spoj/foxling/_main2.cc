#include <bits/stdc++.h>
using namespace std;

#define GETCHAR getchar_unlocked
int getNum(void){
  int i,ans;
  while(!(((i=GETCHAR())>='0')&&(i<='9')));
  ans=(i-'0');
  while(((i=GETCHAR())>='0')&&(i<='9')){
    ans=((ans<<1)+(ans<<3)+(i-'0'));
  }
  return ans;
}

int main(){
  
  int V=getNum();int E=getNum();
  vector<vector<int>> gg;
  unordered_map<int,int,hash<int>> mm;
  int v=0;
  while(E--){
    int a=getNum();int b=getNum();
    if(mm.find(a)==mm.end()){mm[a]=v;v+=1;gg.push_back({});}
    if(mm.find(b)==mm.end()){mm[b]=v;v+=1;gg.push_back({});}
    a=mm[a]; b=mm[b];
    gg[a].push_back(b);gg[b].push_back(a);
  }
  int ans=V;
  V=v;
  vector<int > qq(V);
  vector<bool> vis(V,false);
  for(int a=0;a<V;a++){
    if(vis[a]){continue;}
    qq[0]=a; int head=0,tail=1;
    vis[a]=true;
    while(head<tail){
      int s=qq[head];head+=1;
      for(auto t:gg[s]){
        if(vis[t]){continue;}
        qq[tail]=t; tail+=1;
        vis[t]=true;
      }
    }
    ans-=(tail-1);
  }
  printf("%d\n",ans);

  return 0;
}
