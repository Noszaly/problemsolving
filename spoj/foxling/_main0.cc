#include <bits/stdc++.h>
using namespace std;

int main(){
  
  int V,E; scanf("%d%d",&V,&E);
  vector<vector<int>> gg(V+1);
  while(E--){
    int a,b;scanf("%d%d",&a,&b);
    gg[a].push_back(b);gg[b].push_back(a);
  }
  int ans=V;
  vector<int > qq(V+1);
  vector<bool> vis(V+1,false);
  for(int a=1;a<=V;a++){
    if(vis[a]){continue;}
    qq[0]=a; int head=0,tail=1;
    vis[a]=true;
    while(head<tail){
      int s=qq[head];head+=1;
      for(auto t:gg[s]){
        if(vis[t]){continue;}
        qq[tail]=t; tail+=1;
        vis[t]=true;
      }
    }
    ans-=(tail-1);
  }
  printf("%d\n",ans);

  return 0;
}
