#include <bits/stdc++.h>
using namespace std;
typedef tuple<int,int> T2;
typedef tuple<int,int,int> T3;
struct T3Hash{
   int operator()( T3 const& T ) const{
       return std::hash<int>()(53*53*get<0>(T)+53*get<1>(T)+get<2>(T));
   }
};
T2 rNum(char* S,int i){
  int x=0;
  while(isdigit(S[i])){
    x=10*x+(S[i]-'0');
    i+=1;
  }
  return T2{x,i};
}

vector<int> tx;
unordered_map<T3,int,T3Hash> dd;
int ii,tt,ans;
char*A; char*B;

T3 procA(){
  if(A[ii]=='('){
    ii+=1;
    auto ll=procA();ii+=1;
    auto rr=procA();ii+=1;
    auto mm=T3{get<0>(ll),get<1>(rr),get<2>(ll)+get<2>(rr)};
    dd[mm]=1;
    return mm;
  }else{
    int x;
    tie(x,ii)=rNum(A,ii);
    tt+=1;
    tx[x]=tt;
    return T3{tt,tt,1};
  }
}

T3 procB(){
  if( B[ii]=='('){
    ii+=1;
    auto ll=procB();ii+=1;
    auto rr=procB();ii+=1;
    auto mm=T3{min(get<0>(ll),get<0>(rr)),max(get<1>(ll),get<1>(rr)),
    get<2>(ll)+get<2>(rr)};
    if(dd.find(mm)!=dd.end()){ans+=1;}
    return mm;
  }else{
    int x;
    tie(x,ii)=rNum(B,ii);
    return T3{tx[x],tx[x],1};
  }
}
char buff[500000];//1e6 kellett
int main(){
  int N;scanf("%d",&N);
  if(N>2){
    tx.resize(N+1);
    A=buff; scanf("%s",A);
    ii=0; tt=0;
    procA();
    B=buff; scanf("%s",B);
    ii=0; ans=0;
    procB();
    printf("%d\n",ans+N);
  }else{
    printf("%d\n",N);
  }

  return 0;
}