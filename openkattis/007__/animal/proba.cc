#include <bits/stdc++.h>
using namespace std;
typedef tuple<int,int> T2;
typedef tuple<int,int,int> T3;

int main(){
  auto a=T2{-1,-2};
  printf("%d %d\n",get<0>(a), get<1>(a));

  auto aa=T2{-1,-2};
  printf("%d\n",aa==a);

  auto b=T3{-1,-2,-3};
  printf("%d %d %d\n",get<0>(b),get<1>(b),get<2>(b));

  printf("%d %d\n",isdigit('a'),isdigit('1'));

  return 0;
}