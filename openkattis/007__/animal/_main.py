def rNum(S,i):
  x=0
  while True==S[i].isdigit():
    x=10*x+int(S[i]); i+=1
  return x,i

tx=[]
dd={}
A,B="",""
ii=0; tt=0
def procA():
  global ii,tt
  if A[ii]=='(':
    ii+=1
    ll=procA();ii+=1
    rr=procA();ii+=1
    mm=(ll[0],rr[1],ll[2]+rr[2])
    dd[mm]=True
    return mm
  else:
    x,ii=rNum(A,ii)
    tt+=1
    tx[x]=tt
    return (tt,tt,1)
    print(x)

ans=0
def procB():
  global ans,ii
  if B[ii]=='(':
    ii+=1
    ll=procB();ii+=1
    rr=procB();ii+=1
    mm=(min(ll[0],rr[0]),max(ll[1],rr[1]),ll[2]+rr[2])
    if None!=dd.get(mm):
      ans+=1
    return mm
  else:
    x,ii=rNum(B,ii)
    return (tx[x],tx[x],1)
    print(x)

N=int(input())
A=input().strip().replace(" ","")
B=input().strip().replace(" ","")
if N>2:
  tx=[0]*(N+1)
  ii,tt=0,0
  procA()
  ii,ans=0,0
  procB()
  print(ans+N)
else:
  print(N)