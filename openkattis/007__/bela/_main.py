nd={'A':11,'K':4,'Q':3,'J':2,'T':10,'9':0,'8':0,'7':0}
d={'A':11,'K':4,'Q':3,'J':20,'T':10,'9':14,'8':0,'7':0}

N,D=input().split()
N=int(N)
s=0
for _ in range(4*N):
  a=input()
  if a[1]==D: s+=d[a[0]]
  else: s+=nd[a[0]]
print(s)