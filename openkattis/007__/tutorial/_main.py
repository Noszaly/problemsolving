import math
ord=[
  lambda n: 0,
  lambda n:sum([math.log2(i) for i in range(1,min(30,n+1))]),
  lambda n:n,
  lambda n:4*math.log2(n),
  lambda n:3*math.log2(n),
  lambda n:2*math.log2(n),
  lambda n:math.log2(n)+math.log2(1e-18+math.log2(n)),
  lambda n:math.log2(n)
]
m,n,t=map(int,input().split())
if ord[t](n)>math.log2(m):
  print("TLE")
else:
  print("AC")