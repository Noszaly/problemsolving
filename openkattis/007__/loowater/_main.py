while True:
  nd,nk=map(int,input().split())
  if 0==nd+nk: break
  dd=[]
  for _ in range(nd):
    dd.append(int(input()))
  kk=[]
  for _ in range(nk):
    kk.append(int(input()))
  dd.sort(); kk.sort()
  p=0
  id,ik=0,0
  while id<nd and ik<nk:
    if kk[ik]>=dd[id]:
      p+=kk[ik]
      ik+=1
      id+=1
    else:
      ik+=1
  if id<nd:
    print("Loowater is doomed!")
  else:
    print(p)