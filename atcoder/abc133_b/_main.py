import math
n,d=map(int,input().split())
arr=[]
for _ in range(n):
	arr.append(list(map(int,input().split())))
def dist2(p,q):
  ans=0
  for i,j in zip(p,q):
    ans+=(i-j)**2
  return ans

ans=0
for i in range(n-1):
  for j in range(i+1,n): 
    d2=dist2(arr[i],arr[j])
    d=int(math.sqrt(d2)+1e-15)
    if d*d==d2: ans+=1
print(ans)    
