def gcd(a,b):
  c=0
  while True:
    c=a%b
    if 0==c: break
    a,b=b,c
  return b

a=int(input())
b=int(input())
x=int(input())
lcm=(a*b)//gcd(a,b)
print(1+(x-1)//lcm)