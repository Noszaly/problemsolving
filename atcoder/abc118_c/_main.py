def gcd(a,b):
  c=0
  while True:
    c=a%b
    if 0==c: break
    a,b=b,c
  return b

N=int(input())
arr=list(map(int,input().split()))
d=arr[0]
for a in arr[1:]:
  d=gcd(d,a)
print(d)  
