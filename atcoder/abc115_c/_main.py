N,K=map(int,input().split())
h=[]
for _ in range(N):
  h.append(int(input()))
h.sort()
opt=h[-1]-h[0]+1 # inf
for i in range(K-1,N):
  opt=min(opt,h[i]-h[i-K+1])
print(opt)  
