N,M=map(int,input().split())
like=[0]*(M+1)
for _ in range(N):
  arr=list(map(int,input().split()))
  for v in arr[1:]:
    like[v]+=1
print(sum([v==N for v in like]))    
